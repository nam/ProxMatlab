%                      NormalCone_graphics.m
%                   written on Jan. 23, 2009 by
%                        Russell Luke
%                  University of Delaware
%
% DESCRIPTION:  Script driver for viewing results from projection
%               algorithms on various toy problems
%
% INPUT:  
%              method_input/output = data structures
%
% OUTPUT:       graphics
% USAGE: NormalCone_graphics(method_input,method_output)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function success = NormalCone_graphics(method_input, method_output)
              
method=method_input.method;
beta0 = method_input.beta_0;
beta_max = method_input.beta_max;
u = method_output.u1(:,:,1);

iter = method_output.stats.iter;
change = method_output.stats.change;
if(any(strcmp('time', fieldnames(method_output.stats))))
    time = method_output.stats.time;
else
    time=999
end


figure(900);

    figure(901); plot(u); title('reconstructed signal'); drawnow; axis('tight')
    label = [ 'iteration', ', time = ',num2str(time), 's'];
    figure(902);   semilogy(change),xlabel(label),ylabel(['change in iterates'])
    label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
    title(label)
    if(any(strcmp('diagnostic', fieldnames(method_input))))
        gap  = method_output.stats.gap;
        label = [ 'iteration', ', time = ',num2str(time), 's'];
        figure(903);   semilogy(gap),xlabel(label),ylabel(['log of the gap distance'])
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(label)
    end



success = 1;
