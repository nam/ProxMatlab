%           P_ptychography_PHeBIE_phi_regularized_ptwise.m
%              written on 29th Janurary 2014
%                       Matthew Tam
%                      CARMA Centre
%                  University of Newcastle
%               last modified 23rd July 2014
%
% DESCRIPTION:  Regularized phi update for the PHeBIE algorithm
%               with pointwise blocking.
%
%

function u = P_ptychography_PHeBIE_phi_regularized_ptwise(input,u)

if ~isfield(input,'overrelax')
   gamma = 0;
else
   gamma = input.overrelax-1;
end

fnorm = sqrt(input.Ny*input.Nx);
for pos=1:input.N_pie
    Indy = input.positions(pos,1) + (1:input.Ny);
    Indx = input.positions(pos,2) + (1:input.Nx);
    newPhi = (2/(2+gamma)) * u.probe .* u.object(Indy,Indx)  +  gamma/(2+gamma) * u.phi(:,:,pos);
    oldPhiHat = feval('fft2',newPhi) / fnorm;
    phiHat = feval('MagProj',input.Amp_exp_norm(:,:,pos),oldPhiHat);
    if isfield(input,'fmask') %Mask in the Fourier domain.
        phiHat = phiHat .* input.fmask(:,:,pos) + oldPhiHat .* (input.fmask(:,:,pos)==0);
    end
    phiPrime = feval('ifft2',phiHat) * fnorm;
    u.phi(:,:,pos) = phiPrime;
end

end

