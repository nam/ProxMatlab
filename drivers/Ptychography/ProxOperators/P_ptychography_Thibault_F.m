%             P_ptychography_Thibault_F.m
%                written by Paer(?) on ??
%           last modified 8th March 2014
%                     Matthew Tam
%                    CARMA Centre
%               University of Newcastle
%
% DESCRIPTION:  Fourier magnitude update for the Thibault et al algorithm.
%
%
% INPUT:       input = a data structure with fields:
%              u     = array to be updated
%
% OUTPUT:      u     = the updated array.
%
% USAGE: u = P_ptychography_Thibault_F(input,u)
%
% Nonstandard Matlab function calls:

function u = P_ptychography_Thibault_F(input,u)

if(strcmp(input.ptychography_prox,'Thibault_AP'))
    for pos=1:input.N_pie
        Indy = input.positions(pos,1) + (1:input.Ny);
        Indx = input.positions(pos,2) + (1:input.Nx);
        u.phi(:,:,pos) = u.probe .* u.object(Indy,Indx);
    end
end

fnorm = sqrt(input.Ny*input.Nx);
for pos=1:input.N_pie   
    %newPhi = feval('fft2',u.phi(:,:,pos)) / fnorm;
    oldPhiHat = feval('fft2',u.phi(:,:,pos)) / fnorm;
    phiHat = feval('MagProj',input.Amp_exp_norm(:,:,pos),oldPhiHat);
    if isfield(input,'fmask') %Mask in the Fourier domain.
        phiHat = phiHat .* input.fmask(:,:,pos) + oldPhiHat .* (input.fmask(:,:,pos)==0);
    end
    u.phi(:,:,pos) = feval('ifft2',phiHat) * fnorm;
end

end

