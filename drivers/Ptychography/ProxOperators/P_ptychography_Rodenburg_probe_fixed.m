%        P_ptychography_Rodenburg_probe_fixed.m
%                written on ?? 2013 by
%                    Paer Mattsson
%              University of Goettingen
%         last modified on 29th January 2014 by
%                     Matthew Tam
%                    CARMA Centre
%                University of Newcastle
%
% DESCRIPTION: Rodenburg's ptychography algorithim.
%
%
% INPUT:       input, a data structure with fields:
%              u,      array in the domain to be projected
%
% OUTPUT:      u,     the output iterate
%
% USAGE: u = P_ptychography_Rodenburg(input,u)
%
% Nonstandard Matlab function calls:

function u = P_ptychography_Rodenburg_probe_fixed(input,u)
thisObject = u.object;
thisProbe  = u.probe;
thisPhi    = zeros(size(u.phi));

% Norming for the magnitude projection
fnorm = sqrt(input.Ny*input.Nx);

s = randperm(input.N_pie);
for pos = s %1:input.N_pie
    % Get indices for probe and object.
    Indy = input.positions(pos,1) + (1:input.Ny);
    Indx = input.positions(pos,2) + (1:input.Nx);
    
    % Mask in the sample plane?
    if input.switch_probemask
        Probe_norm = norm_avg(thisProbe);
        thisProbe  = thisProbe .* input.Probe_mask;
        thisProbe  = thisProbe .* Probe_norm / norm_avg(thisProbe);
    end
    
    % Compute the wave from the latest probe and object guess.
    indexedObject = thisObject(Indy, Indx);
    phi      = thisProbe .* indexedObject;
    oldPhiHat = feval('fft2',phi) / fnorm;
    phiHat = feval('MagProj',input.Amp_exp_norm(:,:,pos),oldPhiHat);
    if isfield(input,'fmask') %Mask in the Fourier domain.
        phiHat = phiHat .* input.fmask(:,:,pos) + oldPhiHat .* (input.fmask(:,:,pos)==0);
    end
    phiPrime = feval('ifft2',phiHat) * fnorm;
    
    % Perform the Rodenburg inner-iterations
    for k=1:input.RodenburgInnerIt
        thisObjectOld = thisObject;
        thisProbeOld = thisProbe;
        I_Probe  = conj(thisProbeOld) .* thisProbeOld;
%        I_Object = conj(thisObjectOld(Indy, Indx)) .* thisObjectOld(Indy, Indx);
        
%         % Update probe
%         if(input.iter > 1)
%             thisProbe = thisProbeOld + 1*(conj(thisObjectOld(Indy, Indx))./(max(max(I_Object))+1e-8)).*(phiPrime - phi);
%         end
        
        % Update object
        thisObject(Indy, Indx) = thisObjectOld(Indy, Indx) + 1*(conj(thisProbeOld)./(max(max(I_Probe))+1e-8)).*(phiPrime - phi);
        phi      = thisProbe .* thisObject(Indy, Indx);
        phiHat   = feval('fft2',phi) / fnorm;
        phiHat   = feval('MagProj',input.Amp_exp_norm(:,:,pos),phiHat);
        phiPrime = feval('ifft2',phiHat) * fnorm;
    end
    
    %Apply the support constraint.
    if input.switch_object_support_constraint
        thisObject = thisObject .* input.object_support;
    end
    
    % Enforce the object lies in [0,1]
    trans_max_true = input.trans_max_true;
    trans_min_true = input.trans_min_true;
    abs_Object = sqrt(conj(thisObject).*thisObject);
    high = (abs_Object > trans_max_true);
    low  = (abs_Object < trans_min_true);
    thisObject = (1-low).*(1-high).*thisObject + (low*trans_min_true + high*trans_max_true).*thisObject./ (abs_Object + 1e-30);
    
    % Save to stop the algorithm from stopping
    thisPhi(:,:,pos) = thisProbe .* thisObject(Indy, Indx);
    
    %% Plot, for debugging
%     figure(333)
%     colormap gray
%     subplot(2,2,1)
%     imagesc(log10(abs(thisObject)));
%     subplot(2,2,2)
%     imagesc(angle(thisObject));
%     subplot(2,2,3)
%     imagesc(log10(abs(thisProbe)));
%     subplot(2,2,4)
%     imagesc(angle(thisProbe));
%     drawnow;
%     pause(0.5)
end
% close 333

% Update the iterate.
u.phi    = thisPhi;
u.object = thisObject;
u.probe  = thisProbe;

end

