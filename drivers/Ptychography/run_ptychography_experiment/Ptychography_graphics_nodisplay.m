%              Ptychography_graphics_nodisplay.m
%              written on 10th March 2014
%                       Matthew Tam
%                      CARMA Centre
%                  University of Newcastle
%
% DESCRIPTION:  Script driver for viewing results from ptychography
%               for use when the "-nodisplay" flag is used to start 
%               MATLAB.
%
% INPUT:        method_input/output = data structures
%
% OUTPUT:       graphics
%
% USAGE:        Ptychography_graphics(method_input,method_output)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function input = Ptychography_graphics( input, output )
% Make local copies of the varibles.
if isfield(output,'u')
    u = output.u; %u is phi (legacy reasons)
elseif isfield(output,'u1')
    u = output.u1;
end

probe  = u.probe;
object = u.object;

%% Choose the appropriate plotting routine, depending on whether the 
%% algorithm has terminated. (i.e., Is this 'final plot' or 'animation' ?)

if isfield(output,'u1') % i.e., Algorithm has terminated.
    %% Final plot routine.
    method_input  = input;
    method_output = output;
    
    method   = method_input.method;
    beta0    = method_input.beta_0;
    beta_max = method_input.beta_max;
    u_0      = method_input.u_0;
    u        = method_output.u1(:,:,1);
    iter     = method_output.stats.iter;
    change   = method_output.stats.change;
    gap      = method_output.stats.gap;
    if isfield(method_output.stats,'customError')
        rmserrorobj    = method_output.stats.customError(:,1);
        rmserrorprobe  = method_output.stats.customError(:,2);
        changephi      = method_output.stats.customError(:,3);
        R_factor       = method_output.stats.customError(:,4);
    end
    
    
    % Object amplitude.
    figure(600)
    subplot(1,2,1)
    ampObject = log10(abs(object));
    %ampObject(ampObject<0.5) = ampObject(ceil(end/2),ceil(end/2));
    imagesc(ampObject);
    colormap gray;
    axis equal tight;
    colorbar;
    title('Best object approximation, amplitude');
    subplot(1,2,2)
    imagesc(log10(abs(method_input.sample_plane)));
    colormap gray;
    axis equal tight;
    colorbar;
    title('Exact object, amplitude');
    
    % Object phase.
    figure(601)
    subplot(1,2,1)
    imagesc(angle(object));
    colormap gray;
    axis equal tight;
    colorbar;
    title('Best object approximation, phase');
    subplot(1,2,2)
    imagesc(angle(method_input.sample_plane));
    colormap gray;
    axis equal tight;
    colorbar;
    title('Exact object, phase');
    
    % Probe.
    figure(602)
    subplot(1,2,1)
    imagesc(hsv2rgb(im2hsv(probe./max(max(abs(probe))), 1)));
    title('Best probe approximation');
    axis equal tight;
    subplot(1,2,2)
    imagesc(hsv2rgb(im2hsv(method_input.probe./max(max(abs(method_input.probe))), 1)));
    title('True probe');
    title('Exact probe');
    axis equal tight;
    
    % Objective function value, or change (algorithm dependent).
    figure(603)
    if strcmp(input.method(end-3:end),'PALM')
        semilogy(change),xlabel('iteration'),ylabel('object function value')
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['Objective value, ' label])
    else
        subplot(1,2,1)
        semilogy(change),xlabel('iteration'),ylabel(['||x_{k+1}-x_{k}||'])
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['change, ' label])
        subplot(1,2,2)
        semilogy(gap),xlabel('iteration'),ylabel('\|\sum_j \|x_k - \Pi_M x_k\| + \sum_j \|x_k - \Pi_N x_k\|$')
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['gap, ' label])
    end
    
    figure(604)
    colormap gray;
    meas = zeros(size(method_input.sample_plane));
    for pos=1:method_input.N_pie
        Indy = method_input.positions(pos,1) + (1:method_input.Ny);
        Indx = method_input.positions(pos,2) + (1:method_input.Nx);
        meas(Indy, Indx) = meas(Indy, Indx)...
            + ones(length(Indy), length(Indx)).*method_input.Probe_mask;
    end
    imagesc(meas);
    title('Number of measurements, pixelwise')
    colorbar;
    
    
    
    if isfield(method_output.stats,'customError')    
        figure(605)
        subplot(1,2,1)
        semilogy(rmserrorobj),xlabel('iteration'),ylabel('RMS-Error, Object')
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['RMS Error Object, ' label])
        subplot(1,2,2)
        colormap gray;
        %newmeas = zeros(size(meas));
        %newmeas(meas > method_input.N_pie*(1/2)) = meas(meas > method_input.N_pie*(method_input.rmsfraction));
        imagesc(input.object_support);
        title('Area over which error is measured.')
        axis equal tight
        colorbar;
        
        figure(606)
        semilogy(rmserrorprobe),xlabel('iteration'),ylabel('RMS-Error, Probe')
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['RMS Error Probe, ' label])
        
        figure(607)
        semilogy(changephi),xlabel('iteration'),ylabel(['||Phi_{k+1}-Phi_{k}||'])
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['change, ' label])
        
        figure(608)
        plot(R_factor),xlabel('iteration'),ylabel('R-factor');
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['R-factor, ' label])
    end
    
else % i.e., Algorithm is still running, and we would like to animate.
    %% Animation routine.
    iter   = input.iter;
    if isfield(output,'change')
        change = output.change;
    end
    
    if(input.anim)       
        [X_obj,Y_obj] = meshgrid((1:input.object_size(2))*input.d1x,(1:input.object_size(1))*input.d1y);
        
        if strcmp(input.datatype,'exp')
            % Reconstructed object amplitude.
            figure(1)
            subplot(3,4,1)
            imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),log10(abs(object)));
            colormap(hsv);
            title(['Ampl., it  ' num2str(iter)]);
            
            % Reconstructed object phase.
            subplot(3,4,2)
            imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),angle(object));
            title(['Phase, it ' num2str(iter)]);
            
            % Actual object amplitude.
            subplot(3,4,5)
            imagesc(log10(abs(input.sample_plane)));
            title('True amplitude');
            
            % Actual object phase.
            subplot(3,4,6)
            imagesc(angle(input.sample_plane));
            hold on
            title('True phase');
            
            subplot(3,4,9)
            imagesc(angle(input.sample_plane));
            hold on
            scatter(input.positions(:,2)+floor(input.Nx/2+1),...
                input.positions(:,1)+floor(input.Ny/2+1),...
                (2*input.R/input.d1x)*ones(1,size(input.positions,1)),'o','black');
            hold off
            title('Measurements');
            
            subplot(3,4,10)
            meas = zeros(size(input.sample_plane));
            for pos=1:input.N_pie
                Indy = input.positions(pos,1) + (1:input.Ny);
                Indx = input.positions(pos,2) + (1:input.Nx);
                meas(Indy, Indx) = meas(Indy, Indx)...
                    + ones(length(Indy), length(Indx)).*input.Probe_mask;
            end
            imagesc(meas);
            
            % Probe related.
            subplot(3,4,3)
            imagesc(hsv2rgb(im2hsv(probe./max(max(abs(probe))), 1)));
            title(['Probe function, it  ' num2str(iter)]);
            
            subplot(3,4,4)
            imagesc(abs((angle(probe - input.probe) + pi) - pi));
            title(['Error in phase, ' num2str(iter)]);
            
            subplot(3,4,7)
            imagesc(hsv2rgb(im2hsv(input.probe./max(max(abs(input.probe))), 1)));
            title('True probe');
            
            subplot(3,4,8)
            imagesc(log10(abs(abs(probe - input.probe))));
            title('Error in amplitude');
            
            if(input.animate)
                for pos=1:input.N_pie
                    subplot(3,4,11);
                    Indy = input.positions(pos,1) + (1:input.Ny);
                    Indx = input.positions(pos,2) + (1:input.Nx);
                    imagesc(single(log10(abs(fftshift(fft2(probe.*object(Indy,Indx))).^2))));
                    title(['PIE iteration -reconstruction  ' num2str(iter)]);
                    %colorbar;
                    subplot(3,4,12);
                    imagesc(single(log10(fftshift(input.Amp_exp_norm(:,:,pos)))));
                    title(['PIE iteration  observed dp' num2str(iter)]);
                    %colorbar;
                    drawnow;
                    pause(1/input.N_pie)
                end
                
            else
                pos = 1;
                subplot(3,4,11);
                Indy = input.positions(pos,1) + (1:input.Ny);
                Indx = input.positions(pos,2) + (1:input.Nx);
                imagesc(single(log10(abs(fftshift(fft2(output.u(:,:,pos)))))));
                title(['PIE iteration -reconstruction  ' num2str(iter)]);
                
                subplot(3,4,12);
                imagesc(single(log10(fftshift(input.Amp_exp_norm(:,:,pos)))));
                title(['PIE iteration  observed dp' num2str(iter)]);
                
                drawnow;
            end
            
            % Plot objective function values.
            if input.MAXIT>1 && isfield(output,'change')
                figure(2)
                if strcmp(input.method(end-3:end),'PALM')
                    semilogy(change),xlabel('iteration'),ylabel('objective function value')
                    label = [''];
                    title(['change, ' label])
                    xlim([1 min(ceil(iter/50)*50,input.MAXIT)]);
                else
                    subplot(1,2,1)
                    semilogy(change),xlabel('iteration'),ylabel(['$\| x_{k+1}-x_{k} \|$'])
                    label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
                    title(['change, ' label])
                    subplot(1,2,2)
                    semilogy(gap),xlabel('iteration'),ylabel('$\sum_j \|x_k - \Pi_M x_k\| + \sum_j \|x_k - \Pi_N x_k\|$')
                    label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
                    title(['gap, ' label])
                end
            end
            
        elseif strcmp(input.datatype,'sim')
            % Phase
            figure(1);
            subplot(1,2,1);
            imagesc(squeeze(input.X_obj(1,:)),squeeze(input.Y_obj(:,1)),angle(single(object))); axis xy; axis equal; axis tight; colormap hsv; caxis([input.plotbounds(1) input.plotbounds(2)]);
            title(['Phase, reconstruction, PIE iteration  ' num2str(iter)]); drawnow,
            subplot(1,2,2);
            imagesc(squeeze(input.X_obj(1,:)),squeeze(input.Y_obj(:,1)),angle(input.object_true)); axis xy; axis equal; axis tight; colormap hsv; caxis([input.plotbounds(1) input.plotbounds(2)]);
            title(['Phase, original']); drawnow;
            
            % Amplitude
            figure(2);
            subplot(1,2,1);
            imagesc(squeeze(input.X_obj(1,:)),squeeze(input.Y_obj(:,1)),abs(single(object))); axis xy; axis equal; axis tight; colormap gray;
            title(['Amplitude, reconstruction, PIE iteration  ' num2str(iter)]); drawnow;
            subplot(1,2,2);
            imagesc(squeeze(input.X_obj(1,:)),squeeze(input.Y_obj(:,1)),abs(input.object_true)); axis xy; axis equal; axis tight; colormap gray;
            title(['Amplitude, original']); drawnow;
            
            % Error
            figure(3);
            subplot(1,2,1);
            plot(err(1:iter)); title('Difference map error.'); drawnow;
            subplot(1,2,2);
            plot(chi(1:iter)); title('chi'); drawnow;
        end
    else
        error('Error: Problem with "input.anim".')
    end
end

    function im = c2image(a, varargin)
        % FUNCTION IM = C2IMAGE(A)
        %
        % Returns a RGB image of complex array A where
        % the phase is mapped to hue, and the amplitude
        % is mapped to brightness.
        
        absa = abs(a);
        phasea = angle(a);
        
        % (optional second argument can switch between various plotting modes)
        abs_range = [];
        if nargin==2
            m = varargin{1};
        elseif nargin==3
            m = varargin{1};
            abs_range = varargin{2};
        else
            m = 1;
        end
        
        if isempty(abs_range)
            nabsa = absa/max(max(absa));
        else
            nabsa = (absa - abs_range(1))/(abs_range(2) - abs_range(1));
            nabsa(nabsa < 0) = 0;
            nabsa(nabsa > 1) = 1;
        end
        
        switch m
            case 1
                im_hsv = zeros([size(a) 3]);
                im_hsv(:,:,1) = mod(phasea,2*pi)/(2*pi);
                im_hsv(:,:,2) = 1;
                im_hsv(:,:,3) = nabsa;
                im = hsv2rgb(im_hsv);
            case 2
                im_hsv = ones([size(a) 3]);
                im_hsv(:,:,1) = mod(phasea,2*pi)/(2*pi);
                im_hsv(:,:,2) = nabsa;
                im = hsv2rgb(im_hsv);
        end
        
    end

    %For debugging.
    if input.anim_pause > 0
        pause(input.anim_pause)
    end
end

