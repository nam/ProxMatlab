% This script runs and reports the ptychography experiment for the paper.
% To run, I added its folder to the MATLAB path and call the script from
% the ProxToolbox's "drivers" folder.

global max_warmup_it
global max_it
global repetitions
max_warmup_it = 20
max_it = 1000
repetitions = 10


theDate    = datestr(now,'yymmmdd');
theMethods = {'PALM' 'Rodenburg' 'Thibault' 'Nesterov_PALM' 'Thibault_AP'};
theMethods2 = {'PALM' 'Rodenburg' 'Thibault' 'Nesterov PALM' 'Thibault AP'};

poissonFactor = 0:0.5:5;
global poissonFact
for p=1:length(poissonFactor)
    poissonFact = poissonFactor(p);
    
    % Run the experiments.
    for m=1:length(theMethods)
        diary(['../OutputData/' theDate theMethods{m} '.txt'])
        main_ProxToolbox(['Ptychography_in_' theMethods{m}])
        diary off
    end
    
    %% Make some summary figures figures
    resultsDirectory = ['../OutputData/' datestr(now,'yymmdd') 'Ptychography/'];
    summaryDirectory = ['../OutputData/' datestr(now,'yymmdd') 'PtychographySummary/'];
    if ~exist(summaryDirectory,'dir')
        mkdir(summaryDirectory);
    end
    
    close all;
    theColour = [[0 0 1]; [1 0 0]; [0 0.75 0]; [0.75 0 0.75]; [0.75 0.75 0]];
    methodAlg = {'PALMPALM', 'RodenburgPALM' 'ThibaultRAAR_PALM' 'PALMNesterov_PALM' 'Thibault_APPALM'};
    
    % R-factor
    figNum=1;
    figure(10);
    for m=1:5
        if poissonFact == 0
            thisFig = open([resultsDirectory datestr(now,'yymmmdd') methodAlg{m} 'run1figure' num2str(figNum) '.fig']);
        else
            thisFig = open([resultsDirectory datestr(now,'yymmmdd') methodAlg{m} 'poisson' num2str(input.poissonFactor) 'run1figure' num2str(figNum) '.fig']);
        end
        ax = get(gcf,'Children');
        bx = get(ax(1),'Children');
        ay = get(bx(5),'YData');
        close(thisFig);
        figure(10);
        hold on;
        plot(ay,'Color',theColour(m,:));
    end
    legend(theMethods2);
    title('R-factor against Iterations');
    xlabel('Iterations');
    ylabel('R-factor');
    set(gca,'yscale','log');
    hold off;
    if poissonFact == 0
        saveas(gca,[summaryDirectory 'Rfactor'],'fig');
        saveas(gca,[summaryDirectory 'Rfactor'],'png');
    else
        saveas(gca,[summaryDirectory 'Rfactor' 'poisson' num2str(input.poissonFactor)],'fig');
        saveas(gca,[summaryDirectory 'Rfactor' 'poisson' num2str(input.poissonFactor)],'png');
    end
    
    % RMS Probe
    figNum=3;
    figure(11);
    for m=1:5
        if poissonFact == 0
        	thisFig = open([resultsDirectory datestr(now,'yymmmdd') methodAlg{m} 'run1figure' num2str(figNum) '.fig']);
        else
            thisFig = open([resultsDirectory datestr(now,'yymmmdd') methodAlg{m} 'poisson' num2str(input.poissonFactor) 'run1figure' num2str(figNum) '.fig']);
        end
        ax = get(gcf,'Children');
        bx = get(ax(1),'Children');
        ay = get(bx(5),'YData');
        close(thisFig);
        figure(11);
        hold on;
        plot(ay,'Color',theColour(m,:));
    end
    legend(theMethods2);
    title('RMS Probe Error against iterations');
    xlabel('Iterations');
    ylabel('RMS Probe Error');
    set(gca,'yscale','log');
    hold off;
    if poissonFact == 0
        saveas(gca,[summaryDirectory 'RMSProbe'],'fig');
        saveas(gca,[summaryDirectory 'RMSProbe'],'png');
    else
        saveas(gca,[summaryDirectory 'RMSProbe' 'poisson' num2str(input.poissonFactor)],'fig');
        saveas(gca,[summaryDirectory 'RMSProbe' 'poisson' num2str(input.poissonFactor)],'png');
    end
    
    % Objective value
    figNum=6;
    figure(12);
    for m=1:5
        if poissonFact == 0
            thisFig = open([resultsDirectory datestr(now,'yymmmdd') methodAlg{m}  'run1figure' num2str(figNum) '.fig']);
        else
            thisFig = open([resultsDirectory datestr(now,'yymmmdd') methodAlg{m} 'poisson' num2str(input.poissonFactor) 'run1figure' num2str(figNum) '.fig']);
        end
        ax = get(gcf,'Children');
        bx = get(ax(1),'Children');
        ay = get(bx(5),'YData');
        close(thisFig);
        figure(12);
        hold on;
        plot(ay,'Color',theColour(m,:));
    end
    legend(theMethods2);
    title('Objective Value against iterations');
    xlabel('Iterations');
    ylabel('Objective Function Value');
    set(gca,'yscale','log');
    hold off;
    if poissonFact == 0
        saveas(gca,[summaryDirectory 'ObjectiveValue'],'fig');
        saveas(gca,[summaryDirectory 'ObjectiveValue'],'png');
    else
        saveas(gca,[summaryDirectory 'ObjectiveValue' 'poisson' num2str(input.poissonFactor)],'fig');
        saveas(gca,[summaryDirectory 'ObjectiveValue' 'poisson' num2str(input.poissonFactor)],'png');
    end
    
    % RMS Object
    figNum=4;
    figure(13);
    for m=1:5
        if poissonFact == 0 
            thisFig = open([resultsDirectory datestr(now,'yymmmdd') methodAlg{m} 'run1figure' num2str(figNum) '.fig']);
        else
            thisFig = open([resultsDirectory datestr(now,'yymmmdd') methodAlg{m} 'poisson' num2str(input.poissonFactor) 'run1figure' num2str(figNum) '.fig']);
        end
        ax = get(gcf,'Children');
        bx = get(ax(4),'Children');
        ay = get(bx(5),'YData');
        close(thisFig);
        figure(13);
        hold on;
        plot(ay,'Color',theColour(m,:));
    end
    legend(theMethods2);
    title('RMS Object Error against iterations');
    xlabel('Iterations');
    ylabel('RMS Object Error');
    set(gca,'yscale','log');
    hold off;
    if poissonFact == 0
        saveas(gca,[summaryDirectory 'RMSObject'],'fig');
        saveas(gca,[summaryDirectory 'RMSObject'],'png');
    else
        saveas(gca,[summaryDirectory 'RMSObject' 'poisson' num2str(input.poissonFactor)],'fig');
        saveas(gca,[summaryDirectory 'RMSObject' 'poisson' num2str(input.poissonFactor)],'png');
    end

end
