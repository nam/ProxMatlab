This folder contains the input files used in the ptychography
paper experiments. The filenames for the input files are of the
form:
             Ptychography_in_<algorithm>.m

The script run_ptychography.m runs the complete experiment. That 
is, it will run each of the input files and report the results. 
Other output data is saved in:
       ProxToolbox_v2.0/OutputData/<date>Ptychography
       ProxToolbox_v2.0/OutputData/<date>PtychographySummary

To do this, I run the following command at the command prompt from 
the drivers folder:
   matlab -nodisplay -nosplash -nodesktop -r "addpath('Ptychography/run_ptychography_experiment/');run('run_ptychography_experiment');quit;"


last modified: MKT 10th March 2014




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Another note.....
If you are having segementation faults when trying to run MATLAB using
the "-nodisplay" option, check that your are using the "nodisplay"
version of the graphics file. For some reasons there MATLAB's LaTeX
interpreter doesn't like the "-nodisplay" flag....
