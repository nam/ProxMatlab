#!/bin/bash

function printStart {
  echo "\\begin{table}[htbp]"
  echo " \\caption{Average (Worst) Results for $1}"
  echo " \\vspace{5pt}"
  echo " \\resizebox{\\textwidth}{!}{"
  echo "  \\begin{tabular}{lcccccc} \hline"
  echo "   Algorithm                   & $ F(u^{300})$      & $\\|u^{300}-u^{299}\\|^2$ & RMS-Object       & RMS-Probe        & R-factor         & Time (s)          \\\\ \\hline"
}

function printEnd {
  echo "  \\hline"
  echo "  \\end{tabular}"
  echo " }\\\\"
  echo "\\end{table}"
}

function printLine {
  l="$1 &  "
  l="$l $2 ($3) &  "
  l="$l $4 ($5) &  "
  l="$l $6 ($7) &  "
  l="$l $8 ($9) &  "
  l="$l ${10} (${11}) &  "
  l="$l ${12} (${13}) \\\\"
  echo $l
}

#resultsDir="../../OutputData"

#############################


printStart "noiseless simulated data."
for f in ../../OutputData/*noNoiseLog.txt
do
  alg=${f:39:${#f}-10}
  obj=`grep "Final objective value:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  cha=`grep "Final step-size norm:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSObj=`grep "RMS Error Object:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSPro=`grep "RMS Error Probe:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  Rfact=`grep "R-factor (Phi):" $f | awk '{print $3};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  time=`grep "Total CPU time:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  echo  -e $alg '\t& ' $obj '\t& ' $cha '\t& ' $RMSObj '\t& ' $RMSPro '\t& ' $Rfact '\t& ' $time "\\\\\\\\"
done
printEnd
echo -e "\n\n\n"

#####################

printStart "simulated data with Poisson noise."
#for f in ../../OutputData/*poisson*Log.txt
  f="../../OutputData/*_PHeBIE_poisson*Log.txt"
  alg="PHeBIE"
  obj=`grep "Final objective value:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  cha=`grep "Final step-size norm:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSObj=`grep "RMS Error Object:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSPro=`grep "RMS Error Probe:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  Rfact=`grep "R-factor (Phi):" $f | awk '{print $3};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  time=`grep "Total CPU time:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  echo  -e $alg '\t& ' $obj '\t& ' $cha '\t& ' $RMSObj '\t& ' $RMSPro '\t& ' $Rfact '\t& ' $time "\\\\\\\\"

printStart "simulated data with Poisson noise."
#for f in ../../OutputData/*poisson*Log.txt
  f="../../OutputData/*_PHeBIE_poisson*Log.txt"
  alg="PHeBIE"
  obj=`grep "Final objective value:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  cha=`grep "Final step-size norm:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSObj=`grep "RMS Error Object:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSPro=`grep "RMS Error Probe:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  Rfact=`grep "R-factor (Phi):" $f | awk '{print $3};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  time=`grep "Total CPU time:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  echo  -e $alg '\t& ' $obj '\t& ' $cha '\t& ' $RMSObj '\t& ' $RMSPro '\t& ' $Rfact '\t& ' $time "\\\\\\\\"


  f="../../OutputData/*_PHeBIEptwise_poisson*Log.txt"
  alg="PHeBIE (pointwise)"
  obj=`grep "Final objective value:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  cha=`grep "Final step-size norm:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSObj=`grep "RMS Error Object:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSPro=`grep "RMS Error Probe:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  Rfact=`grep "R-factor (Phi):" $f | awk '{print $3};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  time=`grep "Total CPU time:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  echo  -e $alg '\t& ' $obj '\t& ' $cha '\t& ' $RMSObj '\t& ' $RMSPro '\t& ' $Rfact '\t& ' $time "\\\\\\\\"

  f="../../OutputData/*_Rodenburg_poisson*Log.txt"
  alg="Rodenburg"
  obj=`grep "Final objective value:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  cha=`grep "Final step-size norm:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSObj=`grep "RMS Error Object:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSPro=`grep "RMS Error Probe:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  Rfact=`grep "R-factor (Phi):" $f | awk '{print $3};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  time=`grep "Total CPU time:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  echo  -e $alg '\t& ' $obj '\t& ' $cha '\t& ' $RMSObj '\t& ' $RMSPro '\t& ' $Rfact '\t& ' $time "\\\\\\\\"

  f="../../OutputData/*_Thibault_AP_poisson*Log.txt"
  alg="Thibault_AP"
  obj=`grep "Final objective value:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  cha=`grep "Final step-size norm:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSObj=`grep "RMS Error Object:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSPro=`grep "RMS Error Probe:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  Rfact=`grep "R-factor (Phi):" $f | awk '{print $3};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  time=`grep "Total CPU time:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  echo  -e $alg '\t& ' $obj '\t& ' $cha '\t& ' $RMSObj '\t& ' $RMSPro '\t& ' $Rfact '\t& ' $time "\\\\\\\\"

  f="../../OutputData/*_Thibault_poisson*Log.txt"
  alg="Thibault"
  obj=`grep "Final objective value:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  cha=`grep "Final step-size norm:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSObj=`grep "RMS Error Object:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSPro=`grep "RMS Error Probe:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  Rfact=`grep "R-factor (Phi):" $f | awk '{print $3};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  time=`grep "Total CPU time:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  echo  -e $alg '\t& ' $obj '\t& ' $cha '\t& ' $RMSObj '\t& ' $RMSPro '\t& ' $Rfact '\t& ' $time "\\\\\\\\"

#done
printEnd
echo -e "\n\n\n"

######################3

printStart "noiseless simulated data with over-restrictive pupil constraint."
for f in ../../OutputData/*restrictedPupilLog.txt
do
  alg=${f:39:${#f}-10}
  obj=`grep "Final objective value:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  cha=`grep "Final step-size norm:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSObj=`grep "RMS Error Object:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  RMSPro=`grep "RMS Error Probe:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  Rfact=`grep "R-factor (Phi):" $f | awk '{print $3};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  time=`grep "Total CPU time:" $f | awk '{print $4};' | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count " ("max")"}'`
  echo  -e $alg '\t& ' $obj '\t& ' $cha '\t& ' $RMSObj '\t& ' $RMSPro '\t& ' $Rfact '\t& ' $time "\\\\\\\\"
done
printEnd


