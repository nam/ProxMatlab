%                          cPtychography.m
%                   created on 15th January 2014 by
%                             Matthew Tam
%                       University of Newcastle
%                  last modified on 29th January 2014
%
% DESCRIPTION: In the ptychography problem phi, the object and the probe do
%              not have the same dimensions. This class combines these 
%              three into a single data structure, and hence avoids the 
%              need for global variables. So that code can be reused, 
%              standard MATLAB operators have been overload.
%
%              In particular, the data structure for the  iterates are of 
%              the form [phi,Object,Probe].

classdef cPtychography
    %% Define memeber variables
    properties
        phi;     % phi(:,:,p) is the p-th diffraction pattern.
        object;  % The object (has larger dimensions as phi(:,:,p)).
        probe;   % The probe (has the same dimensions as phi(:,:,p)).
        %phiOnly; % When using RAAR, we only want to do the reflections etc on phi.
    end %properties
    
    %% Define member functions.
    methods
        % Constructor.
        function x = cPtychography(thePhi,theObject,theProbe)
            % The variables.
            x.phi    = thePhi;
            x.object = theObject;
            x.probe  = theProbe;
            % Type of operator overloading (we set this to true to use
            % RAAR).
            %if ~exist('thisPhiOnly','var')
            %    thisPhiOnly = false;
            %end
            %x.phiOnly = thisPhiOnly;
        end
        %% Overloading of standard MATLAB operations
        %  Functions are overload is such as way that overload operation
        %  only apply the function to the phi variabe. In this way, the
        %  class should be backward compatible, and is consistent with
        %  Paer's implementation.
        function x = plus(x1,x2)
           % if x1.phiOnly
           %     x = cPtychography(x1.phi+x2.phi,x1.object,x1.probe);
           % else
                x = cPtchography(x1.phi+x2.phi,x1.object+x2.object,x1.probe+x2.probe);
           % end
        end
        function x = minus(x1,x2)
           % if x1.phiOnly
           %     x = cPtychography(x1.phi-x2.phi,x1.object,x1.probe);
           % else
                x = cPtychography(x1.phi-x2.phi,x1.object-x2.object,x1.probe-x2.probe);
           % end
        end
        function x = times(t,x2) %assume scalar multiplication from the left.
           % if x2.phiOnly
           %     x = cPtychography(t*x2.phi,x2.object,x2.probe);
           % else
                x = cPtychography(t*x2.phi,t*x2.object,t*x2.probe);
           % end
        end
        function x = mrdivide(x1,t) %assume scalar division from the right.
           % if x1.phiOnly
           %     x = cPtychography(x1.phi/t, x2.object/t, x2.probe/t);
           % else
                x = cPtychography(x1.phi/t,x1.object,x1.probe);
           % end
        end
        function y = subsref(x,s)
            if length(s)==1 && strcmp(s.type,'()') %If an index is called.
                y = subsref(x.phi,s);
            else %Otherwise use default behaviour
                y = builtin('subsref',x,s);
            end
        end

        %% Get the value of the objective function (used in PALM).
        function objValue = objective(x1,method_input)
            objValue   = 0;
            thisObject = x1.object;
            for pos=1:method_input.N_pie
                Indy     = method_input.positions(pos,1) + (1:method_input.Ny);
                Indx     = method_input.positions(pos,2) + (1:method_input.Nx);
                objValue = objValue + norm(x1.probe .* thisObject(Indy,Indx) - x1.phi(:,:,pos),'fro')^2;
            end
            %objValue = objValue / (method_input.N_pie * size(thisObject,1) * size(thisObject,2));
        end        
        
        function changeObjProPhi = change(x1,x2,method_input)
            normM = method_input.norm_data;
            changephi = 0;
            if(method_input.Ny==1)||(method_input.Nx==1)
                changephi = (feval('norm',x1.phi-x2.phi, 'fro')/normM)^2;
                %tmp_gap = (feval('norm',tmp1-tmp_u,'fro')/normM)^2;
            else
                for j=1:method_input.product_space_dimension
                    changephi = changephi+(feval('norm',x1.phi(:,:,j)-x2.phi(:,:,j), 'fro')/normM)^2;
                    % compute (||P_SP_Mx-P_Mx||/normM)^2:
                    %tmp_gap = tmp_gap+(feval('norm',tmp1(:,:,j)-tmp_u(:,:,j),'fro')/normM)^2;
                end
            end
            
            changeObjProPhi = changephi;                                                           % ||phi||^2
            changeObjProPhi = changeObjProPhi + (feval('norm',x1.object-x2.object,'fro')/normM)^2; % + ||object||^2
            changeObjProPhi = changeObjProPhi + ( feval('norm',x1.probe-x2.probe,'fro') /normM)^2; % + ||probe||^2
	    changeObjProPhi = sqrt(changeObjProPhi);
        end
            
        
        %% Compute other error(s) of interest.
        function customError = computeErrors(x1,x2,method_input,x3)     
            %meas = zeros(size(method_input.sample_plane));
            %for pos=1:method_input.N_pie
            %    Indy = method_input.positions(pos,1) + (1:method_input.Ny);
            %    Indx = method_input.positions(pos,2) + (1:method_input.Nx);
            %    meas(Indy, Indx) = meas(Indy, Indx)...
            %        + ones(length(Indy), length(Indx)).*method_input.Probe_mask;
            %end
            %objerrorindex = find(meas > method_input.N_pie*(method_input.rmsfraction));
            objerrorindex = method_input.object_support;
            
            %gammaobj = sum(sum(method_input.sample_plane(objerrorindex).*conj(x2.object(objerrorindex)))) /sum(sum(abs(x2.object(objerrorindex).^2)));
            %rmsObj = sum(sum(abs(method_input.sample_plane(objerrorindex) - gammaobj*x2.object(objerrorindex)).^2))/(sum(sum(abs(method_input.sample_plane(objerrorindex)).^2)));
            
            %gammaprobe = sum(sum(method_input.probe.*conj(x2.probe)))/sum(sum(abs(x2.probe).^2));
            %rmsProbe = sum(sum(abs(method_input.probe - gammaprobe*x2.probe).^2))/(sum(sum(abs(method_input.probe).^2)));            
            
            %objectFit = dftregistration(fft2(method_input.sample_plane(objerrorindex)), fft2(x2.object(objerrorindex)))
            probeFit  = dftregistration(fft2(method_input.probe), fft2(x2.probe));           
            
            
            rowShift = probeFit(3);
            colShift = probeFit(4);
            shiftedObject = circshift(x2.object, [rowShift colShift]);
            gammaobj = sum(sum(method_input.sample_plane(objerrorindex).*conj(shiftedObject(objerrorindex))) /sum(sum(abs(shiftedObject).^2)));
            rmsObj = sum(sum(abs(method_input.sample_plane(objerrorindex) - gammaobj*shiftedObject(objerrorindex)).^2))/(sum(sum(abs(method_input.sample_plane(objerrorindex)).^2)));
            
            rmsErrorObj   = rmsObj;
            rmsErrorProbe = probeFit(1);
            %[rmsErrorObj rmsErrorProbe]

            R_factor = 0;
            fnorm = sqrt(method_input.Ny*method_input.Nx);
            for pos=1:method_input.N_pie
                Indy = method_input.positions(pos,1) + (1:method_input.Ny);
                Indx = method_input.positions(pos,2) + (1:method_input.Nx);
                if isfield(method_input,'fmask')
                    R_factor = R_factor + sum(sum( (abs(method_input.Amp_exp_norm(:,:,pos)-abs(fft2(x2.object(Indy,Indx).*x2.probe)/fnorm))) .* method_input.fmask(:,:,pos) ));
                else
                    R_factor = R_factor + sum(sum(abs(method_input.Amp_exp_norm(:,:,pos)-abs(fft2(x2.object(Indy,Indx).*x2.probe)/fnorm))));
                end
            end
            R_factor  = R_factor / sum(sum(sum(method_input.Amp_exp_norm)));
            
%             rmserrorobj = 1;
%             rmserrorprobe = 1;
            
            normM = method_input.norm_data;
            changePhi = 0;
            if(method_input.Ny==1)||(method_input.Nx==1)
                changePhi = (feval('norm',x1.phi-x2.phi, 'fro')/normM)^2;
            else
                for j=1:method_input.product_space_dimension
                    changePhi = changePhi+(feval('norm',x1.phi(:,:,j)-x2.phi(:,:,j), 'fro')/normM)^2;
                end
            end
            
            %customError = [rmserrorobj, rmserrorprobe, changephi, R_factor, changeObjProPhi];
            if strcmp(method_input.ptychography_prox,'Thibault')
                x2.phi = x3.phi;
            end
            objective = feval('objective', x2, method_input);
            
            customError = [rmsErrorObj, rmsErrorProbe, changePhi, R_factor, objective];
            
            
            %customError
            %addpath('Ptychography/efficient_subpixel_registration');
            %dftregistration(fft2(method_input.probe),fft2(x2.probe))
        end
    end %method
end %classdef

