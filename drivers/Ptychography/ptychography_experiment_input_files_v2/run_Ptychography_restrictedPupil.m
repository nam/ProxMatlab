global max_warmup_it
global max_it
global repetitions

max_warmup_it = 20
max_it = 300
repetitions = 5




input_files={'Ptychography_in_PHeBIE_restrictedPupil';...
    'Ptychography_in_Rodenburg_restrictedPupil';...
    'Ptychography_in_Thibault_restrictedPupil';...
    'Ptychography_in_Thibault_AP_restrictedPupil';...
    'Ptychography_in_Thibault_RAAR_restrictedPupil'};

   
    
for i=1:size(input_files)
    input_file = input_files{i}
    diary(['../OutputData/' datestr(now,'yymmdd') input_file 'Log.txt'])
    [output,input] = main_ProxToolbox(char(input_file));
    diary off
end
   



