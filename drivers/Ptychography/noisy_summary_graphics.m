directory = ['../../OutputData/140401Ptychography'];
addpath(directory)
addpath('ProxOperators','Ptychography_data',...
        'Ptychography_data/InputData','Ptychography_data/Robin')

close all
clear all
input_files=['Ptych_Gaenz_n2p0_mp7_PHeBIE_0_in';...
    'Ptych_Gaenz_n2p0_mp7_Rodenbur_in';...
    'Ptych_Gaenz_n2p0_mp7_ThiDRl10_in';...
    'Ptych_Gaenz_n2p0_mp7_ThiDRl50_in'];

 stringlength=32;
 datasets=4;
 linetype=['b   '; '.-c '; '--rs'; ':m+ '];
 figure(1), axes('FontSize', 14), hold on 
 figure(2), axes('FontSize', 14), hold on
 figure(3), axes('FontSize', 14), hold on
 figure(4), axes('FontSize', 14), hold on
 figure(5), axes('FontSize', 14), hold on
 figure(6), axes('FontSize', 14), hold on
 for j=1:datasets-1
    parameter_filename=input_files(j:datasets:stringlength*datasets);
    jj=stringlength;
    while strcmp(parameter_filename(jj),' ')
        jj=jj-1;
    end
    parameter_filename=parameter_filename(1:jj);
    load([parameter_filename '_run1.output.mat'])
    load([parameter_filename '_run1.input.mat'])
    statset(:,1:5)=output.stats.customError;
    statset(:,6)=output.stats.change';
    objectset=output.u_final.object;
    probeset=output.u_final.probe;
    cpu(j)=output.cputime;
    figure(1), semilogy(statset(:,1), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(2), semilogy(statset(:,2), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(3), semilogy(statset(:,3), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(4), semilogy(statset(:,4), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(5), semilogy(statset(:,5), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(6), loglog(statset(:,6), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(j*10), colormap('gray'), 
        subplot(1,2,1),   
        ampObject = log10(abs(objectset));
        %ampObject(ampObject<0.5) = ampObject(ceil(end/2),ceil(end/2));
        imagesc(ampObject), axis equal tight, colorbar('FontSize', 14)
        % imagesc(abs(objectset)), axis('off'), caxis([-.01, .01]), 
        title('Best object approximation, amplitude '),  xlabel(parameter_filename)
        subplot(1,2,2),
        imagesc(angle(objectset)), axis equal tight, colorbar('FontSize', 14)
        % imagesc(abs(objectset)), axis('off'), caxis([-.01, .01]), 
        title('Best object approximation, phase '), xlabel(parameter_filename)

    figure(j*11), axes('FontSize', 14), colormap('gray'); 
        imagesc(hsv2rgb(im2hsv(probeset./max(max(abs(probeset))), 1)));
        title('Best probe approximation'),  xlabel(parameter_filename)
        axis equal tight, colorbar('FontSize', 14);
        % imagesc(abs(probeset(:,:,j))), axis('off'), caxis([0, 2000]),      
 end
lgnd1=['PHeBIE: ' num2str(cpu(1)) 's']; 
lgnd2=['Maiden-Rodenburg: ' num2str(cpu(2)) 's']; 
lgnd3=['Thibault: ' num2str(cpu(3)) 's']; 
% lgnd4=['Thibault-DRl: ' num2str(cpu(4)) 's']; 


% figure(1), legend('PHeBIE', 'PHeBIE-Nesterov', 'Rodenburg', 'Thibault-DRl', 'Thibault'),  hold off
% figure(2), legend('PHeBIE', 'PHeBIE-Nesterov', 'Rodenburg', 'Thibault-DRl', 'Thibault'), hold off
% figure(3), legend('PHeBIE', 'PHeBIE-Nesterov', 'Rodenburg', 'Thibault-DRl', 'Thibault'), hold off
% figure(4), legend('PHeBIE', 'PHeBIE-Nesterov', 'Rodenburg', 'Thibault-DRl', 'Thibault'), hold off
figure(1), legend(lgnd1, lgnd2, lgnd3),  title('rms error object'), hold off
figure(2), legend(lgnd1, lgnd2, lgnd3),  title('rms error probe'), hold off
figure(3), legend(lgnd1, lgnd2, lgnd3),  title('change \Phi'), hold off
figure(4), legend(lgnd1, lgnd2, lgnd3),  title('R-factor'), hold off
figure(5), legend(lgnd1, lgnd2, lgnd3),  title('Objective Value'), ylabel('Objective Value'), hold off
figure(6), legend(lgnd1, lgnd2, lgnd3)  
figure(6), xlabel('iteration'),ylabel('norm iterate change'), title('Critical Point Convergence'), hold off
%     figure(100), axes('FontSize', 14), colormap('gray'), 
%         subplot(1,2,1)
%         ampObject = log10(abs(u.object));
%         %ampObject(ampObject<0.5) = ampObject(ceil(end/2),ceil(end/2));
%         imagesc(ampObject), axis equal tight, colorbar('FontSize', 14)
%         % imagesc(abs(objectset(:,:,j))), axis('off'), caxis([-.01, .01]), 
%         title('Best object approximation, amplitude '),  xlabel(parameter_filename)
%         subplot(1,2,2)
%         imagesc(angle(u.object)), axis equal tight, colorbar('FontSize', 14)
%         % imagesc(abs(objectset(:,:,j))), axis('off'), caxis([-.01, .01]), 
%         title('Best object approximation, phase '), xlabel(parameter_filename)
% 
%     figure(101), axes('FontSize', 14), colormap, 
%         imagesc(hsv2rgb(im2hsv(u.probe./max(max(abs(u.probe))), 1)));
%         title('Best probe approximation'),  xlabel(parameter_filename)
%         axis equal tight, colorbar('FontSize', 14);
%         % imagesc(abs(probeset(:,:,j))), axis('off'), caxis([0, 2000]),      
 
