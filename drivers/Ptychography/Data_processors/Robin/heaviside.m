function val = heaviside(M)
val = (1 + sign(M))/2;