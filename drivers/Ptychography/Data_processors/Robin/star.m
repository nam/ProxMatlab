function [s]=star( r, a,  N )
	s=linspace( -N/2, N/2-1, N);
	s=repmat(s,N,1);
	s=s+i*s';
	
	s=(abs(s)<r).*(sin(a*angle(s))>0);
