function P = PCDI(P_in)
% load data-set
% Directory of input data
P.data_dir = P_in.data_dir;
% Datafile of input data
P.datafile = P_in.datafile;
datafile =P_in.datafile;

% Load experimental data
cd(P.data_dir);
load(P.datafile,'P');

%% compare which parameters in P will be overwritten by P_in
%  and adapt input parameters from structure P_in
names_P = fieldnames(P_in);
fprintf('The following list of fields within P will be overwritten by input P_in:\n')
for akk = 1:length(names_P)
    if isfield(P,names_P{akk})
        fprintf(['<',names_P{akk},'>, ']);
    end
    eval(['P.',names_P{akk},'=','P_in.',names_P{akk},';']);
end
fprintf('\n');
% now P_in is unnecessary - clear it for memory
clear P_in;
P.date = datestr(now,30);
% calculate dimensions
% sample plane
P.d1x = P.z12*P.lambda/(P.Nx*P.d2x);
P.d1y = P.z12*P.lambda/(P.Ny*P.d2y);
% q spacing in detector plane
P.dq2x = 2*pi/(P.Nx*P.d1x);
P.dq2y = 2*pi/(P.Ny*P.d1y);

if ~isfield(P,'prop_method')
    P.prop_method ='far_field';
end

if strcmp(P.prop_method,'near_field')
    P.d1x = P.d2x;
    P.d1y = P.d2y;
end

%GPU calculation ?
if ~isfield(P,'GPU_calculate')
    P.GPU_calculate = false;
end

%check which ptychographic reconstruction method should be used
% 'DM'/'pPIE' = P.Thibault et al. Ultramicroscopy,109,338-343,2009
% 'ePIE' = Andrew M. Rodenburg et al. Ultramicroscopy,109,1256-1262,2009
if ~isfield(P,'PIE_method')
    P.PIE_method = 'DM';
end
if strcmp(P.PIE_method,'PIE')
    P.PIE_method = 'DM';
end
fprintf('Method # %s \n',P.PIE_method);

% check if probe change should be stopped
if ~isfield(P,'N_probe_change_stop')
    P.N_probe_change_stop = inf;
end

% check if phase force should be stopped
if ~isfield(P,'phase_force_zero_region_stop')
    P.phase_force_zero_region_stop = inf;
end

% check if clipping of object should be stopped
if ~isfield(P,'clip_object_stop')
    P.clip_object_stop = inf;
end


%calculate traditional chi^2 value ? typically lengthy calculation
%necessary
if ~isfield(P,'CHI_calculate')
    P.CHI_calculate = false;
end


current_path = pwd;

if P.save_data == true
    disp(['Current path is: ' pwd]);
    disp('Data will be saved.')
    tmp = findstr(datafile,'.');
    tmp = tmp-1;
    if ~isfield(P,'save_dir')
        save_dir = [datestr(now,30),'_',datafile(1:tmp) '_tau_' num2str(P.tau)];
    else
        save_dir = P.save_dir;
    end
    clear tmp;
    mkdir(save_dir);
    cd(save_dir);
    P.save_dir = save_dir;
else
    disp('Data is not saved.');
end


%% Parameter import and declaration

% Take probe from previous calculation?
if isfield(P,'precalculated_probe')
    precalculated_probe = P.precalculated_probe;
else
    precalculated_probe = false;
end

% Take object from previous calculation?
if isfield(P,'precalculated_object')
    precalculated_object = P.precalculated_object;
else
    precalculated_object = false;
end

% force phase of object in specific region to zero
if isfield(P,'phase_force_zero_region')
    phase_force_zero_region = P.phase_force_zero_region;
    if phase_force_zero_region
        zero_pha_supp = 0*P.zero_pha_supp;
        zero_pha_supp = P.zero_pha_supp>0;
        not_zero_pha_supp = 0*P.zero_pha_supp;
        not_zero_pha_supp = P.zero_pha_supp<=0;
    end
else
    phase_force_zero_region = false;
end

% Plot mask in sample plane
if ~isfield(P,'plot_mask')
    P.plot_mask = false;
end

if ~isfield(P,'probe_mask_gamma')
    P.probe_mask_gamma = 1;
end

if  ~isfield(P,'Probe_filt')
    P.Probe_filt = false;
end

% relative radius of ideal circular probe mask with respect to radius of
% pinhole used for initial simulation of the probe function
beta_sam = 0.9*P.Nx*P.d1x/2/P.R*P.probe_mask_gamma;
P.beta_sam = beta_sam;

% Fresnel number (useful, if a pinhole is used to define the beam)
F = (2*P.R)^2/(P.lambda*P.z01);
P.F = F;

% cfactor for probe
cfact = 0.0001*P.ny*P.nx;
P.cfact = cfact;


%% Generate coordinate systems (NEW)

% get positions
switch P.scan_type
    case 'round_roi'
        PP = P.round_roi;
        PP.dx_spec = P.d1x;
    case 'custom'
        PP.d1x = P.d1x;
        PP.d1y = P.d1y;
        PP.positions = P.positions;
    otherwise % standard is rectangular scan mesh
        PP.nx =P.nx;
        PP.ny = P.ny;
        PP.d1x = P.d1x;
        PP.d1y = P.d1y;
        PP.pie_step = P.pie_step;
end
PP.scan_type = P.scan_type;
P.positions = ptycho_prep_pos(PP);
clear PP;

% Adjust objetc size accordingly
object_size = [P.Ny,P.Nx] + max(P.positions,[],1);
P.object_size = object_size;

% NOTE: norm_avg(psi)^2 = mean intensity in psi
% numel(psi)*norm_avg(psi)^2 = total intensity in psi
% norm_avg is a norm that acts on wave fields, not on intensities
% norm_avg(psi) has no direct physical meaning (like mean of amplitude
% etc.)

% Specimen plane (E1)/ probe plane (as long as direct propagation is used) -
[X_1,Y_1] = meshgrid(((1:P.Nx)-floor(P.Nx/2)-1)*P.d1x,((1:P.Ny)-floor(P.Ny/2)-1)*P.d1y);

% Extended coordinate system for object plane
[X_obj,Y_obj] = meshgrid((1:P.object_size(2))*P.d1x,(1:P.object_size(1))*P.d1y);

% Average total intensity in experimental diffraction pattern
I_avg = mean(mean(sum(sum(P.I_exp,1),2),3),4);
fprintf('Average intensity of single frame is %3.2e photons.\n',I_avg);
P.I_avg = I_avg;
% Maximum average intensity in a single experimental frame
I_max_avg = max(max(sum(sum(P.I_exp,1),2),[],3),[],4) / (P.Nx*P.Ny);
P.I_max_avg= I_max_avg;
% Generate cell array of normalized intensities
% Normalizeation such, that for I = I_max (as matrix) the average pixel
% intensity is 1 and total is Nx*Ny, for all other I the values are lower
Amp_exp_norm = zeros(size(P.I_exp,1),size(P.I_exp,2),P.N_pie);
fmask = ones(size(P.I_exp,1),size(P.I_exp,2),P.N_pie);

cx=floor(size(P.I_exp,2)/2);
cy=floor(size(P.I_exp,1)/2);
% Removal of residual noise in the data is done here
ind=1;
for n_slow = 1:P.ny
    for n_fast = 1:P.nx
        %check if semi-transparent central stop was used. scale intensity
        %accordingly
        
        if isfield(P,'bs_mask')
            dat = P.I_exp(:,:,n_slow,n_fast);
            dat(logical(P.bs_mask)) = dat(logical(P.bs_mask))*P.bs_fact;
            P.I_exp(:,:,n_slow,n_fast) = dat;
        end
        
        % save time and get experimental data into matlab fft2 non-centered
        % system
        if strcmp(P.prop_method,'far_field')
            dat=rot90(rot90(circshift(P.I_exp(:,:,n_slow,n_fast),-[cy cx])));
            %dat=ifftshift(P.I_exp(:,:,n_slow,n_fast));
        else
            dat=P.I_exp(:,:,n_slow,n_fast);
        end
        % normalization such that average intensity of data is on the order
        % of 1
        Amp_exp_norm(:,:,ind) = sqrt(dat/I_max_avg);
        
        %check for pixelmask
        if isfield(P,'fmask')
            if strcmp(P.prop_method,'far_field')
                fmask(:,:,ind) = rot90(rot90(circshift(double(P.fmask(:,:,n_slow,n_fast)),-[cy cx])));
                %fmask(:,:,ind) =ifftshift(double(P.fmask(:,:,n_slow,n_fast)));
            else
                fmask(:,:,ind) = double(P.fmask(:,:,n_slow,n_fast));
            end
        end
        ind = ind+1;
    end
end
if isfield(P,'fmask')
    P=rmfield(P,{'fmask'});
end
P.I_exp_sm=sum(sum(P.I_exp,3),4);
P.I_exp = P.I_exp(:,:,1);
if precalculated_probe
    if isfield(P,'probe_guess')
        Probe = P.probe_guess;
        fprintf('.');
    else
        load('.../reconstruction_100822_182654__256x256.mat');
        Probe = probe_guess;
    end
else
    switch P.calculate_probe
        case 'ellipsis'
            probe = abs(imrotate(ellipsis(X_1,Y_1,0,0,P.probe.a,P.probe.b),P.probe.alpha,'crop'));
            
            
            % propagation from pinhole to sample plane
            Probe = prop_nf_pa(probe,P.lambda,P.z01,P.d1x,P.d1y);
            
            %normalization, such that average intensity in Probe pixels is one
            Probe = Probe/norm_avg(Probe);
        case 'gauss'
            Probe = GAUSSU(sqrt(X_1.^2+Y_1.^2),P.z01,P.lambda,P.probe.FWHM/(2*sqrt(log(2))));
        case 'gauss_sep_hv' %gauss which is separable in hor. & vert. direction
            %calculate horizontal part
            Probe_h = GAUSSU(abs(X_1),P.z01,P.lambda,P.probe.FWHM_h/(2*sqrt(log(2))));
            %calculate vertical part
            Probe_v = GAUSSU(abs(Y_1),P.z01,P.lambda,P.probe.FWHM_v/(2*sqrt(log(2))));
            Probe = Probe_v.*Probe_h;
            clear Probe_v Probe_h;
        case 'gauss_flat_phase' % gauss in sample plane with flat phase
            Probe = GAUSSU(sqrt(X_1.^2+Y_1.^2),0,P.lambda,P.probe.FWHM/(2*sqrt(log(2))));
        case 'gauss_flat_phase_sep_hv' %gauss which is separable in hor. & vert. direction
            %calculate horizontal part
            Probe_h = GAUSSU(abs(X_1),0,P.lambda,P.probe.FWHM_h/(2*sqrt(log(2))));
            %calculate vertical part
            Probe_v = GAUSSU(abs(Y_1),0,P.lambda,P.probe.FWHM_v/(2*sqrt(log(2))));
            Probe = Probe_v.*Probe_h;
            clear Probe_v Probe_h;
        otherwise
            error('Cannot decide which probe I shall calculate!');
    end
end



if precalculated_object
    if isfield(P,'obj_guess')
        obj_guess = P.obj_guess;
    else
        error('''obj_guess'' not found!')
    end
else
    if P.random_start == true
        % note that this is only a special form of a random object field with
        % relatively weak random amplitude, alternatively one could use
        % stronger random amplitudes. The initial probe function is the same at
        % all positions.
        obj_guess = (0.05*rand(P.object_size) + 0.95).*exp(1i*(2*pi*rand(P.object_size) - pi)/20);
    else
        obj_guess = ones(P.object_size);
    end
end



% Generate initial guess for exit wave functions
datasize = [P.Ny,P.Nx,P.ny*P.nx];

switch  P.PIE_method
    case 'ePIE'
    otherwise
        psi = ones(datasize,'single');
        
        for pos=1:P.N_pie
            Indy = P.positions(pos,1) + (1:P.Ny);
            Indx = P.positions(pos,2) + (1:P.Nx);
            psi(:,:,pos) = Probe.*obj_guess(Indy,Indx);
        end  
end
object = obj_guess;

if P.plot_initial
    
    figure;
    imagesc(squeeze(X_1(1,:)),squeeze(Y_1(:,1)),hsv2rgb(im2hsv(Probe./max(max(abs(Probe))),1)));
    axis xy equal tight;
    xlabel(['x [m]']); ylabel(['y [m]']);
    title(['Probe-guess, in probe plane (F = ',num2str(F,2),')']);
    
    figure;
    imagesc(squeeze(X_1(1,:)),squeeze(Y_1(:,1)),angle(fftshift(ifft2(ifftshift(mean(mean(P.I_exp,3),4))))));
    axis xy; axis equal; axis tight; axis image; colormap(hsv); colorbar;
    title('Average autocorrelation function (angle)');
    
end

% Generating probe masks
if P.switch_probemask
    P.Probe_mask = circ(X_1,Y_1,0,0,beta_sam*P.R);
else
    P.Probe_mask = ones(size(Probe));
end

% Generating plot mask
if P.plot_mask
    plot_ind_2d = {floor(2*datasize(1)/3):(P.object_size(1)-floor(2*datasize(1)/3)),floor(2*datasize(2)/3):(P.object_size(2)-floor(2*datasize(2)/3))};
    if isempty(plot_ind_2d{1}) || isempty(plot_ind_2d{2})
        plot_ind_2d = {floor(1*datasize(1)/3):(P.object_size(1)-floor(1*datasize(1)/3)),floor(1*datasize(2)/3):(P.object_size(2)-floor(1*datasize(2)/3))};
    end
    plot_mask = zeros(P.object_size);
    plot_mask(plot_ind_2d{:})=1;
    plot_ind = find(plot_mask==1);
end

%% Setting up initial values and parameters for SXDM reconstruction

err(P.N_it) = 0;
power(P.N_it) = 0;
chi(P.N_it) = 0;
power_bs(P.N_it) = 0;
power_bs_current(P.N_it) = 0;
numav = 0;
obj_avg = zeros(P.object_size);
Probe_avg = zeros(P.Ny,P.Nx);
dummy_obj_ones = ones(P.object_size);


% SXDM core
close all;
for it=1:P.N_it
    fprintf('Iteration # %d of %d\n',it,P.N_it);
    tic;
    switch  P.PIE_method
        case 'DM'
            % Overlap constraint loops
            
            if it > (P.N_probe_change_start-1) && it <= (P.N_probe_change_stop) % probe refinement and object refinement
                
                prch0 = 0;
                for it_nested=1:P.N_nested_max
                    
                    % new object
                    % -------------------------------------------------------------
                    obj_enum = 1e-8*dummy_obj_ones;
                    obj_denom = 1e-8*dummy_obj_ones;
                    I_Probe = abs(Probe).^2;
                    c_Probe = conj(Probe);
                    for pos=1:P.N_pie
                        Indy = P.positions(pos,1) + (1:P.Ny);
                        Indx = P.positions(pos,2) + (1:P.Nx);
                        
                        obj_enum(Indy,Indx) = obj_enum(Indy,Indx) + c_Probe.*psi(:,:,pos);
                        obj_denom(Indy,Indx) = obj_denom(Indy,Indx) + I_Probe;
                    end
                    object = obj_enum./obj_denom;
                    
                    if P.clip_object && it <= P.clip_object_stop
                        % prohibit values larger 1
                        abs_object = abs(object);
                        high = (abs_object > P.trans_max_true);
                        low = (abs_object < P.trans_min_true);
                        object = (1-low).*(1-high).*object + (low*P.trans_min_true + high*P.trans_max_true).*object./abs_object;
                    end
                    
                    if phase_force_zero_region && it <= P.phase_force_zero_region_stop
                        pha_obj = angle(object);
                        amp_obj = abs(object);
                        pha_obj = min(pha_obj,0).*not_zero_pha_supp+pha_obj.*(1-P.zero_pha_gamma(min(length(P.zero_pha_gamma),it))*P.zero_pha_supp).*zero_pha_supp;
                        object =amp_obj.*exp(sqrt(-1).*pha_obj);
                    end
                    
                    % -------------------------------------------------------------
                    % New probe
                    % -------------------------------------------------------------
                    Probe_enum = P.cfact*Probe;
                    Probe_denom = P.cfact;
                    
                    for pos=1:P.N_pie
                        Indy = P.positions(pos,1) + (1:P.Ny);
                        Indx = P.positions(pos,2) + (1:P.Nx);
                        
                        Probe_enum = Probe_enum + psi(:,:,pos) .* conj(object(Indy,Indx));
                        Probe_denom = Probe_denom + abs(object(Indy,Indx)).^2;
                    end
                    
                    Probe_old = Probe;
                    Probe = Probe_enum./Probe_denom;
                    
                    if it > P.N_probe_mask_start-1
                        
                        if P.switch_probemask
                            %Mask in sample plane
                            Probe_norm = norm_avg(Probe);
                            Probe = Probe.*P.Probe_mask;
                            Probe = Probe .* Probe_norm/norm_avg(Probe);
                        end
                        
                    end
                    
                    prch = norm((Probe - Probe_old));
                    fprintf('Change in probe: %f\n',prch);
                    
                    if prch0 == 0
                        prch0 = prch;
                    elseif prch < 0.1*prch0 % comparison of probe change relative to first change in nested loop
                        break
                    end
                    % -------------------------------------------------------------
                    
                end
                
                
            else % Just object refinement
                
                % New object
                % -------------------------------------------------------------
                obj_enum = 1e-8*dummy_obj_ones;
                obj_denom = 1e-8*dummy_obj_ones;
                
                I_Probe = abs(Probe).^2;
                c_Probe = conj(Probe);
                
                for pos=1:P.N_pie
                    
                    Indy = P.positions(pos,1) + (1:P.Ny);
                    Indx = P.positions(pos,2) + (1:P.Nx);
                    
                    obj_enum(Indy,Indx) = obj_enum(Indy,Indx) + c_Probe.*psi(:,:,pos);
                    obj_denom(Indy,Indx) = obj_denom(Indy,Indx) + I_Probe;
                end
                
                object = obj_enum./obj_denom;
                
                if P.clip_object
                    % Prohibit values larger 1
                    abs_object = abs(object);
                    high = (abs_object > P.trans_max_true);
                    low = (abs_object < P.trans_min_true);
                    object = (1-low).*(1-high).*object + (low*P.trans_min_true + high*P.trans_max_true).*object./abs_object;
                end
                
                % -------------------------------------------------------------
                
            end
            
            
            % Modulus constraint loop
            err_current = 0;
            power_current = 0;
            chi_current = 0;
            
            counter = 0;
            fnorm = sqrt(P.Ny*P.Nx);
            
            
            for pos=1:P.N_pie
                Indy = P.positions(pos,1) + (1:P.Ny);
                Indx = P.positions(pos,2) + (1:P.Nx);
                
                p1 = Probe.*object(Indy,Indx);
                
                if P.GPU_calculate
                    f = double(fft2(GPUsingle((2*p1 - psi(:,:,pos))/fnorm)));
                else
                    if strcmp(P.prop_method,'far_field')
                        f = fft2(2*p1 - psi(:,:,pos))/fnorm;
                    elseif strcmp(P.prop_method,'near_field')
                        %prop_nf_pa(im,lambda,Delta_z,dx,dy)
                        f = prop_nf_pa(2*p1 - psi(:,:,pos),P.lambda,P.z_eff,P.d1x,P.d2x)/fnorm;
                    end
                end
                
                af = abs(f);
                ph = f ./ (af + 1e-10);
                
                if  P.CHI_calculate
                    % Traditional chi-squared definition without sigma_sq
                    chi_value = sqrt(sum(sum((af.^2 - Amp_exp_norm(:,:,pos).^2).^2))/(P.Ny*P.Nx));
                    chi_current = chi_current + chi_value/P.N_pie;
                else
                    chi_current = chi_current+0;
                end
                
                % Amplitude deviation
                fdev = (af - Amp_exp_norm(:,:,pos));
                % Squared amplitude deviation
                fdev2 = fmask(:,:,pos).*fdev.^2;
                
                % Pierres power definition
                power_value = sum(sum(fdev2))/(P.Ny*P.Nx)*I_max_avg;
                power_current = power_current + power_value/P.N_pie;
                
                % Relaxed Fourier modulus projection
                if (power_value) > (P.tau*0.5)
                    renorm = sqrt(P.tau*0.5/power_value);
                    af =af.*(1-fmask(:,:,pos))+ fmask(:,:,pos).*(Amp_exp_norm(:,:,pos) + renorm*fdev);
                    counter = counter+1;
                end
                
                
                if P.GPU_calculate
                    p2 = fnorm*double(ifft2(GPUsingle(af.*ph)));
                else
                    if strcmp(P.prop_method,'far_field')
                        p2 = fnorm*ifft2(af.*ph);
                    elseif strcmp(P.prop_method,'near_field')
                        p2 = fnorm*prop_nf_pa(af.*ph,P.lambda,-P.z_eff,P.d1x,P.d2x);
                    end
                end
                
                df = p2 - p1;
                psi(:,:,pos) = psi(:,:,pos) + df;
                
                % Difference map error
                err_current = err_current + sum(sum(abs(df.^2)));
                
            end
        case 'DM_par'
            % Overlap constraint loops
            
            if it > (P.N_probe_change_start-1) && it <= (P.N_probe_change_stop) % probe refinement and object refinement
                
                prch0 = 0;
                for it_nested=1:P.N_nested_max
                    
                    % new object
                    % -------------------------------------------------------------
                    obj_enum = 1e-8*dummy_obj_ones;
                    obj_denom = 1e-8*dummy_obj_ones;
                    I_Probe = abs(Probe).^2;
                    c_Probe = conj(Probe);
                    for pos=1:P.N_pie
                        Indy = P.positions(pos,1) + (1:P.Ny);
                        Indx = P.positions(pos,2) + (1:P.Nx);
                        
                        obj_enum(Indy,Indx) = obj_enum(Indy,Indx) + c_Probe.*psi(:,:,pos);
                        obj_denom(Indy,Indx) = obj_denom(Indy,Indx) + I_Probe;
                    end
                    object = obj_enum./obj_denom;
                    
                    if P.clip_object && it <= P.clip_object_stop
                        % prohibit values larger 1
                        abs_object = abs(object);
                        high = (abs_object > P.trans_max_true);
                        low = (abs_object < P.trans_min_true);
                        object = (1-low).*(1-high).*object + (low*P.trans_min_true + high*P.trans_max_true).*object./abs_object;
                    end
                    
                    if phase_force_zero_region && it <= P.phase_force_zero_region_stop
                        pha_obj = angle(object);
                        amp_obj = abs(object);
                        pha_obj = min(pha_obj,0).*not_zero_pha_supp+pha_obj.*(1-P.zero_pha_gamma(min(length(P.zero_pha_gamma),it))*P.zero_pha_supp).*zero_pha_supp;
                        object =amp_obj.*exp(sqrt(-1).*pha_obj);
                    end
                    
                    % -------------------------------------------------------------
                    % New probe
                    % -------------------------------------------------------------
                    Probe_enum = P.cfact*Probe;
                    Probe_denom = P.cfact;
                    
                    for pos=1:P.N_pie
                        Indy = P.positions(pos,1) + (1:P.Ny);
                        Indx = P.positions(pos,2) + (1:P.Nx);
                        
                        Probe_enum = Probe_enum + psi(:,:,pos) .* conj(object(Indy,Indx));
                        Probe_denom = Probe_denom + abs(object(Indy,Indx)).^2;
                    end
                    
                    Probe_old = Probe;
                    Probe = Probe_enum./Probe_denom;
                    
                    if it > P.N_probe_mask_start-1
                        
                        if P.switch_probemask
                            %Mask in sample plane
                            Probe_norm = norm_avg(Probe);
                            Probe = Probe.*P.Probe_mask;
                            Probe = Probe .* Probe_norm/norm_avg(Probe);
                        end
                        
                    end
                    
                    prch = norm((Probe - Probe_old));
                    fprintf('Change in probe: %f\n',prch);
                    
                    if prch0 == 0
                        prch0 = prch;
                    elseif prch < 0.1*prch0 % comparison of probe change relative to first change in nested loop
                        break
                    end
                    % -------------------------------------------------------------
                    
                end
                
                
            else % Just object refinement
                
                % New object
                % -------------------------------------------------------------
                obj_enum = 1e-8*dummy_obj_ones;
                obj_denom = 1e-8*dummy_obj_ones;
                
                I_Probe = abs(Probe).^2;
                c_Probe = conj(Probe);
                
                for pos=1:P.N_pie
                    
                    Indy = P.positions(pos,1) + (1:P.Ny);
                    Indx = P.positions(pos,2) + (1:P.Nx);
                    
                    obj_enum(Indy,Indx) = obj_enum(Indy,Indx) + c_Probe.*psi(:,:,pos);
                    obj_denom(Indy,Indx) = obj_denom(Indy,Indx) + I_Probe;
                end
                
                object = obj_enum./obj_denom;
                
                if P.clip_object
                    % Prohibit values larger 1
                    abs_object = abs(object);
                    high = (abs_object > P.trans_max_true);
                    low = (abs_object < P.trans_min_true);
                    object = (1-low).*(1-high).*object + (low*P.trans_min_true + high*P.trans_max_true).*object./abs_object;
                end
                
                % -------------------------------------------------------------
                
            end
            
            
            % Modulus constraint loop
            err_current = 0;
            Err_current = zeros(1,P.N_pie);
            power_current = 0;
            Power_current = zeros(1,P.N_pie);
            chi_current = 0;
            
            counter = 0;
            fnorm = sqrt(P.Ny*P.Nx);
            
            positions_x = P.positions(:,2);
            positions_y = P.positions(:,1);
            
            parfor pos=1:P.N_pie
                Indy = positions_y(pos) + (1:P.Ny);
                Indx = positions_x(pos) + (1:P.Nx);
                
                p1 = Probe.*object(Indy,Indx);
                

                if strcmp(P.prop_method,'far_field')
                    f = fft2(2*p1 - psi(:,:,pos))/fnorm;
                elseif strcmp(P.prop_method,'near_field')
                    %prop_nf_pa(im,lambda,Delta_z,dx,dy)
                    f = prop_nf_pa(2*p1 - psi(:,:,pos),P.lambda,P.z_eff,P.d1x,P.d2x)/fnorm;
                end

                
                af = abs(f);
                ph = f ./ (af + 1e-10);
                
                if  P.CHI_calculate
                    % Traditional chi-squared definition without sigma_sq
                    chi_value = sqrt(sum(sum((af.^2 - Amp_exp_norm(:,:,pos).^2).^2))/(P.Ny*P.Nx));
                    chi_current = chi_current + chi_value/P.N_pie;
                else
                    chi_current = chi_current+0;
                end
                
                % Amplitude deviation
                fdev = (af - Amp_exp_norm(:,:,pos));
                % Squared amplitude deviation
                fdev2 = fmask(:,:,pos).*fdev.^2;
                
                % Pierres power definition
                power_value = sum(sum(fdev2))/(P.Ny*P.Nx)*I_max_avg;
                Power_current(pos) = power_value/P.N_pie;
                
                % Relaxed Fourier modulus projection
                if (power_value) > (P.tau*0.5)
                    renorm = sqrt(P.tau*0.5/power_value);
                    af =af.*(1-fmask(:,:,pos))+ fmask(:,:,pos).*(Amp_exp_norm(:,:,pos) + renorm*fdev);
                    counter = counter+1;
                end
                
                
                if P.GPU_calculate
                    p2 = fnorm*double(ifft2(GPUsingle(af.*ph)));
                else
                    if strcmp(P.prop_method,'far_field')
                        p2 = fnorm*ifft2(af.*ph);
                    elseif strcmp(P.prop_method,'near_field')
                        p2 = fnorm*prop_nf_pa(af.*ph,P.lambda,-P.z_eff,P.d1x,P.d2x);
                    end
                end
                
                df = p2 - p1;
                psi(:,:,pos) = psi(:,:,pos) + df;
                
                Err_current(pos) = sum(sum(abs(df.^2)));
                
            end
             % Difference map error
             err_current =sum(Err_current);
             % Pierres power definition
             power_current =sum(Power_current);
             power_value =Power_current(end);
        case 'ePIE'
            % Modulus constraint loop
            err_current = 0;
            power_current = 0;
            chi_current = 0;
            power_bs_current = 0;
            
            counter = 0;
            fnorm = sqrt(P.Ny*P.Nx);
            Probe_old = Probe;
            rand_pos = randperm(P.N_pie);
            for Pos=1:P.N_pie
                pos = rand_pos(Pos);
                Indy = P.positions(pos,1) + (1:P.Ny);
                Indx = P.positions(pos,2) + (1:P.Nx);
                
                p1 = object(Indy,Indx).*Probe;
                
                if P.GPU_calculate
                    f = double(fft2(GPUsingle(p1))/fnorm);
                else
                    if strcmp(P.prop_method,'far_field')
                        f = fft2(p1)/fnorm;
                    elseif strcmp(P.prop_method,'near_field')
                        %prop_nf_pa(im,lambda,Delta_z,dx,dy)
                        f = prop_nf_pa(p1,P.lambda,P.z_eff,P.d1x,P.d2x)/fnorm;
                    end
                end
                
                af = abs(f);
                ph = f ./ (af + 1e-8);
                
                if  P.CHI_calculate
                    % Traditional chi-squared definition without sigma_sq
                    chi_value = sqrt(sum(sum((af.^2 - Amp_exp_norm(:,:,pos).^2).^2))/(P.Ny*P.Nx));
                    chi_current = chi_current + chi_value/P.N_pie;
                else
                    chi_current = chi_current+0;
                end
                
                % Amplitude deviation
                fdev = (af - Amp_exp_norm(:,:,pos));
                % Squared amplitude deviation
                fdev2 = fmask(:,:,pos).*fdev.^2;
                
                % Pierres power definition
                power_value = sum(sum(fdev2))/(P.Ny*P.Nx)*I_max_avg;
                power_current = power_current + power_value/P.N_pie;
                
                if isfield(P,'bs_mask')
                    power_bs_value = sum(sum(P.bs_mask.*fdev.^2))/(P.Ny*P.Nx)*I_max_avg;
                    power_bs_current = power_bs_current + power_bs_value/P.N_pie;
                end
                
                %Relaxed Fourier modulus projection
                if (power_value) > (P.tau*0.5)
                    renorm = sqrt(P.tau*0.5/power_value);
                    af =af.*(1-fmask(:,:,pos))+ fmask(:,:,pos).*(Amp_exp_norm(:,:,pos) + renorm*fdev);
                    counter = counter+1;
                end
                
                
                
                if P.GPU_calculate
                    p2 = fnorm*double(ifft2(GPUsingle(af.*ph)));
                else
                    if strcmp(P.prop_method,'far_field')
                        p2 = fnorm*ifft2(af.*ph);
                    elseif strcmp(P.prop_method,'near_field')
                        %prop_nf_pa(im,lambda,Delta_z,dx,dy)
                        p2 = prop_nf_pa(af.*ph,P.lambda,-P.z_eff,P.d1x,P.d2x)*fnorm;
                    end
                end
                
                df = p2 - p1;
                
                
                % Difference map error
                err_current = err_current + sum(sum(abs(df.^2)));
                
                % new object estimate
                % ---------------------------------------------------------
                I_Probe = abs(Probe);
                c_Probe = conj(Probe);
                
                obj_enum = c_Probe;
                I_Probe = I_Probe.^2;
                obj_denom= (max(I_Probe(:))+1e-8);
                object_old  = object(Indy,Indx); 
                
                object(Indy,Indx) = object_old+ P.alpha*obj_enum./obj_denom.*df;
                
                if P.clip_object
                    % prohibit values larger 1
                    abs_object = abs(object);
                    high = (abs_object > P.trans_max_true);
                    low = (abs_object < P.trans_min_true);
                    object = (1-low).*(1-high).*object + (low*P.trans_min_true + high*P.trans_max_true).*object./abs_object;
                end
                
                if phase_force_zero_region
                    pha_obj = angle(object);
                    amp_obj = abs(object);
                    pha_obj = min(pha_obj,0).*not_zero_pha_supp+pha_obj.*(1-P.zero_pha_gamma*P.zero_pha_supp).*zero_pha_supp;
                    object =amp_obj.*exp(sqrt(-1).*pha_obj);
                end
                
                
                if it > (P.N_probe_change_start-1) % probe refinement
                    % new probe estimate
                    % ---------------------------------------------------------
                    
                    I_obj = abs(object_old);
                    c_obj = conj(object_old);
                    
                    Probe_enum = c_obj;
                    I_obj = I_obj.^2;
                    Probe_denom =(max(I_obj(:))+1e-8);
                    
                    Probe =Probe + P.beta*Probe_enum./Probe_denom.*df;
                                      
                    if it > P.N_probe_mask_start-1
                        if P.switch_probemask
                            %Mask in sample plane
                            Probe_norm = norm_avg(Probe);
                            Probe = Probe.*P.Probe_mask;
                            Probe = Probe .* Probe_norm/norm_avg(Probe);
                        end 
                    end                  
                end
                
                if P.Probe_filt && mod(it,1) == 0
                    
                    % save "power" in image for later normalization
                    Probe_norm = norm_avg(Probe);
                    
                    Probe_abs = abs(Probe);
                    Probe_phase = Probe./(Probe_abs+1e-20);
                    
                    % apply filter that identifies outliers
                    % calculate difference of the the central pixel and the mean of
                    % is surrounding pixels, and normalize this difference to the
                    % mean value of the whole matrix
                    
                    H = ones(5,5);
                    H = -H/(numel(H)-1);
                    H(3,3) = 1;
                    
                    % the following line is probably not useful
                    %            H = H/mean2(Probe_abs);
                    
                    %H = -1/8*[1 1 1;1 -8 1; 1 1 1]/mean2(Probe_abs);
                    % filter the image with kernel H and form the absolute value
                    % values outside the image are assumed to be 0
                    test = abs(imfilter(Probe_abs,H,0));
                    %            imagesc(test); axis xy equal tight; colormap gray;
                    
                    % find all values larger than a certain threshold
                    %[I,J] = find(test>0.8*max(test(:)));
                    
                    % replace idenfified pixel values with the mean of their
                    % neighbourhood
                    % width of the "neighbourhood matrix", choose only odd
                    % number for symmetry reasons!
                    N_kernel = 3;
                    N_step = (N_kernel-1)/2;
                    % generate operation matrix
                    G = ones(N_kernel,N_kernel);
                    G(N_step+1,N_step+1) = 0;
                    G = G/(numel(G)-1);
                    
                    % replace outliers by mean of their neighbourhood
                    
                    apply_here = zeros(size(test));
                    apply_here(test>0.9*max(test(:))) = 1;
                    
                    Probe_abs = apply_here.*imfilter(Probe_abs,G) + (1-apply_here).*Probe_abs;
                    
                    %            Probe_abs(I,J) = 0;
                    
                    Probe = Probe_abs.*Probe_phase;
                    Probe = Probe .* Probe_norm/norm_avg(Probe);
                    
                end
                pos = 1;
                Indy = P.positions(pos,1) + (1:P.Ny);
                Indx = P.positions(pos,2) + (1:P.Nx);
                psi(:,:,pos) = Probe.*object(Indy,Indx);
            end
            
            prch = norm((Probe - Probe_old));
            fprintf('Change in probe: %f\n',prch);
            
            
        otherwise
    end
    % Store distances and errors for logging
    err(it) = sqrt(err_current/(P.N_pie*P.Ny*P.Nx));
    chi(it) = chi_current;
    power(it) = power_current;
%    power_bs(it) = power_bs_current;
    
    fprintf('Average chi per pixel (not normalized to error) is %5f\n',chi_current);
    fprintf('sqrt(<d^2>) =  %3.2e\n',sqrt(power_value));
    fprintf('Difference map error: %12.3f\n',err(it));
    fprintf('Fraction of "internal" resonctructions is %3.2f\n',1-counter/P.N_pie);
    
    if (it >= P.average_start) && mod(it, P.average_interval)==0
        obj_avg = obj_avg + object;
        Probe_avg = Probe_avg + Probe;
        numav = numav + 1;
    end
    
    
    if P.save_data
        if P.progress_protocol
            th = 1;
            if it == 1
                mkdir('progress_protocol');
            end
            cd('progress_protocol');
            
            if P.plot_mask
                tmp = mod(angle(rmphaseramp(object, plot_mask)),2*pi);
                mn = min(tmp(plot_ind));
                mx = max(tmp(plot_ind));
            else
                tmp = mod(angle(object),2*pi);
                mn = min(tmp(:));
                mx = max(tmp(:));
            end
            
            imwrite(imresize((tmp-mn)/(mx-mn)*255,[256 256]),jet(256),['pha_object_it_' num2str(it,'%04i') '.png'],'png','bitdepth',8);
            imwrite(imresize(c2image(Probe),[128 128]),['cplx_Probe_it_' num2str(it,'%04i') '.png'],'png','bitdepth',8);
            tmp = log10(abs(fftshift(FFT2(psi(:,:,1)))).^2);
            mn = min(tmp(:));
            mx = max(tmp(:));
            imwrite(imresize((tmp-mn)/(mx-mn)*255,[128 128]),jet(256),['dp_it_' num2str(it,'%04i') '.png'],'png','bitdepth',8);
            
            cd('..');
        end
    end
    
    
    if P.update_plots
        
        if strcmp(P.datatype,'exp')
            tmp = single(object);
            abs_obj = abs(tmp);
            
            if P.plot_mask
                pha_obj = angle(rmphaseramp(tmp, plot_mask));
            else
                pha_obj = angle(tmp);
            end
            clear tmp;
            
            % Phase
            figure(1);
            imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),pha_obj);
            axis xy; axis equal; axis tight; colormap gray(256);
            colorbar;
            if P.plot_mask
                caxis([min(pha_obj(plot_ind)) max(pha_obj(plot_ind))]);
            else
                caxis([P.plotbounds(1) P.plotbounds(2)]);
            end
            title(['Phase, iteration  ' num2str(it)]); drawnow;
            % Amplitude
            figure(2);
            imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),abs(single(object))); axis xy; axis equal; axis tight; colormap gray;
            title(['Amplitude, iteration  ' num2str(it)]); colorbar; drawnow;
            if P.plot_mask
                caxis([min(abs_obj(plot_ind)) max(abs_obj(plot_ind))]);
            end
            
            
            figure(3);
            imagesc(squeeze(X_1(1,:)),squeeze(Y_1(:,1)),hsv2rgb(im2hsv(single(Probe./max(max(abs(single(Probe))))),1))); axis xy; axis equal; axis tight;
            title(['Probe function, iteration  ' num2str(it)]);
            drawnow;
            
            I_norm = single(abs(Amp_exp_norm(:,:,1).^2));
            MIN = log10(min(min(I_norm(I_norm > 0))));
            MAX = log10(max(max(single(Amp_exp_norm(:,:,1).^2))));
            
            figure(4);
            subplot(1,2,1);
            if strcmp(P.prop_method,'far_field')                    
                imagesc(single(log10(abs(fftshift(FFT2(psi(:,:,1)))).^2))); 
            elseif strcmp(P.prop_method,'near_field')
                imagesc(single(log10(abs(prop_nf_pa(psi(:,:,1),P.lambda,P.z_eff,P.d1x,P.d2x)/fnorm).^2)));
            end
            axis xy; axis equal; axis tight; caxis([MIN MAX])
            title(['iteration -reconstruction  ' num2str(it)]);
            drawnow;
            subplot(1,2,2);
            if strcmp(P.prop_method,'far_field')    
                imagesc(single(fftshift(log10(abs(Amp_exp_norm(:,:,1)).^2))));
            elseif strcmp(P.prop_method,'near_field')
                imagesc(single((log10(abs(Amp_exp_norm(:,:,1)).^2))));
            end
            axis xy; axis equal; axis tight; caxis([MIN MAX])
            title(['iteration  observed dp ' num2str(it)]);
            drawnow;
            
            % Error
            figure(5);
            plot(err(1:it)); title('Difference map error.'); drawnow;
            
        elseif strcmp(P.datatype,'sim')
            % Phase
            figure(1);
            subplot(1,2,1);
            imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),angle(single(object))); axis xy; axis equal; axis tight; colormap hsv; caxis([P.plotbounds(1) P.plotbounds(2)]);
            title(['Phase, reconstruction, iteration  ' num2str(it)]); drawnow,
            subplot(1,2,2);
            imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),angle(P.object_true)); axis xy; axis equal; axis tight; colormap hsv; caxis([P.plotbounds(1) P.plotbounds(2)]);
            title(['Phase, original']); drawnow;
            % Amplitude
            figure(2);
            subplot(1,2,1);
            imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),abs(single(object))); axis xy; axis equal; axis tight; colormap gray;
            title(['Amplitude, reconstruction, iteration  ' num2str(it)]); drawnow;
            subplot(1,2,2);
            imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),abs(P.object_true)); axis xy; axis equal; axis tight; colormap gray;
            title(['Amplitude, original']); drawnow;
            % Error
            figure(3);
            subplot(1,2,1);
            plot(err(1:it)); title('Difference map error.'); drawnow;
            subplot(1,2,2);
            plot(chi(1:it)); title('chi'); drawnow;
            
        end
        
    end
    tmp = toc;
    fprintf(['\nElapsed time is ',num2str(tmp),' seconds.\n'])
    fprintf(['\nEstimated time left is ',num2str(tmp*(P.N_it-it)/60),' min.\n'])
end

P.err = err;
P.chi = chi;
P.power = power;
P.power_bs=power_bs;
obj_avg = obj_avg/numav;
Probe_avg = Probe_avg/numav;

%% Plot final result (average)

if P.N_it > (P.average_start + P.average_interval)
    P.object = double(obj_avg);
    P.Probe = double(Probe_avg);
else
    P.object = double(object);
    P.Probe = double(Probe);
end

if P.update_plots
    
    if strcmp(P.datatype,'exp')
        tmp = single(P.object);
        abs_obj = abs(tmp);
        
        if P.plot_mask
            pha_obj = angle(rmphaseramp(tmp, plot_mask));
        else
            pha_obj = angle(tmp);
        end
        clear tmp;
        % Phase
        figure;
        imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),pha_obj); axis xy; axis equal; axis tight; colormap hsv;
        if P.plot_mask
            caxis([min(pha_obj(plot_ind)) max(pha_obj(plot_ind))]);
        else
            caxis([P.plotbounds(1) P.plotbounds(2)]);
        end
        
        title(['Phase, final']);
        % Amplitude
        figure;
        imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),abs_obj); axis xy; axis equal; axis tight; colormap gray;
        if P.plot_mask
            caxis([min(abs_obj(plot_ind)) max(abs_obj(plot_ind))]);
        end
        title(['Amplitude, final']);
    elseif strcmp(P.datatype,'sim')
        % Phase
        figure;
        subplot(1,2,1);
        imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),angle(P.object)); axis xy; axis equal; axis tight; colormap hsv; caxis([P.plotbounds(1) P.plotbounds(2)]);
        title(['Phase, reconstruction, iteration  ' num2str(it)]);
        subplot(1,2,2);
        imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),angle(P.object_true)); axis xy; axis equal; axis tight; colormap hsv; caxis([P.plotbounds(1) P.plotbounds(2)]);
        title(['Phase, original']);
        % Amplitude
        figure;
        subplot(1,2,1);
        imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),abs(P.object)); axis xy; axis equal; axis tight; colormap gray;
        title(['Amplitude, final']);
        subplot(1,2,2);
        imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),abs(P.object_true)); axis xy; axis equal; axis tight; colormap gray;
        title(['Amplitude, original']);
        
    end
    
end

if P.save_data
    if P.progress_protocol
        if it == 1
            mkdir('progress_protocol');
        end
        cd('progress_protocol');
        
        %amplitude
        tmp = abs(P.object);
        if P.plot_mask
            mn = min(tmp(plot_ind));
            mx = max(tmp(plot_ind));
        else
            mn = min(tmp(:));
            mx = max(tmp(:));
        end
        imwrite((tmp-mn)/(mx-mn)*255,bone(256),['fin_abs_object.png'],'png','bitdepth',8);
        %phase
        tmp = mod(angle(P.object),2*pi);
        if P.plot_mask
            tmp = angle(rmphaseramp(tmp, plot_mask));
            mn = min(tmp(plot_ind));
            mx = max(tmp(plot_ind));
        else
            tmp = angle(tmp);
            mn = min(tmp(:));
            mx = max(tmp(:));
        end
        imwrite((tmp-mn)/(mx-mn)*255,gray(256),['fin_pha_object.png'],'png','bitdepth',8);
        %cmplx object
        imwrite(c2image(P.object),['fin_cplx_object.png'],'png','bitdepth',8);
        %cmplx probe
        imwrite(c2image(P.Probe),['fin_cplx_Probe.png'],'png','bitdepth',8);
        %diffraction pattern
        tmp = log10(abs(fftshift(FFT2(psi(:,:,1)))).^2);
        mn = min(tmp(:));
        mx = max(tmp(:));
        imwrite((tmp-mn)/(mx-mn)*255,jet(256),['fin_dp.png'],'png','bitdepth',8);
        
        cd('..');
    end
end

% P.psi = psi;
%% Store results

if P.save_data == true
    
    fprintf('Now storing the data.\n');
    tmp = findstr(datafile,'.');
    tmp = tmp-1;
    save(['reconstruction_',datafile(1:tmp)],'P','-v7.3');
    clear tmp;
    diary parameters.txt
    P
    diary off;
    
    
    
end
cd(current_path);

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Berechnet die el. Feldkomponente eines Gauss'schen Strahles
%Als Eingabe Position auf der z-Achse (Z) xy-Ebene (RHO)
%benutzte Wellenlaenge (lambda) und Waist Radius (wnull)

%           by R.Wilke 05.05.2009

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Einige berechnete Parameter:
% W: beam radius
% R: radius of curvature
% znull: Raygleigh range
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [U,GAUSS] = GAUSSU(RHO,Z,lambda,wnull)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               Berechne zuerst die benoetigten Parameter
%

k = 2*pi/lambda;
Anull = 1 ;
znull = wnull^2*pi/lambda;
W = wnull*sqrt(1+Z.^2/znull^2);

TMP=(Z==0);             %Problem falls Z irgendwo Null
R=Z;                    %d.h. nicht durch Null teilen
R(TMP)=Inf;
R(~TMP) = Z(~TMP).*(1+znull^2./Z(~TMP).^2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               Berechne komplette Feldamplitude U (Rho, Z)
%

U = Anull*(1./R+sqrt(-1)*2./(k*W.^2)).*exp(sqrt(-1)*k*Z).* ...
    exp((sqrt(-1)*k./(2*R)-1./W.^2).*RHO.^2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               alternativ Darstellung zu obigen U
%

% zeta = atan(Z/znull)

%U = Anull*sqrt(-1)*2./(k*wnull*W).*exp(-sqrt(-1)*zeta).* ...
%    exp(sqrt(-1)*k*Z).*exp((sqrt(-1)*k./(2*R)-1./W.^2).*RHO.^2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               speichere alle Parameter in structure 'Gauss'
%               fuer eventuelle uebergabe
%



GAUSS.k = k;
GAUSS.lambda = lambda;
GAUSS.Anull = Anull;
GAUSS.Wnull = wnull;
GAUSS.Znull = znull;
GAUSS.W     = W;
GAUSS.R     = R;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        von RHO unabhaengiger Anteil des Feldes U = Zpart.*RHOpart:
%        Zpart
%


GAUSS.Zpart = Anull*(1./R+sqrt(-1)*2./(k*W.^2)).*exp(sqrt(-1)*k*Z);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%         von RHO abhaengiger Anteil des Feldes U = Zpart.*RHOpart:
%         RHOpart
%

GAUSS.RHOpart = exp((sqrt(-1)*k./(2*R)-1./W.^2).*RHO.^2);
end


function positions = ptycho_prep_pos(p)




positions = [];

switch p.scan_type
    
    case 'round_roi'
        dx_spec = p.dx_spec;
        rmax = sqrt((p.lx/2)^2 + (p.ly/2)^2);
        nr = 1 + floor(rmax/p.dr);
        for ir=1:nr+1
            rr = ir*p.dr;
            dth = 2*pi / (p.nth*ir);
            for ith=0:p.nth*ir-1
                th = ith*dth;
                xy = rr * [sin(th), cos(th)];
                if( abs(xy(1)) >= p.ly/2 || (abs(xy(2)) > p.lx/2) )
                    continue
                end
                
                positions(end+1,:) = xy ./dx_spec; %#ok<AGROW>
            end
        end
    case 'custom'
        positions = p.positions;
        positions(:,2) = positions(:,2)/p.d1x; % horizontal axis
        positions(:,1) = positions(:,1)/p.d1y; % vertical axis
    otherwise
        
        ind = 1;
        for n_slow = 0:(p.ny-1)
            for n_fast = 0:(p.nx-1)
                positions(ind,2) = p.pie_step(2) * n_fast/p.d1x; % horizontal axis
                positions(ind,1) = p.pie_step(1) * n_slow/p.d1y; % vertical axis
                ind = ind+1;
            end
        end
        
end

positions(:,1) = positions(:,1) - min(positions(:,1));
positions(:,2) = positions(:,2) - min(positions(:,2));
positions = round(positions);

end


function im = c2image(a, varargin)
% FUNCTION IM = C2IMAGE(A)
%
% Returns a RGB image of complex array A where
% the phase is mapped to hue, and the amplitude
% is mapped to brightness.

absa = abs(a);
phasea = angle(a);

% (optional second argument can switch between various plotting modes)
abs_range = [];
if nargin==2
    m = varargin{1};
elseif nargin==3
    m = varargin{1};
    abs_range = varargin{2};
else
    m = 1;
end

if isempty(abs_range)
    nabsa = absa/max(max(absa));
else
    nabsa = (absa - abs_range(1))/(abs_range(2) - abs_range(1));
    nabsa(nabsa < 0) = 0;
    nabsa(nabsa > 1) = 1;
end

switch m
    case 1
        im_hsv = zeros([size(a) 3]);
        im_hsv(:,:,1) = mod(phasea,2*pi)/(2*pi);
        im_hsv(:,:,2) = 1;
        im_hsv(:,:,3) = nabsa;
        im = hsv2rgb(im_hsv);
    case 2
        im_hsv = ones([size(a) 3]);
        im_hsv(:,:,1) = mod(phasea,2*pi)/(2*pi);
        im_hsv(:,:,2) = nabsa;
        im = hsv2rgb(im_hsv);
end
end
