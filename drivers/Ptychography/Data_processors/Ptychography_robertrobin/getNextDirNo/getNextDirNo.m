function no  = getNextDirNo(base_dir,dig,type_str)
% this functions looks into the base_dir folder and compares all Folders of
% the type type_str_xxxxxx where xxxxxx is the SIM no. It takes the last no and
% returns the next one

%
tmp = dir(base_dir);
N = length(tmp);
M = length(type_str);
No = [];
for akk = 1:N
    name = tmp(akk).name;
    if ~strcmp(name,'..') && ~strcmp(name,'.')
        ln = length(name);
        limit_ln =(length(type_str)+dig);
        if ~isempty(findstr(name,type_str)) && ln==limit_ln
        No(end+1) = str2num(name(M+1:M+dig));
        end
    end
end

if isempty(No)
    no =1;
else
    no = max(No)+1;
end
end

