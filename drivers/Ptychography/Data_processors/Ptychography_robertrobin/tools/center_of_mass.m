function [cy,cx] = center_of_mass(I)

[indy,indx] = ind2sub(size(I),[1:numel(I)]');
M = reshape(I,numel(I),1);

ys = sum(sum(I))^(-1)*M.*indy;
xs = sum(sum(I))^(-1)*M.*indx;

cy = sum(sum(ys));
cx = sum(sum(xs));