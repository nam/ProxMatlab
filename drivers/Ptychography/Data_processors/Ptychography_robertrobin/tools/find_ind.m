function [ind_x,ind_y] = find_ind(X,Y,x,y)

%SX = size(X);
%SY = size(Y);

%if ~isequal(SX(1),SX(2),SY(1),SY(2))
%    error('Wrong imput format for coordinate matrix');
%end

[dummy,ind_x] = min(abs(X(1,:) - x));
[dummy,ind_y] = min(abs(Y(:,1) - y));