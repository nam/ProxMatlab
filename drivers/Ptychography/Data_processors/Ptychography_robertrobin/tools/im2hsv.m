% Author: K. Giewekemeyer
% based on routine pie_simulation_code.m by M. Dierolf et al.
%
% Last version copied from
% smb://homer/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/
% analysis_at_home/matlab_20100723/main/tools.
% 
% Last checked and modified on 24.10.2010.
% new modification on 04.07.2011 because persistent problem
%Error using ==> image
%TrueColor CData contains element out of range 0.0 <= value <= 1.0
%
%Error in ==> imagesc at 21
%    hh = image(varargin{:},'CDataMapping','scaled');
% Error in ==> PCDI at 588
%             imagesc(squeeze(X_1(1,:)),squeeze(Y_1(:,1)),hsv2rgb(im2hsv(single(Probe./max(max(abs(single(Probe))))),1)));
%             axis xy; axis equal; axis tight;

function imhsv=im2hsv(im,th)
%normalize amplitude
abs_im = abs(im)-min(abs(im(:)));
abs_im = abs_im./max(abs_im(:)+eps);

imhsv = zeros(size(im,1), size(im,2),3);

% mapping of phase values from (-pi,pi) to (0,2pi)
%imhsv(:,:,1) = (angle(im.*exp(1i*pi))+pi)./(2*pi);

imhsv(:,:,1) = mod(angle(im),2*pi)./(2*pi);
imhsv(:,:,2) = ones(size(im,1),size(im,2));
% amplitude values
tmp = (abs_im - double((1+sign(abs_im-th))/2.*abs_im) + double((1+sign(abs_im-th))/2.*th) ) /th;
tmp(tmp>1) = 1;
tmp(tmp<0) = 0;
imhsv(:,:,3) = tmp;
% This yields:
% abs(x)/th   for abs(x) <= th and
% 1           for abs(x) > th.

