function val = IFFT(X);

val = ifft(X)*sqrt(numel(X));