clear all; close all; 
set(0,'DefaultFigureColor','w');
%addpath /home/rwilke/Matlab/Messdatenanalysen/2012/PETRAIII/run12/matlab_general/
addpath /home/AG_Salditt/Messzeiten/2012/PETRAIII/run_12_11Jun-13Jun/offline_analysis/rnw/P10_matlab/general/
% MatLab source code adapted from Klaus Giewekemeyer's

%%
fmask = true(619,487);
fmask(408:424,1:487) = false;
fmask(196:212,1:487) = false;
%%
% This is experimental data!
P.datatype = 'exp';

% Use logged positions from external file?
P.log_positions = false;

% Save the data?
P.save_data = true;

current_path = pwd;


%% Parameter declaration
% I/O settings for adapting data from prepdata_template from cSAXS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
scan_no= 48;

P.file_prefix = 'NTT_01'; % i.e. newfile
spec_path = '/home/AG_Salditt/Messzeiten/2012/PETRAIII/run_12_11Jun-13Jun/data/spec/';
scan_details = spec_get_scan_info(scan_no,P.file_prefix,spec_path);
P.spec_details =scan_details;
P.data_dir = ['/home/AG_Salditt/Messzeiten/2012/PETRAIII/run_12_11Jun-13Jun/data/detectors/p300/',P.file_prefix,'/'];

%
asize = 192*[1 1];
P.first_file_no =  scan_details.first_file;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%scantype
scan_type = scan_details.type;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Factor for downsizing the data
P.B =1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Number of accumulations per frame
P.N_acc = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% beam center
Bx = 251;
By = 312;


% choose roi 
roi = asize/2;

roix = (Bx-asize(2)/2):(Bx+asize(2)/2-1);
roiy = (By-asize(1)/2):(By+asize(1)/2-1);


% Desired number of pixels
P.Nx = length(roix)/P.B;
P.Ny = length(roiy)/P.B;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Energy in keV
P.E = 7.9;
% Wavelength in [m]
P.lambda = 12.3984225/P.E*1e-10;
P.k = 2*pi/P.lambda;


% Estimated propagation distances
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Distance sample <-> detector in [m]
P.z12 = 5.1;
% Distance sample <-> pinhole in [m]
P.z01 =0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step sizes of PIE (ptychographic iterative engine) scan in [m]
% Information can be retrieved from lab book

switch scan_type
    case 'round_roi'
        %lengths of box for roun_roi scan [m]
        P.lx = 10E-6;
        P.ly = 10E-6;
        %distance between two successive shells [m]
        P.dr = 0.5E-6;
        %numbers of scan positions on first shell [typical value is 5]
        P.nth =5;
    case 'stxm'
        P.pie_step_x = abs(scan_details.range(1,1)-scan_details.range(1,2))*1E-6/scan_details.steps(1);
        P.pie_step_y = abs(scan_details.range(2,1)-scan_details.range(2,2))*1E-6/scan_details.steps(2); %- for stzrot =0 ; + for stzrot 180
        P.pie_step = [P.pie_step_y P.pie_step_x];
    case 'mesh'
        P.pie_step_x = abs(scan_details.range(1,1)-scan_details.range(1,2))*1E-6/scan_details.steps(1);
        P.pie_step_y = abs(scan_details.range(2,1)-scan_details.range(2,2))*1E-6/scan_details.steps(2); %- for stzrot =0 ; + for stzrot 180
        P.pie_step = [P.pie_step_y P.pie_step_x];
    otherwise
        P.pie_step_x = abs(scan_details.range(1,1)-scan_details.range(1,2))*1E-6/scan_details.steps(1);
        P.pie_step_y = abs(scan_details.range(2,1)-scan_details.range(2,2))*1E-6/scan_details.steps(2); %- for stzrot =0 ; + for stzrot 180
        P.pie_step = [P.pie_step_y P.pie_step_x];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Number of pie positions
P.nx = scan_details.steps(1)+1; % horizontal, fast axis
P.ny = scan_details.steps(2)+1; % vertical, slow axis

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Actual sidelength of meshscan, length of fast axis
P.N = P.nx;

% Total number of PIE points
P.N_pie = P.nx*P.ny;

%  Prepare coordinate systems etc.

% Detector plane (E2) -----------------------------------------------------

%pixel dimensions
P.d2x = P.B*172*1E-6;
P.d2y = P.B*172*1E-6;

% Sample plane (E1) -------------------------------------------------------
% Only true in the small-angle approximation

% figure;
% alpha = (1:0.1:100);
% r = 1./sqrt(1 + 1./(4*alpha.^2));
% semilogy(alpha,1-r)
% xlabel('z/D');
% ylabel('1 - dx_{appr}/dx_{acc}');
% grid on;

alpha = P.z12/(P.Nx*P.d2x);

% Check small-angle approximation
fprintf('1-D/z/sin(D/z) = %3.2e.\n',1-1/alpha/sin(1/alpha));
% Ratio of approximated dx to exact dx
% r = 1/sqrt(1 + 1/(4*alpha^2));
% fprintf('Ratio of approximated to exact pixel size: %f.\n',r);
if 1/alpha > 0.1
    error('Careful, small-angle approximation is violated.');
end

P.d1x = P.z12*P.lambda/(P.Nx*P.d2x);
P.d1y = P.z12*P.lambda/(P.Ny*P.d2y);

P.dq2x = 2*pi/(P.Nx*P.d1x);
P.dq2y = 2*pi/(P.Ny*P.d1y);

% Fourier space system corresponding to sample/detector plane
[Q2_x,Q2_y] = meshgrid(((1:P.Nx)-floor(P.Nx/2)-1)*P.dq2x,((1:P.Ny)-floor(P.Ny/2)-1)*P.dq2y);

% Specimen plane (E1)/ probe plane (as long as direct propagation is used) -
[X_1,Y_1] = meshgrid(((1:P.Nx)-floor(P.Nx/2)-1)*P.d1x,((1:P.Ny)-floor(P.Ny/2)-1)*P.d1y);


% Setting up PIE matrix
% Positions matrix (will contain (x,y)-coordinates of positions in pixel
% units of object plane)



% Nominal positions
positions = zeros([P.N_pie,2]);
switch scan_type
    case 'round_roi'
        rmax = sqrt((P.lx/2)^2 + (P.ly/2)^2);
        nr = 1 + floor(rmax/P.dr);
        ind = 1;
        for ir=1:nr+1
            rr = ir*P.dr;
            dth = 2*pi / (P.nth*ir);
            for ith=0:P.nth*ir-1
                th = ith*dth;
                xy = rr * [sin(th), cos(th)];
                if( abs(xy(1)) >= P.ly/2 || (abs(xy(2)) > P.lx/2) )
                    continue
                end
                positions(ind,:) = xy ;
                ind = ind+1;
            end
        end
        positions(:,2)=positions(:,2)/P.d1x;
        positions(:,1)=positions(:,1)/P.d1y;
    otherwise
        
        ind = 1;
        for n_slow = 0:(P.ny-1)
            for n_fast = 0:(P.nx-1)
                positions(ind,2) = P.pie_step(2) * n_fast/P.d1x; % horizontal axis
                positions(ind,1) = P.pie_step(1) * n_slow/P.d1y; % vertical axis
                ind = ind+1;
            end
        end
        
end

positions(:,1) = positions(:,1) - min(positions(:,1));
positions(:,2) = positions(:,2) - min(positions(:,2));
positions = round(positions);

% figure(1);
% scatter(positions(:,2),positions(:,1))


%% look for beam center


I_exp = zeros(size(fmask,1),size(fmask,2),P.N_pie);

dump_positions = zeros(P.N_pie,2);

ind =0;



if matlabpool('size')==0
    matlabpool open local 8
elseif matlabpool('size')<8
    matlabpool close
    matlabpool open local 8
end
for akk = 1:P.ny
    tic;
    Ind =[];
    for bkk =1:P.nx
        Ind=[Ind, ind];
        ind =ind+1;
    end
    
    I_exp_tmp=zeros(size(fmask,1),size(fmask,2),P.nx);
    
    parfor bkk =1:P.nx
        
        filename = [P.data_dir,P.file_prefix,'_',sprintf('%05i',P.first_file_no+Ind(bkk)),'.cbf'];  
        
		
		im = imagereader_P10(filename,'pilatus');
		
        
        I_exp_tmp(:,:,bkk)=(im);
    end
    for bkk =1:P.nx
        I_exp(:,:,Ind(bkk)+1) =  I_exp_tmp(:,:,bkk);
    end
    T = toc;
    %clc;
    figure(1)
    imagesc(log10(abs(I_exp(:,:,ind))))
    axis ij
    fprintf('Loading image # %d of %d.\n',ind,P.N_pie);
    fprintf('Estimated time left %2.2f minutes.\n',T*(P.ny-akk)/60);
    pause(.1);
end





%% transfer experimental data


ind = 1;

P.I_exp = zeros(P.Ny,P.Nx,P.ny,P.nx);
P.fmask = zeros(P.Ny,P.Nx,P.ny,P.nx);

cx=floor((P.Nx)/2);
cy=floor((P.Ny)/2);

for akk = 1:P.ny
    for bkk =1:P.nx
        
%         P.I_exp(:,:,akk,bkk) = I_exp(roiy,roix,ind);
%         P.fmask(:,:,akk,bkk) = fmask(roiy,roix);
        
        %rot90(rot90(circshift(P.I_exp(:,:,n_slow,n_fast),-[cy cx])))
        % @ stzrot 180
        
        tmp =squeeze(I_exp(:,:,ind)).*fmask;
         %tmp =tmp.*(bs_mask)*fact+tmp.*(~bs_mask);
		 
%         
%         tmask = ones(size(fmask));
%         tmask(331,248)=0;
%         tmask(305,245:258)=0;
%         tmask(317,245:258)=0;
%         tmask(305:317,245)=0;
%         tmask(305:317,258)=0;
		% initial setting
%          P.fmask(:,:,akk,bkk) = circshift(rot90(rot90(ifftshift(fliplr(fmask(roiy,roix))))),+[cy cx]);
%          P.I_exp(:,:,akk,bkk) = circshift(rot90(rot90(ifftshift(fliplr(tmp(roiy,roix))))),+[cy cx]);
%  		
		 
         P.fmask(:,:,akk,bkk) = circshift(rot90(rot90(ifftshift((fmask(roiy,roix))))),+[cy cx]);
         P.I_exp(:,:,akk,bkk) = circshift(rot90(rot90(ifftshift((tmp(roiy,roix))))),+[cy cx]);
        
        %pause(.15)
        
        ind = ind+1;
    end
    
    figure(3)
    %imagesc(log10(abs(rot90(rot90(circshift(P.I_exp(:,:,akk,bkk),-[cy cx]))))));
    imagesc(log10(abs((P.I_exp(:,:,akk,bkk)))).*P.fmask(:,:,akk,bkk));
    axis xy equal tight
    colormap jet
end



P.I_exp(P.I_exp<0)=0;
P.I_exp(isnan(P.I_exp))=0;

%% transfer experimental data


% Average incoming counts at the detector
I_avg = mean(mean(sum(sum(P.I_exp,1),2),3),4);
P.I_avg = I_avg;
fprintf('Average intensity of single frame is %3.2e photons.\n',I_avg);

% figure; imagesc(log10(P.I_exp(:,:,1))); axis xy equal tight; colorbar

fprintf('Finished data preparation.\n');



%%
ind = 0;

positions =zeros(P.nx*P.ny,2); 

for akk = 1:P.ny
    for bkk = 1:P.nx

    tmp = read_P10_dump_positions([P.data_dir,'dump/',P.file_prefix,sprintf('_%05i.cbf.txt',P.first_file_no+ind)],'py','pz');
    ind = ind+1;
    positions(ind,2)=tmp.py.user/1e6;
    positions(ind,1)=tmp.pz.user/1e6;
    end
    fprintf('.\n')
end
%
save(['positions_',P.file_prefix,'_',num2str(P.first_file_no,'%05i')],'positions')  ;
% show how positions would lie on the grid
load(['positions_',P.file_prefix,'_',num2str(P.first_file_no,'%05i')],'positions');

positions(:,2)=positions(:,2)/P.d1x;
positions(:,1)=positions(:,1)/P.d1y;
positions(:,1) = positions(:,1) - min(positions(:,1));
positions(:,2) = positions(:,2) - min(positions(:,2));

% figure(1);
% scatter(positions(:,2),positions(:,1))

positions = round(positions);

% figure(2);
% scatter(positions(:,2),positions(:,1))
%


P.positions = positions;

% Adjust objetc size accordingly
object_size = [P.Ny,P.Nx] + max(P.positions,[],1);
P.object_size = object_size;
% restore the ungrided positions
load(['positions_',P.file_prefix,'_',num2str(P.first_file_no,'%05i')],'positions');

%%
%cd(current_path);
P.save_data = true; 
if P.save_data == true
    
    fprintf('Now storing the data.\n');
    P.save_dir = [pwd,'/',P.file_prefix,'_S',scan_details.no,'_',sprintf('%05i',P.first_file_no)];
    mkdir(P.save_dir);
    cd(P.save_dir);
    
    save(['data_',P.file_prefix,'_',sprintf('%05i',P.first_file_no),sprintf('_%03ix%03i',asize(1),asize(end)),'.mat'],'P','-v7.3');

    diary parameters.txt
    P 
    diary off;
    save(['positions_',P.file_prefix,'_',num2str(P.first_file_no,'%05i')],'positions')  
    cd(current_path);
    clear I_exp
end
%close all;


 

