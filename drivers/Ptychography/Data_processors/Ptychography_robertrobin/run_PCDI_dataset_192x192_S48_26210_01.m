%% Ptychographic Imaging



clear all; close all;







addpath(['./getNextDirNo/'])
addpath(['./']);
addpath(['/home/rhesse/Documents/svn/regularity/ProxToolbox_v2.0/drivers/Ptychography/Ptychography_robertrobin/tools']);

scan_no= 48;
run_file = 1;

P_in.file_prefix = 'NTT_01'; % i.e. newfile
% Data parameters
first_file_no = 26210;
asize = 192*[1 1];


P_in.data_dir= ['./',P_in.file_prefix,'_S',num2str(scan_no,'%02i'),'_',sprintf('%05i',first_file_no),'/'];
P_in.datafile = ['data_',P_in.file_prefix,'_',sprintf('%05i',first_file_no),'_',sprintf('%03ix%03i',asize(1),asize(end)),'.mat'];
P_in.home_dir = ['./'];
P_in.run_file = ['run_PCDI_dataset_',sprintf('%03ix%03i',asize(1),asize(end)),'_S',num2str(scan_no,'%02i'),'_',sprintf('%05i',first_file_no),'_',num2str(run_file,'%02i'),'.m'];
% Physical parameter in [m]
%distance aperture/focus 2 sample - important for initialization of probe
P_in.z01 = 0; %
%distance detector 2 sample
P_in.z12 =  5.1;% original value  
% Energy in keV
P_in.E =7.9;
% Wavelength in [m]
P_in.lambda = 12.3984225/P_in.E*1e-10;
P_in.k = 2*pi/P_in.lambda;

% %scantype round_roi
% P_in.scan_type = 'round_roi';  % possible values 'round_roi' or leave '' for raster
% P_in.round_roi.lx = 10E-6;
% P_in.round_roi.ly = 10E-6;
% P_in.round_roi.nth = 5;
% P_in.round_roi.dr = 0.5E-6;
% 

% P_in.scan_type='raster';
% P_in.pie_step_x = abs(scan_details.range(1,1)-scan_details.range(1,2))*1E-6/scan_details.steps(1);
% P_in.pie_step_y = abs(scan_details.range(2,1)-scan_details.range(2,2))*1E-6/scan_details.steps(2); %- for stzrot =0 ; + for stzrot 180
% P_in.pie_step = [P_in.pie_step_y P_in.pie_step_x];

% %scantype raster (not used here)
P_in.scan_type = 'custom';  % possible values 'round_roi' or leave '' for raster
% % load positions
load([P_in.home_dir,'positions_',P_in.file_prefix,'_',num2str(first_file_no,'%05i')],'positions');


P_in.positions = positions;
%clear P;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Algorithmic parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
P_in.PIE_method = 'DM';      % 'ePIE' or 'DM'/'PIE'
P_in.N_it = 600; % number of total PCDI cycles
% Noise (from 0.1 to 10)
P_in.tau =1e-9; %Threshold value for Fourier projection standard [1e-9 ]

P_in.alpha =0.5; %Threshold value for Fourier projection
P_in.beta =0.5; %Threshold value for Fourier projection

P_in.average_start =400; % start of complex averaging of Probe and illumination
P_in.average_interval = 2; % interval at which iterates are considererd for 
                      % averaging
% P_in.blurr_size = 0.1; 
% P_in.artifact_filter = 0; % artifact-filter, should be switched off ("0") 
%                      % in units of PIE steps
P_in.Probe_filt = false;

P_in.random_start = 0; % start with random phase and amplitude (can try both)

% Clipping variables for amplitude transmission, should be kept like this
P_in.clip_object = true;
% True (estimated) minimum transmission
P_in.trans_min_true = 0.9;
% True maximum value of object transmission
P_in.trans_max_true = 1;

% Iteration, from which Probe is also updated, not only object
P_in.N_probe_change_start = 1;
% Maximum number of probe updates within one PIE iteration ("3")
P_in.N_nested_max =5;
% Probe-support constraint, for online reconstructions during beamtime set 
% this to "infinity", so that no probe support is used
% Probe mask in sample plane
P_in.N_probe_mask_start = 1;
P_in.switch_probemask = true;

P_in.phase_force_zero_region = false;
% P_in.zero_pha_gamma = 0.1;
% 
% load('S12_obj_phase_supp_256x256.mat','supp');
% 
% %supp = supp;
% supp = imfilter(double(supp),fspecial('gaussian',[16 16],2));
% supp = 1-supp;
% supp = supp -min(supp(:));
% supp = supp/max(supp(:));
% P_in.zero_pha_supp = supp;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%what kind of illumination to use?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
P_in.precalculated_probe =true;
load('./reconstruction_data_NTT_01_26210_192x192.mat','P')
%P.Probe(150:220,10:35)=0;
%
% 
 probe = imresize(P.Probe,[asize(1) asize(end)]);
 figure(1)
 imagesc(log10(abs(probe)))
% 
% 
 P_in.obj_guess = P.object;
 P_in.probe_guess = probe;
clear P;

%if precalculated_probe = false use this parameters
P_in.calculate_probe = 'gauss';
P_in.probe.FWHM = 0.2E-6/sqrt(2*log(2))*(2*sqrt(log(2)));



%elliptical pinhole
% 
P_in.calculate_probe = 'ellipsis';
P_in.probe.a = .6E-6;
P_in.probe.b = .6E-6;
P_in.probe.alpha = 0;

% use precalculated object ? 
P_in.precalculated_object = false;
%P_in.obj_guess= P.object_rmphaseramp;
%clear P;
% For initial guess for probe function
% Pinhole (radius in m)
P_in.R = 2.2E-6; % standard 2.2e-6
P_in.probe_mask_gamma = inf; % standard 1


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computational parameters, not affecting the results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
P_in.update_plots =true; % set to "0" if figure plotting is not needed
P_in.plot_mask = false; %use mask for plotting 
% if plot_mask  =false plotbounds will be used
P_in.plotbounds = [-0.2 0.2];
% Plot intial functions
P_in.plot_initial = false; % set to "false" if figure plotting is not needed



% Save png's of probe and object to report progress
P_in.progress_protocol = false;
% save data?
P_in.save_data =false;
%where2save
%
%P_in.save_dir  = ['EXP_',num2str(getNextDirNo(P_in.data_dir,6,'EXP_'),'%06i'),'_',P_in.datafile(1:(end-4)),'_tau_' num2str(P_in.tau)];

%calculate traditional CHI^2 ? lenghty...
P_in.CHI_calculate = false;

% use GPU calculations ?
P_in.GPU_calculate = false;

dir_prefix = [P_in.PIE_method,'_',sprintf('%03ix%03i',asize(1),asize(end)),'_'];
base_dir =['./',P_in.file_prefix,'_S',num2str(scan_no,'%02i'),'_',sprintf('%05i',first_file_no),'/'];
 next_no = num2str(getNextDirNo(base_dir,3,dir_prefix),'%03i');

 
base_dir =[base_dir,dir_prefix,next_no,'/'];
%logfile = [base_dir,'run_PCDI_7025_ePIE_',next_no,'_',sprintf('%03ix%03i',asize,asize),'_',datestr(now,30),'.log'];
%

if P_in.save_data
    mkdir(base_dir)  
end
% 
 P_in.save_dir = base_dir;

%diary(logfile)


%diary off;
% clear PP;

%


PP{1} = P_in;



 %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Execute program
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 P_out = PCDI(PP{1});
