%                   Ptychography.m
%              written on 15th January 2014 by
%                       Matthew Tam
%                      CARMA Centre
%                  University of Newcastle
%           last modified on 23rd July 2014
%
% DESCRIPTION: This module sets the prox mappings based on the problem type
% within the problem family 'Ptychography'

function [input, output] = Ptychography(input)

%%===============================================================
%% Rewrite fields in our representation:  this bit needs work to 
%%   standardise, particularly for the object domain constraints
%===============================================================

assert(strcmp(input.experiment,'ptychography')); %check that we are doing ptychography.
switch input.ptychography_prox
    case 'PHeBIE'
        input.Prox1 = 'P_ptychography_PHeBIE_probe';
        input.Prox2 = 'P_ptychography_PHeBIE_object';
        input.Prox3 = 'P_ptychography_PHeBIE_phi';
        input.ProxSequence = [1 2 3];
        input.warmup_Prox1 = 'P_ptychography_PHeBIE_object';
        input.warmup_Prox2 = 'P_ptychography_PHeBIE_phi';
        input.warmup_ProxSequence = [1 2];
        if strcmp(input.method,'Nesterov_PHeBIE')
            input.fnames = {'probe','object','phi'};
        end
    case 'PHeBIEregPhi'
        input.Prox1 = 'P_ptychography_PHeBIE_probe';
        input.Prox2 = 'P_ptychography_PHeBIE_object';
        input.Prox3 = 'P_ptychography_PHeBIE_phi_regularized';
        input.ProxSequence = [1 2 3];
        input.warmup_Prox1 = 'P_ptychography_PHeBIE_object';
        input.warmup_Prox2 = 'P_ptychography_PHeBIE_phi_regularized';
        input.warmup_ProxSequence = [1 2];
        if strcmp(input.method,'Nesterov_PHeBIE')
            input.fnames = {'probe','object','phi'};
        end
    case 'PHeBIEregPhiPtwise'
        input.Prox1 = 'P_ptychography_PHeBIE_probe_ptwise';
        input.Prox2 = 'P_ptychography_PHeBIE_object_ptwise';
        input.Prox3 = 'P_ptychography_PHeBIE_phi_regularized';
        input.ProxSequence = [1 2 3];
        input.warmup_Prox1 = 'P_ptychography_PHeBIE_object_ptwise';
        input.warmup_Prox2 = 'P_ptychography_PHeBIE_phi_regularized';
        input.warmup_ProxSequence = [1 2];
        if strcmp(input.method,'Nesterov_PHeBIE')
            input.fnames = {'probe','object','phi'};
        end       
    case 'DRlPHeBIEregPhiPtwise'
        input.Prox1 = 'P_ptychography_PHeBIE_D_ptwise';
        input.Prox2 = 'P_ptychography_PHeBIE_phi_regularized';
        input.ProxSequence = [1 2];
        input.warmup_Prox1 = 'P_ptychography_PHeBIE_object_ptwise';
        input.warmup_Prox2 = 'P_ptychography_PHeBIE_phi_regularized';
        input.warmup_ProxSequence = [1 2];
    	input.DRlwith = {'phi'}; %a list of file name on which reflections are to be performed with.     
    case 'Rodenburg'
        input.Prox1 = 'P_ptychography_Rodenburg';
        input.ProxSequence = [1];
        input.warmup_Prox1 = 'P_ptychography_Rodenburg_probe_fixed';
        input.warmup_ProxSequence = [1];
    case 'Thibault'
        input.Prox1 = 'P_ptychography_Thibault_OP';
        input.Prox2 = 'P_ptychography_Thibault_F';
        input.ProxSequence = [1 2];
        input.warmup_Prox1 = 'P_ptychography_Thibault_O';
        input.warmup_Prox2 = 'P_ptychography_Thibault_F';
        input.warmup_ProxSequence = [1 2];
        if ~strcmp(input.method,'RAAR_PHeBIE')||~strcmp(input.warmup_method,'RAAR_PHeBIE')
            warning(['Method "' input.method '" is incorrect for use with Thibault. Method changed to "RAAR_PHeBIE". If you wish to use a Thibault-like method without RAAR, set ptychography_prox=Thibault_AP.'])
            input.method = 'RAAR_PHeBIE';
            input.warmup_method = 'RAAR_PHeBIE';
        end
        input.RAARwith = {'phi'}; %a list of file name on which reflections are to be performed with.
    case 'Thibault_fattened'
        input.Prox1 = 'P_ptychography_Thibault_OP';
        input.Prox2 = 'P_ptychography_Thibault_F';
        input.ProxSequence = [1 2];
        input.warmup_Prox1 = 'P_ptychography_Thibault_O';
        input.warmup_Prox2 = 'P_ptychography_Thibault_F';
        input.warmup_ProxSequence = [1 2];
        if ~strcmp(input.method,'RAAR_PHeBIE')||~strcmp(input.warmup_method,'RAAR_PHeBIE')
            warning(['Method "' input.method '" is incorrect for use with Thibault. Method changed to "RAAR_PHeBIE". If you wish to use a Thibault-like method without RAAR, set ptychography_prox=Thibault_AP.'])
            input.method = 'RAAR_PHeBIE';
            input.warmup_method = 'RAAR_PHeBIE';
        end
        input.RAARwith = {'phi'}; %a list of file name on which reflections are to be performed with.
    case 'Thibault_AP'
        input.Prox1 = 'P_ptychography_Thibault_OP';
        input.Prox2 = 'P_ptychography_Thibault_F';
        input.ProxSequence = [1 2];
        input.warmup_Prox1 = 'P_ptychography_Thibault_O';
        input.warmup_Prox2 = 'P_ptychography_Thibault_F';
        input.warmup_ProxSequence = [1 2];
    otherwise
        error('Error: Prox-operators could not be found.')
end

if(isempty(input.product_space_dimension))
    input.product_space_dimension = 1;
end

% set the animation program:
if ~isfield(input,'animation')
    input.animation='Ptychography_graphics';
end
%%===================================================================
%% 
%%   Run the algorithm!!!
%% 
%%===================================================================
t0 = cputime;

%% Is blocking enabled?
if input.blocking_switch % ie. True
    input.in_block = GetPtychographyBlocks(input);
end        

%% Run the warm-up algorithm.
if isfield(input,'warmup')&&(input.warmup==1)
    if input.verbose
        disp('Start warm-up algorithm...')
    end
    
    % Create a copy of the data structure.
    input_warmup = input;
    input_warmup.verbose = 0;
    
    % Set warm-up algorithm parameters.
    input_warmup.method = input.warmup_method;
    input_warmup.MAXIT  = input.warmup_MAXIT;
    
    % Set the ProxOperators.
    for s=1:length(input.warmup_ProxSequence)
        ProxNum = num2str(input.warmup_ProxSequence(s));
        eval(strcat('input_warmup.Prox',ProxNum,'=input.warmup_Prox',ProxNum,';'));
    end
    input_warmup.ProxSequence = input.warmup_ProxSequence;
    
    % Settings only used when doing a Nesterov-type acceleration.
    if isfield(input_warmup,'fnames')
       input_warmup.fnames(strcmp(input_warmup.fnames,'probe')) = [];
    end
    
    % Run the algorithm.
    if input.blocking_switch
        output_warmup = feval('blocking_meta',input_warmup);
    else
        output_warmup = feval(input_warmup.method,input_warmup);
    end
    input.u_0 = output_warmup.u;
    
    if input.verbose
        disp('End warm-up algorithm...')
    end
end

t1 = cputime;

%% Run the main algorithm.
if input.blocking_switch
    output = feval('blocking_meta',input);
else
    output = feval(input.method,input);
end

tf = cputime;
output.cputime=tf-t0;

% clear out the phi variables...aren't interested in these and they 
% take up a lot of space
output.u.phi=[];
input.Amp_exp_norm=[];
input.u_0.phi=[];
input.I_exp=[];
input.fmask=[];

iter  = output.stats.iter-1;
stats = output.stats;
label = ['Took ',num2str(iter),' iterations'];
disp(label)

tmp = min(stats.change);
if(tmp>input.TOL)
    label = ['MAXIT=', num2str(input.MAXIT),' exceeded'];
    disp(label)
end
disp(' ')

%%=========================================================================
%%
%%  now write output in form that comforms with standard format
%%  assumed in main_ProxToolbox
%%
%%=========================================================================

%%=========================================================================
%% 
%%   Graphics (could also do this from within main_ProxToolbox)
%% 
%%=========================================================================
if(input.graphics==1)
    success = feval(input.animation, input,output);
end

% if(input.graphics==1)
%     % Save graphics to file.
%     figs = get(0,'children');
%     directory = ['/scratch/ProxToolbox/OutputData/' datestr(now,'yymmdd') input.problem_family];
%     if ~exist(directory,'dir')
%         mkdir(directory);
%     end
%     for i=1:length(figs)
% %         figure_filename = [directory '/' input.ptychography_prox 'beta' num2str(input.beta_max) 'figure' num2str(i)];
% 
%         figure_filename = [directory '/' input.input_files 'figure' num2str(i)];
%         saveas(figs(i), figure_filename, 'fig');
%         saveas(figs(i), figure_filename, 'png');
%     end
% end
% 
% for i=1:length(figs)
%   if strcmp(input.noise,'poisson')
%     figure_filename = [directory datestr(now,'yymmmdd') input.ptychography_prox input.method 'poisson' num2str(input.poissonfactor) 'run' num2str(input.run_number) 'figure' num2str(i)];
%   else
%     figure_filename = [directory datestr(now,'yymmmdd') input.ptychography_prox input.method 'run' num2str(input.run_number) 'figure' num2str(i)];
%   end
%   saveas(figs(i), figure_filename, 'fig');
%   saveas(figs(i), figure_filename, 'png');
% end



%%=========================================================================
%% 
%%   write results to file (could also do this from within main_ProxToolbox)
%% 
%%=========================================================================

% fprintf('Final objective value:\t\t %f \n', stats.customError(end,5))
fprintf('Final step-size norm:\t\t %f \n', stats.change(end))
% fprintf('RMS Error Object:\t\t  %f \n', stats.customError(end,1))
% fprintf('RMS Error Probe:\t\t  %f \n', stats.customError(end,2))
% fprintf('R-factor (Phi):\t\t\t %f \n', stats.customError(end,4))
fprintf('Warm-up algorithm CPU time:\t %f \t sec \n', t1-t0)
fprintf('Main algorithm CPU time:\t %f \t sec \n', tf-t1)
fprintf('Total CPU time: \t\t %f \t sec \n\n\n', tf-t0)


end
