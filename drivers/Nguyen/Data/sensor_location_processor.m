%                     sensor_location_processor
%              written on 8th September 2016 by
%                          Russell Luke (from Haifa!)
%                   University of Gottingen
%
% DESCRIPTION:
%       data reader/processor

function method_input = sensor_location_processor(method_input)

noise = method_input.noise;
dimension = method_input.Nx;
sensors = method_input.Ny;

% options for method_input.instance include:  `simple random', `simple file', and 'JWST'
if(strcmp(method_input.instance, 'simple file'))
   disp(['Loading data file: sensor_location_data.mat'])
   load -ascii sensor_location_data.mat
elseif(strcmp(method_input.instance, 'simple random'))
    rng(3);
    % generate the true position of the source
    method_input.truth = 2*method_input.phys_boundary*(rand(1,dimension)-0.5);
    % generate the sensors A
    method_input.receiver_data = 2*method_input.phys_boundary*(rand(sensors,dimension)-0.5);
    method_input.receiver_data_scale=method_input.phys_boundary*sensors*dimension;
    % calculate the distances from the source to the sensors
     method_input.data = zeros(1,sensors);
    for ii = 1:sensors
        method_input.data(ii) = norm(method_input.receiver_data(ii,:)-method_input.truth,2);
    end
    %method_input.data = method_input.radius_boundary*rand(1,sensors);
    method_input.norm_data = norm(method_input.data);

    % initial quadrupts guess
    method_input.x_0 = repmat(2*method_input.phys_boundary*(rand(1,dimension)-0.5),sensors,1); % method_input.truth; % 
    method_input.u_0 = method_input.x_0-method_input.receiver_data; % zeros(size(method_input.x_0)); 
	% 2*method_input.phys_boundary*(rand    (sensors,dimension)-0.5);
    method_input.v_0 = method_input.u_0; % zeros(size(method_input.x_0)); % 2*method_input.phys_boundary*(rand(sensors,dimension)-0.5);
    method_input.w_0 = method_input.receiver_data - method_input.x_0; % 2*method_input.phys_boundary*(rand(sensors,dimension)-0.5);
    method_input.product_space_dimension = sensors;
elseif(strcmp(method_input.instance, 'JWST'))
   method_input = feval('JWST_data_processor',method_input);
end
