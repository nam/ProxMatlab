%                      BLOCK_RAAR_sequential_ART_in.m
%              written on October 6, 2011 by
%                        Russell Luke
%                 University of Goettingen
%
% DESCRIPTION:  parameter input file for main_dvr.m
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function input = BLOCK_QNAP_sequential_ART_in(input)

%% We start very general.
%%
%% What type of problem is being solved?  Classification 
%% is according to the geometry:  'Affine', 'Phase', 
%% 'Affine-sparsity',  'Custom'

input.problem_family = 'ART';
input.expert=true; % false = uses  algorithm_wrapper.m

%%==========================================
%% Problem parameters
%%==========================================
%% What is the name of the data file?
input.data_filename = 'BLOCK_ART_data_processor';
input.fanbeam = 'no';



%% What type of object are we working with?
%% Options are: 'phase', 'real', 'nonnegative', 'complex'
input.object = 'complex';

%% What type of constraints do we have?
%% Options are: 'support only', 'real and support', 'nonnegative and support',
%%              'amplitude only', 'sparse real', 'sparse complex', and 'hybrid'
%%              'convex'
input.constraint = 'convex';

%% What type of measurements are we working with?
%% Options are: 'single diffraction', 'diversity diffraction', 
%%              'ptychography', 'complex', and 'diversity affine'
input.experiment = 'convex';


%% What are the dimensions of the measurements?
% given in the ART_data_processor


%%==========================================
%%  Algorithm parameters
%%==========================================
%% Now set some algorithm parameters that the user should be 
%% able to control (without too much damage)


% Point to appropriate projectors.

input.Prox1 = 'P_block_sequential_hyperplane'; % projection onto support and nonnegativity constraint
input.Prox2='P_Diag'; % projection onto mask constraint

%% Algorithm:
input.method = 'QNAP'; %'RAAR'; %'AP', 'Cimmino';  
input.numruns=1; % the only time this parameter will
% be different than 1 is when we are
% benchmarking, that is, when algorithm performance statistics 
% are being generated for randomly generated problems/initial 
% values etc.  



%% maximum number of iterations and tolerances
input.MAXIT = 40;
input.TOL = 1e-6;

%% relaxaton parameters in RAAR, HPR and HAAR
input.beta_0 = 1.0;  % starting relaxation prameter (only used with
% HAAR, HPR and RAAR)
input.beta_max =0.9;   % maximum relaxation prameter (only used with
% HAAR, RAAR, and HPR)
input.beta_switch = 5; % iteration at which beta moves from beta_0 -> beta_max

%% parameter for the data regularization
%% need to discuss how/whether the user should
%% put in information about the noise
input.data_ball = 1e-15;
% the above is the percentage of the gap
% between the measured data and the
% initial guess satisfying the
% qualitative constraints.  For a number
% very close to one, the gap is not expected
% to improve much.  For a number closer to 0
% the gap is expected to improve a lot.
% Ultimately the size of the gap depends
% on the inconsistency of the measurement model
% with the qualitative constraints.

%%==========================================
%% parameters for plotting and diagnostics
%%==========================================
input.diagnostic = true;  % to stop the diagnostics, just comment this field out.
input.verbose = 1; % options are 0 or 1
input.graphics = 1; % whether or not to display figures, options are 0 or 1.
                   % default is 1.
input.anim = 1;  % whether or not to disaply ``real time" reconstructions
                % options are 0=no, 1=yes, 2=make a movie
                % default is 1.
input.animation='ART_animation';
input.graphics_display = 'ART_graphics'; % unless specified, a default 
                            % plotting subroutine will generate 
                            % the graphics.  Otherwise, the user
                            % can write their own plotting subroutine

%%======================================================================
%%  Technical/software specific parameters
%%======================================================================
%% Given the parameter values above, the following technical/algorithmic
%% parameters are automatically set.  The user does not need to know 
%% about these details, and so probably these parameters should be set in 
%% a module one level below this one.  

end

