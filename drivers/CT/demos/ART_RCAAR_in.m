%                      ART_RCAAR_in.m
%                 Modified by:  Matthew Tam
%                  University of Newcastle
%                       5th Nov 2013.
%                      
%          original file: ART_averaged_proj_in.m
%              written on October 6, 2011 by
%                        Russell Luke
%                 University of Goettingen
%
% DESCRIPTION:  parameter input file for main_ProxToolbox.m
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function input = ART_RCAAR_in()

%% We start very general.
%%
%% What type of problem is being solved?  Classification 
%% is according to the geometry:  'Affine', 'Phase', 
%% 'Affine-sparsity',  'Custom'

input.problem_family = 'ART';

%%==========================================
%% Problem parameters
%%==========================================
%% What is the name of the data file?
input.data_filename = 'ART_data_processor';



%% What type of object are we working with?
%% Options are: 'phase', 'real', 'nonnegative', 'complex'
input.object = 'complex';

%% What type of constraints do we have?
%% Options are: 'support only', 'real and support', 'nonnegative and support',
%%              'amplitude only', 'sparse real', 'sparse complex', and 'hybrid'
%%              'convex'
input.constraint = 'convex';

%% What type of measurements are we working with?
%% Options are: 'single diffraction', 'diversity diffraction', 
%%              'ptychography', 'complex', and 'diversity affine'
input.experiment = 'convex';

%% Which formulation is to be used to solve the problem?
%% (So far only applied to RCAAR).
%% Options are: 'product space', and 'sequential'
%% ART only supports sequential.
input.formulation = 'sequential';

%% What are the dimensions of the measurements?
% given in the ART_data_processor


%%==========================================
%%  Algorithm parameters
%%==========================================
%% Now set some algorithm parameters that the user should be 
%% able to control (without too much damage)


% Point to appropriate projectors.

input.Prox1 = 'P_sequential_hyperplane_RCAAR';  % 'P_parallel_hyperplane'; % 'P_sequential_hyperplane_odd'; % projection onto support and nonnegativity constraint
input.Prox2 = 'P_sequential_hyperplane_RCAAR'; %'P_Diag'; %'P_sequential_hyperplane_even'; % projection onto mask constraint

%% Algorithm:
input.method = 'RCAAR'; %'AP', 'Cimmino';  
input.numruns=1; % the only time this parameter will
% be different than 1 is when we are
% benchmarking, that is, when algorithm performance statistics 
% are being generated for randomly generated problems/initial 
% values etc.  
input.storeiter=0; % Do you want to store the iterates from the last 
% iteration cyclce? True=1 (uses more memory), False=0 (uses less). 
% This only really matters for sequential formulations.



%% maximum number of iterations and tolerances
input.MAXIT = 27; %2700
input.TOL = -1e-6;

%% relaxaton parameters in RAAR, HPR and HAAR
input.beta_0 = 1.0;  % starting relaxation prameter (only used with
% HAAR, HPR and RAAR)
input.beta_max =0.6;   % maximum relaxation prameter (only used with
% HAAR, RAAR, and HPR)
input.beta_switch = 13; % iteration at which beta moves from beta_0 -> beta_max

%% parameter for the data regularization
%% need to discuss how/whether the user should
%% put in information about the noise
input.data_ball = 1e-15;
% the above is the percentage of the gap
% between the measured data and the
% initial guess satisfying the
% qualitative constraints.  For a number
% very close to one, the gap is not expected
% to improve much.  For a number closer to 0
% the gap is expected to improve a lot.
% Ultimately the size of the gap depends
% on the inconsistency of the measurement model
% with the qualitative constraints.

%%==========================================
%% parameters for plotting and diagnostics
%%==========================================
input.diagnostic = true; % to stop the diagnostics, just comment this field out.
input.verbose = 1; % options are 0 or 1
input.graphics = 1; % whether or not to display figures, options are 0 or 1.
                   % default is 1.
input.anim = 0;  % whether or not to disaply ``real time" reconstructions
                % options are 0=no, 1=yes, 2=make a movie
                % default is 1. Animation does not currently work for reordered arrays 
input.graphics_display = 'ART_graphics'; % unless specified, a default 
                            % plotting subroutine will generate 
                            % the graphics.  Otherwise, the user
                            % can write their own plotting subroutine

%%======================================================================
%%  Technical/software specific parameters
%%======================================================================
%% Given the parameter values above, the following technical/algorithmic
%% parameters are automatically set.  The user does not need to know 
%% about these details, and so probably these parameters should be set in 
%% a module one level below this one.  

end

