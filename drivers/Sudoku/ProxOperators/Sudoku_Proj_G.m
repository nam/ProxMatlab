%                   Sudoku_ProjektionGiven.m
%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
% DESCRIPTION:  Projection subroutine onto the given entries in a
%               sudoku problem
%
% INPUT:       input = a data structure with fields .Gegebenes_Sudoku
%                 A = array in the domain to be projected
%
% OUTPUT:       Anew = the projection
%               
% USAGE: Anew=Sudoku_ProjektionGiven(input,A)
%
% Nonstandard Matlab function calls:  none
function Anew=Sudoku_ProjektionGiven(input, A)

% Diese Funktion führt die Turmprojektion und die Projektion auf das
% gegebene Sudoku an einer ihr übergebenen 9x9x9-Matrix aus
%------------------------------------------------------------------------
Anew=zeros([9 9 9]);
%------------------------------------------------------------------------

for x=1:9
    for y=1:9
        
        if(input.Gegebenes_Sudoku(x,y)>0)
            Anew(x,y,input.Gegebenes_Sudoku(x,y))=1;
        else
            [MAX,Koordinate]=max(A(x,y,:));
            Anew(x,y,Koordinate)=1;
        end
        
    end
end
clear A input
return

