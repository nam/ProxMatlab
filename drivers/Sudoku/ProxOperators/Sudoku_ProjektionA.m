function MatrixA=LOESEN_ProjektionA(A, Gegebenes_Sudoku)

% Diese Funktion stellt die Projektion P_A dar
%------------------------------------------------------------------------

MatrixA(:,:,:,1)= LOESEN_ProjektionZeile(A(:,:,:,1));
MatrixA(:,:,:,2)= LOESEN_ProjektionSpalte(A(:,:,:,2));
MatrixA(:,:,:,3)= LOESEN_ProjektionQuadrat(A(:,:,:,3));
MatrixA(:,:,:,4)= LOESEN_ProjektionGiven(A(:,:,:,4), Gegebenes_Sudoku);

end
