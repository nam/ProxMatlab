%                   Sudoku_ProjektionSpalte.m
%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
% DESCRIPTION:  Projection subroutine onto the column constraints in a
%               sudoku problem
%
% INPUT:       input = a data structure with fields (not used here)
%                 A = array in the domain to be projected
%
% OUTPUT:       backQuadratMatrix = the projection
%               
% USAGE: backSpalteMatrix=Sudoku_ProjektionSpalte(input,A)
%
% Nonstandard Matlab function calls:  none
function backSpalteMatrix=Sudoku_ProjektionSpalte(input, A)

% Diese Funktion führt die Spaltenprojektion an einer ihr übergebenen
% 9x9x9-Matrix aus
%------------------------------------------------------------------------

backSpalteMatrix=zeros([9 9 9]);
for x=1:9
    for z=1:9
        
        [MAX,Koordinate]=max(A(x,:,z));
        vector=zeros([1 9 1]);
        
        vector(Koordinate)=1;
        backSpalteMatrix(x,:,z)=vector;
        
    end
end
clear A input
return
