%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function Verstos=GE_RegelVerstos(Sudoku,x,y)

%Diese Fkt. liefert Wert "True" zurück, falls ein Regelverstoß vorliegt.
%Regelversoß meint hierbei, dass die Zahl im Kästchen x,y nicht mit den
%anderen Zahlen im Sudoku nach den Regeln kombiniertbar ist.
%------------------------------------------------------------------------

Verstos=false;

%Untersucht ob der Wert an dem Punkt (x,y) in der Spalte erlaubt ist
%------------------------------------------------------------------------

for x1=1:9
    
    if (Sudoku(x1,y)==Sudoku(x,y) && x~=x1)
        
        Verstos=true;
        
    end
end

%Untersucht ob der Wert an dem Punkt (x,y) in der Zeile erlaubt ist
%------------------------------------------------------------------------
for y1=1:9
    
    if (Sudoku(x,y1)==Sudoku(x,y) && y~=y1)
        
        Verstos=true;
        
    end
end

%Untersucht ob der Wert an dem Punkt (x,y) im zugehörigen Quadrat
%erlaubt ist
%------------------------------------------------------------------------

for k=1:3
    for m=1:3
        
        if( Sudoku( ( round((x-5)/3) +1) *3+k, (round((y-5)/3)+1)*3+m )...
                ==Sudoku(x,y) && (round((x-5)/3) +1) *3+k~=x &&...
                (round((y-5)/3)+1)*3+m~=y)
            
            Verstos=true;
        end
        
    end
    
end