%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function Sudoku=LOESCH_ErstelleSudoku_Variante2(Sudoku, ...
    AnzahlZuLoeschendeZahlen)

% Diese Funktion löscht erst AnzahlZuLoeschendeZahlen Beiträge und testet
% dann auf die Eindeutigkeit des Sudokus
%------------------------------------------------------------------------

Eindeutig=false;
Sudoku2=Sudoku;

while(Eindeutig==false)
    
    for i=1:AnzahlZuLoeschendeZahlen
        Sudoku2=LOESCH_EntferneEinenEintrag2(Sudoku2);
    end
    
    Eindeutig=EDK_TesteEindeutigkeit_DerReiheNach(Sudoku2);
    
    if(Eindeutig)
        Sudoku=Sudoku2;
    else
        Sudoku2=Sudoku;
    end
    
end

end