%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function [moeglich]=LOESCH_ZahlenLoeschbar(Sudoku)

% Dieser Algorithmus testet, ob wirklich keine der noch vorhandenen
% Zahlen löschbar ist. Dies wurde nur zum Testen der Funktionen benutzt.
%------------------------------------------------------------------------
Sudoku2=Sudoku;
moeglich=false;
%------------------------------------------------------------------------

for x=1:9
    for y=1:9
        
        if(Sudoku(x,y)~=0)
            
            Sudoku2(x,y)=0;
            moeglich=EDK_TesteEindeutigkeit_DerReiheNach(Sudoku2);
            
            if(moeglich)
                break;
            else
                Sudoku2(x,y)=Sudoku(x,y);
                
            end
            
        end
    end
    
    if(moeglich)
        break;
    end
end

end
