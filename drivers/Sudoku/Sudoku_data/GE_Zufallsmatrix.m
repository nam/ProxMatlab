%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function Zufallsmatrix2=GE_Zufallsmatrix()

%Diese Fkt. gibt eine Permuation der Kästchen in einem Quadrat zurück.

% Es wird eine 9x4x9 Matrix erstellt. Der Aufbau ist wie folgt:
% (:,:,i) gibt eine 9x4-Matrix für das i-te Quadrat an. Innerhalb dieser
% Matrix enthält eine Zeile (rand, x, y, belegt) in der ersten Koordinate
% einen Zufallswert, der zum "Durchmischen" der Koordinaten dient. Die
% Werte x,y laufen von 1 bis 3 (da nur ein Quadrat betrachtet wird). Der
% vierte Wert gibt an, ob das Kästchen im Sudoku bereits belegt ist oder
% nicht (O -> Kästchen ist frei).
%------------------------------------------------------------------------

Zufallsmatrix2=rand([9 4 9]);


for Quadrat=1:9
    Laufvariable=0;
    for x=1:3
        for y=1:3
            Laufvariable=Laufvariable+1;
            
            Zufallsmatrix2(Laufvariable,2:4,Quadrat)=[x y 0];
            
        end
    end
end

% "Durchmischen" der Koordinaten
%------------------------------------------------------------------------

for Quadrat=1:9
    Zufallsmatrix2(:,:,Quadrat)=sortrows(Zufallsmatrix2(:,:,Quadrat),[1]);
end

end
