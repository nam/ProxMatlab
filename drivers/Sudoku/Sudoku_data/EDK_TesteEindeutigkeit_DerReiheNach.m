%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function Eindeutig=EDK_TesteEindeutigkeit_DerReiheNach(Sudoku1)

%Diese Fkt. wird ausgerufen, wenn die Eindeutigkeit eines Sudokus getestet
%werden soll. Sobald es mehr als eine Lösung gibt, bricht der Algorithmus
%ab und gibt eine 0 (Also die Verneinung der Eindeutigkeit) zurück.
%------------------------------------------------------------------------
Ergebnisse=0;
Quadrat=1;
Eintrag=1;
%------------------------------------------------------------------------

[Ergebnisse]=EDK_SetzeEintraege_DerReiheNach(Sudoku1, Quadrat,...
    Eintrag, Ergebnisse);

if(Ergebnisse>1)
    Eindeutig=false;
else
    Eindeutig=true;
end

end