%                      sensor_graphics.m
%                  written on August 8, 2016 by 
%                  Russell Luke & Nguyen Thao
%       Inst. Fuer Numerische und Angewandte Mathematik
%                    Universitaet Gottingen
%
%
% DESCRIPTION:  Script driver for viewing results from sensor location
%               problem
% 
%
% INPUT:  
%              method = character string for the algorithm used.
%         true_object = the ``true solution"
%                 u_0 = the initial guess
%                   u = the algorithm "fixed point"
%              change = the norm square of the change in the
%                              iterates
%              error  = squared set distance error at each
%                              iteration
%              noneg = the norm square of the nonnegativity/support
%                              constraint at each iteration
%              gap  = the norm square of the gap distance, that is
%                     the distance between the projections of the
%                     iterates to the sets
%
% OUTPUT:       graphics
% USAGE: sensor_graphics(method_input,method_output)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function success = sensor_graphics(method_input, method_output)
              
method=method_input.method;
iter = method_output.stats.iter;
change = method_output.stats.change;
gap  = method_output.stats.gap;
time = method_output.stats.time;


figure(900);
    subplot(2,1,1);   semilogy(change),xlabel('iteration'),ylabel(['||x^{k+1}-x^{k}||'])
    label = ['Algorithm: ',method, ',  ',num2str(time), 'seconds'];
    title(label)
    subplot(2,1,2);   semilogy(gap),xlabel('iteration'),ylabel(['||P_1(x^{k})-P_2(x^{k})||'])
    label = ['Algorithm: ',method];
    title(label)

success = 1;
