%                 Project_on_product_of_balls.m
%              written on 8th September 2016 by
%                          Russell Luke (from Haifa!)
%                   University of Gottingen
%
% DESCRIPTION:  The projection of a vector on the product of balls with different radius.
%
%

function uB = Project_on_product_of_balls(method_input,u)

uB = u;
m=method_input.Ny;
n=method_input.Ny;
p=method_input.Nz;
K=method_input.product_space_dimension;

if(~strcmp(method_input.instance,'Phase')) % simple 2D sensor location problem
    if(m==K)         
        % the following assumes a certain data array structure
	for jj=1:K;
            norm_u=norm(u(jj,:),2);
	    if norm_u > method_input.data(jj);
       	        uB(jj,:) = u(jj,:)*method_input.data(jj)/norm_u;
	    end
	end
    elseif(n==K)
        % the following assumes a certain data array structure
	for jj=1:K;
            norm_u= norm(u(:,jj),2);
	    if norm_u > method_input.data(jj);
       	        uB(:,jj) = u(:,jj)*method_input.data(jj)/norm_u;
	    end
	end
    else
	disp('[messasge from Project_on_product_of_balls] All cases not in yet.')
    end
else % phase retrieval
    TOL2=input.TOL2;
    epsilon = input.data_ball;
    for j=1:input.product_space_dimension-1
        U = feval('fft2',input.indicator_ampl.*exp(1i*input.illumination_phase(:,:,j)).*u(:,:,j));
        U_sq = U.*feval('conj',U);
        U_abs = sqrt(U_sq);
        tmp = U_sq./input.data_sq(:,:,j);
        tmp2=input.data_zeros(:,j)~=0;
        tmpI = input.data_zeros(tmp2,j);
        tmp(tmpI)=1;
        U_sq(tmpI)=0;
        IU= tmp==0;
        tmp(IU)=1;
        tmp=log(tmp);
        hU = sum(sum(U_sq.*tmp + input.data_sq(:,:,j) - U_sq));
        if(hU>=epsilon+TOL2)
            tmp=input.data(:,:,j);
            IU = U_abs>>tmp; % find only those indeces where amplitude is greater than maeasurement
            U0  = feval('MagProj',tmp(IU,j),U(IU));
            u(:,:,j) = input.indicator_ampl.*exp(-1i*input.illumination_phase(:,:,j)).*feval('ifft2',U0);
        else
            % no change
        end
    end
    % now project onto the pupil constraint.
    % this is a qualitative constraint, so no 
    % noise is taken into account.
    j=input.product_space_dimension;
    tmp=u(:,:,j);
    IU=abs(tmp)>=input.abs_illumination; % find only those indeces where amplitude is greater than measurement
    tmp(IU)  = feval('MagProj',input.abs_illumination(IU),tmp(IU));
    u(:,:,j) = tmp;    
end

end

