%                Project_on_shifted_diagonal.m
%              written on 9th September 2016 by
%                Russell Luke  (from Haifa!)
%                University of Gottingen
%
% DESCRIPTION:  The projection of a vector on the ``block diagonal'' 
%               subspace in the Fourier domain
%
%

function ud = Project_on_shifted_diagonal(method_input, u)

ud=u;
K = method_input.product_space_dimension; % the size of the product space
m = method_input.Ny; % the row-dim of the prod. space elements 
n = method_input.Nx; % the column-dim of the prod. space elements 
p = method_input.Nz;
TOL2=method_input.TOL2;
if(~any(strcmp('shift_data',fieldnames(method_input)))) % used to be called receiver_data
    center=0; % default shift
else
    center=method_input.shift_data;
end

if(m==1) % product space of points
    tmp = sum(u+center,2);  % shifts the diagonal
    tmp=tmp/K;
    ud = tmp*ones(1,K) - center; % shifts the points back to centered geometry
elseif(n==1) % product space of points
    tmp = sum(u+center);
    tmp=tmp/K;
    ud = ones(K,1)*tmp - center;
elseif(m==K) % product space of row vectors
    ud = repmat(sum(u+center, 1)./K,K,1)-center;
elseif(n==K) % product space of column vectors
    ud = repmat(sum(u+center, 2)./K,K,1)-center;
elseif(p==1) % product space a 3D array of 2D matrices
    tmp=zeros(n,m);
    for k=1:K
        tmp = tmp+u(:,:,k)+center(:,:,k);
    end
    tmp=tmp/K;
    ud = repmat(tmp,[1,1,K])-center;
else
    % the depth-dim of the prod. space elements (for 3-D matrices)
    tmp=zeros(n,m,p);
    for k=1:K
        tmp = tmp+u(:,:,:,k)+center(:,:,:,k);
    end
    tmp=tmp/K;
    ud = repmat(tmp,[1,1,1,K])-center;
end
return
