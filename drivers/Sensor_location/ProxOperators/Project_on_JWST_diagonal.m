%                Project_on_JWST_diagonal.m
%              written on 9th September 2016 by
%                Russell Luke  (from Haifa!)
%                University of Gottingen
%
% DESCRIPTION:  The projection of a vector on the ``block diagonal'' 
%               subspace in the Fourier domain
%
%

function ud = Project_on_JWST_diagonal(method_input, u)

ud=u;
K = method_input.product_space_dimension; % the size of the product space
m = method_input.Ny; % the row-dim of the prod. space elements 
n = method_input.Nx; % the column-dim of the prod. space elements 
p = method_input.Nz;
TOL2=method_input.TOL2;
epsilon = method_input.data_ball;
if(m>1)&&(n>1)&&(p==1)   
    tmp_u = zeros(m,n);
    for j=1:K-1
        tmp_u = tmp_u+method_input.indicator_ampl.*exp(-1i*method_input.illumination_phase(:,:,j)).*feval('ifft2',u(:,:,j));
    end
    % now project onto the pupil support
    j=method_input.product_space_dimension;
    tmp_u = tmp_u + method_input.indicator_ampl.*u(:,:,K);
    tmp_u = tmp_u/K;
    % now transform back to product space:
    ud(:,:,K) = tmp_u;
    for j=1:K-1
        ud(:,:,j) = feval('fft2',exp(1i*method_input.illumination_phase(:,:,j)).*tmp_u);
    end
end

return
