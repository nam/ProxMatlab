%                      main_ProxToolbox.m
%                  written on May 23, 2012 by 
%                         Russell Luke
%       Inst. Fuer Numerische und Angewandte Mathematik
%                    Universitaet Gottingen
%
%
% DESCRIPTION:  Script driver for viewing results from projection
%               algorithms on various toy problems
%
% INPUT:  
%              method = character string for the algorithm used.
%         true_object = the original image
%                 u_0 = the initial guess
%                   u = the algorithm "fixed point"
%              change = the norm square of the change in the
%                              iterates
%              error  = squared set distance error at each
%                              iteration
%              noneg = the norm square of the nonnegativity/support
%                              constraint at each iteration
%              gap  = the norm square of the gap distance, that is
%                     the distance between the projections of the
%                     iterates to the sets
%
% OUTPUT:       graphics
% USAGE: main_Luke_graphics(method_input,method_output)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function success = main_Luke_graphics(method_input, method_output)
              
method=method_input.method;
beta0 = method_input.beta_0;
beta_max = method_input.beta_max;
u_0 = method_input.u_0;
u = method_output.u1(:,:,1);
u2 = method_output.u2(:,:,1);
iter = method_output.stats.iter;
change = method_output.stats.change;
gap  = method_output.stats.gap;



figure(900);

    subplot(2,2,1); imagesc(abs(u)); colormap gray; axis equal tight; colorbar; title('best approximation - physical domain'); drawnow; 
    subplot(2,2,2); imagesc(abs(u2)); colormap gray; axis equal tight; colorbar; title('best approximation - Fourier constraint'); drawnow; %caxis([4.85,5.35]);
    subplot(2,2,3);   semilogy(change),xlabel('iteration'),ylabel(['||x^{2k+2}-x^{2k}||'])
    label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
    title(label)
    subplot(2,2,4);   semilogy(gap),xlabel('iteration'),ylabel(['||x^{2k+1}-x^{2k}||'])
    label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
    title(label)


  figure(904), colormap('gray')
      subplot(2,2,1), imagesc(real(u)), colorbar
      xlabel('best approximation - physical constraint')
      label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
      title(label)
    subplot(2,2,2); imagesc(angle(u)); colormap gray; axis equal tight; colorbar; title('best approximation phase - physical constraint'); caxis([-0.9 -0.4]); drawnow; %caxis([4.85,5.35]);
    subplot(2,2,3); imagesc(abs(u2)); colormap gray; axis equal tight; colorbar; title('best approximation - Fourier constraint'); drawnow; %caxis([4.85,5.35]);
    subplot(2,2,4); imagesc(angle(u2)); colormap gray; axis equal tight; colorbar; title('best approximation phase - Fourier constraint'); caxis([-0.9 -0.4]); drawnow; %caxis([4.85,5.35]);


success = 1;
