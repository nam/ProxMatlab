%                      P_FourierSample.m
%             written on August 20, 2012 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               support constraints
%
% INPUT:        input = data structure with .supp_ampl a vector of indeces of the nonzero elements of the array.
%
%               u = array to be projected
%
% OUTPUT:       p_FourierSample    = the projection 
%               
% USAGE: p_FourierSample = P_FourierSample(input,u)
%
% Nonstandard Matlab function calls:  

function p_FourierSample = P_FourierSample(input,u)

U=feval('fft',u);
tmp=U;
tmp(input.sample_idx) = input.b;
p_FourierSample=feval('ifft',tmp);


