% Input:
%   n = signal size

function [b,sample_mask,u_true]=SparseStoch1D(input)

n=input.Nx*input.Ny;
Sparsity = input.Sparsity; % input.Sparsity is a 
% percentage of nonzero entries.  0% is fully sparse, 1.0 is dense.
sample_rate= input.sample_rate; %
%       There's a bit of a cheat in Candes' example:  he samples n/8 Fourier
%       coefficients at random, but since the Fourier transform is of a real
%       signal, there is hidden symmetry.  So if he samples n/8 coefficients he
%       really gets about twice that number of samples due to symmetry in the
%       Fourier transform. 
%------------------------------------
% Set up toy problem:
% Candes' example, sort of
%------------------------------------
rand('twister',13); % sets the random seed to the default
tmp=rand(n,1);
[tmp2,I]=sort(tmp,'descend');
support=zeros(n,1);
cutoff=round(Sparsity*n);
support(I(1:cutoff))=ceil(tmp2(1:cutoff));

% support=floor(support);
randn('state',27);
u_true = support.*randn(n,1);
% seed=clock;
% seed=floor(seed(6)*1000);
rand('twister',2*n); % sets the random seed to the default
tmp=rand(n,1);
[tmp2,I]=sort(tmp,'descend');
sample_mask=zeros(n,1);
cutoff=round(sample_rate*n);
sample_mask(I(1:cutoff))=ceil(tmp2(1:cutoff));

% sample_mask(1,1)=1; % always sample the zero frequency?
% symmetrize samples to keep object real
sample_mask(2:end/2)=(ceil(.5*(sample_mask(2:end/2)+sample_mask(end:-1:end/2+2)))); 
sample_mask(end:-1:end/2+2)=sample_mask(2:end/2);
sample_mask(end/2+1)=1;

% define linear operators
b=(sample_mask.*feval('fft',u_true));

