%  Affine projection subroutine of the
%  dual subgradient descent method  
%  for minimizing the l1 norm
%  subject to a linear constraint.  This 
%  strategy was first proposed in 
%  ``Duality and Convex Porgramming" by
%  J. M. Borwein and D. R. Luke, 
%  to appear in THE HANDBOOK OF IMAGING
%  edited by Otmar Scherzer.  This algorithm
%  together with analysis is in preparation for 
%  submission to the book FIXED-POINT ALGORITHMS 
%  FOR INVERSE PROBLEMS IN SCIENCE AND ENGINEERING,
%  part of the Springer Verlag series Optimization 
%  and Its Applications.
%  For preprints go to 
%
% http://num.math.uni-goettingen.de/~r.luke/publications
% 
%  Russell Luke
%  Universitaet Goettingen and 
%  University of Delaware
%  Oct. 28, 2009
%
function v = P_B(M,b,v0)

v = feval('fft',v0);
v = v.*(1-M)+b;
v = feval('ifft',v);

clear M b v0
return