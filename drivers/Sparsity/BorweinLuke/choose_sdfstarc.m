%  Subgradient selection subroutine of the
%  dual subgradient descent method  
%  for minimizing the l1 norm
%  subject to a linear constraint.  This 
%  strategy was first proposed in 
%  ``Duality and Convex Porgramming" by
%  J. M. Borwein and D. R. Luke, 
%  to appear in THE HANDBOOK OF IMAGING
%  edited by Otmar Scherzer.  This algorithm
%  together with analysis is in preparation for 
%  submission to the book FIXED-POINT ALGORITHMS 
%  FOR INVERSE PROBLEMS IN SCIENCE AND ENGINEERING,
%  part of the Springer Verlag series Optimization 
%  and Its Applications.
%  For preprints go to 
%
% http://num.math.uni-goettingen.de/~r.luke/publications
% 
% 
%  Russell Luke
%  Universitaet Goettingen and 
%  University of Delaware
%  Oct. 28, 2009
%
%
function [v,iter]=choose_sdfstarc(b,M,v0,JJpr,JJpi,JJmr,JJmi,TOL, MAXIT)

step = 1;
iter=1;
v=zeros(size(v0));
beta=.71; % best is .71
MAXIT=1000*MAXIT;
q=v;
p=v;
v1 = feval('P_Ac',JJpr,JJpi,JJmr, JJmi,v0);
v=v1;
while (step>TOL)&&(iter<=MAXIT)
    % alternating projections:
    % v0 = feval('P_B',M,b,v1);
    % v1  = feval('P_A',JJp,JJm,v0);
    % step = norm(v1-v);
    % iter=iter+1;
    % v=v1;

    % runs the RAAR algorithm to find the best approximation
    v2 = 2*v1-v0;
    v  = feval('P_Bc',M,b,v2); % feval('P_A',JJp,JJm,v2);
    v  = beta/2*(2*v-v2 + v0) + (1-beta)*v1;
    v0=v;
    iter=iter+1;
    v1_old=v1;
    v1 = feval('P_Ac',JJpr,JJpi,JJmr, JJmi,v0); 
    step = norm(v1-v1_old);

    % Dykstra
%     v2 = feval('P_B',M,b,v+q);
%     q = v + q - v2;
%     v = feval('P_A',JJp,JJm,v2 + p);
%     p = v2 + p - v;
%     step = norm(v1-v);
%     v1=v;
%     iter=iter+1;
    
    % if iter<=2
    %    keyboard
    % end
end

% the point we are interested in is in A:
v=v1;
clear b M v0 JJp JJm TOL MAXIT
