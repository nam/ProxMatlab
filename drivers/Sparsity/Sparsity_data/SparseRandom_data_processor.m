function input = SparseRandom_data_processor(input)


n=input.Nx*input.Ny;
Sparsity = input.Sparsity; % input.Sparsity is a 
% percentage of nonzero entries.  0% is fully sparse, 1.0 is dense.
sample_rate= input.sample_rate; %
m = floor(sample_rate*n);
%       There's a bit of a cheat in Candes' example:  he samples n/8 Fourier
%       coefficients at random, but since the Fourier transform is of a real
%       signal, there is hidden symmetry.  So if he samples n/8 coefficients he
%       really gets about twice that number of samples due to symmetry in the
%       Fourier transform. 
%------------------------------------
% Set up toy problem:
% Candes' example, sort of
%------------------------------------
%rand('twister',13); % sets the random seed to the default
tmp=rand(n,1);
[tmp2,I]=sort(tmp,'descend');
support=zeros(n,1);
cutoff=round(Sparsity*n);
support(I(1:cutoff))=ceil(tmp2(1:cutoff));

% support=floor(support);
%randn('state',27);
input.truth =support.*randn(n,1);
support_idx= support~=0;
u_scale=min(abs(input.truth(support_idx)));
input.truth=ceil(input.truth/u_scale)*10;

% seed=clock;
% seed=floor(seed(6)*1000);
%rand('twister',2*n); % sets the random seed to the default
%tmp=rand(n,1);
%[tmp2,I]=sort(tmp,'descend');
%sample_mask=zeros(n,1);
%cutoff=round(sample_rate*n);
%sample_mask(I(1:cutoff))=ceil(tmp2(1:cutoff));

% sample_mask(1,1)=1; % always sample the zero frequency?
% symmetrize samples to keep object real
%sample_mask(2:end/2)=(ceil(.5*(sample_mask(2:end/2)+sample_mask(end:-1:end/2+2)))); 
%sample_mask(end:-1:end/2+2)=sample_mask(2:end/2);
%sample_mask(end/2+1)=1;

%input.sample_idx= sample_mask~=0;
% Generation of the random matrix
tmpM = normrnd(input.mu, input.sigma, m,n);


tmp=tmpM*input.truth;
tmpM1 = tmpM'*(tmpM*tmpM')^(-1);
disp('Matrix generated');
% %========================
% % add noise here
% if input.snr~=inf
%     snr=10;
%     % Add Poisson noise according to Ning Lei:
%     % f = alpha*4*4*pi/q/q*abs(cos(qx(iX))*sin(qy(iY))*(sin(q)/q-cos(q)));
%     %        f2 = PoissonRan(f*f*2)/alpha/alpha/2;   % 2 is for I(-q)=I(q)
%     %	sigma(iX, iY, iZ) = f/sqrt(2)/alpha/alpha/abs(ff)/abs(ff);
%     % July 15, 2010:  Since this is meant to model photon counts (which
%     % are integers) we add noise and then round down
%     tmp2=zeros(size(tmp));
%     for i=1:n
%         r=abs(tmp(i,1))/sqrt(n);
%         tmp2(i,1)= PoissonRan(r/snr)*sqrt(n);
%         r=r*sqrt(n);
%         tmp3(i,1)=real(tmp(i,1)) + tmp2(i,1)*real(tmp(i,1))/r + 1i*(imag(tmp(i,1)) +tmp2(i,1)*imag(tmp(i,1))/r);
%     end
%     % tmp=round(tmp);
%     % symmetrize tmp since this is from a real signal:
%     tmp3=feval('fft',real(feval('ifft',tmp3)));
% else
%     tmp3=tmp;
% end

% end add noise
%========================


input.b=tmp;
%tmp=zeros(n,1);
%tmp(input.sample_idx)=input.b;
input.pinvp = tmpM1*tmp;
input.pident = tmpM1*tmpM;
%The initial value is chosen from within a neighbourhood of the true
%solution.
%input.u_0= input.truth + .5*min(abs(input.truth(input.truth~=0)))*(1*(-1+(1+1)).*rand(size(input.truth)));
%input.u_0=input.b_star;
input.u_0 = (-1+(1+1)).*rand(size(input.truth));
input.product_space_dimension=1;
input.truth_dim = size(input.truth);
input.norm_truth=norm(input.truth,'fro');

clear tmp sample_mask
disp('Lets go!');
end

