%                      Sparse_Fourier_AP_in.m
%              written on October 6, 2011 by
%                        Russell Luke
%                 University of Goettingen
%
% DESCRIPTION:  parameter input file for main_ProxToolbox.m
%
%               This demo supported the publication:
% 
%     ``Alternating Projections and Douglas-Rachford for Sparse 
%       Affine Feasibility '', R. Hesse, D. R. Luke and P. Neumann 
%       IEEE Transactions on Signal Processing, 62(18):4868--4881, 2014. 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function input = Sparse_Random_AP_in(input)

%% We start very general.
%%
%% What type of problem is being solved?  Classification 
%% is according to the geometry:  'Affine', 'Phase', 
%% 'Affine-sparsity',  'Custom'

input.problem_family = 'Sparsity';
input.expert=false; % false = uses  algorithm_wrapper.m

%%==========================================
%% Problem parameters
%%==========================================
%% What is the name of the data file?
input.data_filename = 'SparseRandom_data_processor';

%% What type of object are we working with?
%% Options are: 'phase', 'real', 'nonnegative', 'complex'
input.object = 'real';

%% What type of constraints do we have?
%% Options are: 'support only', 'real and support', 'nonnegative and support',
%%              'amplitude only', 'sparse real', 'sparse complex', and 'hybrid'
%%              'convex'
input.constraint = 'sparse real';

%% What type of measurements are we working with?
%% Options are: 'single diffraction', 'diversity diffraction', 
%%              'ptychography', 'complex', and 'diversity affine'
input.experiment = 'linear';

%% What kind of random matrix will be generated?
input.sigma = 1;
input.mu = 0;

%% What are the dimensions of the measurements?
input.Ny = 64^2;
input.Nx = 1;
input.Nz = 1;

input.Sparsity=.005; % input.Sparsity is a 
% percentage of nonzero entries.  0% is fully sparse, 1.0 is dense.
input.sample_rate=1/8; %
input.s=82; % a priori number of nonzero entries (256 -> 328) (512 -> 1311)
%       There's a bit of a cheat in Candes' example:  he samples n/8 Fourier
%       coefficients at random, but since the Fourier transform is of a real
%       signal, there is hidden symmetry.  So if he samples n/8 coefficients he
%       really gets about twice that number of samples due to symmetry in the
%       Fourier transform. 

% the next is a generic scaling
% that removes the dependence of the 
% norms from the problem dimension. 
% More specific normalizations are up to the user. 
input.norm_data = sqrt(input.Nx*input.Ny);

% noise?
input.snr = inf;  % snr=inf for no noise

%%==========================================
%%  Algorithm parameters
%%==========================================
%% Now set some algorithm parameters that the user should be 
%% able to control (without too much damage)


% Point to appropriate projectors.

input.Prox2 = 'P_Affine'; % projection onto support and nonnegativity constraint
input.Prox1='P_Sparsity'; % projection onto mask constraint

%% Algorithm:
input.method = 'AP'; %'AP', 'Cimmino';  
input.numruns=1; % the only time this parameter will
% be different than 1 is when we are
% benchmarking, that is, when algorithm performance statistics 
% are being generated for randomly generated problems/initial 
% values etc.  



%% maximum number of iterations and tolerances
input.MAXIT = 30;
input.TOL = 1e-12;

%% relaxaton parameters in RAAR, HPR and HAAR
input.beta_0 = 1.0;  % starting relaxation prameter (only used with
% HAAR, HPR and RAAR)
input.beta_max =1.0;   % maximum relaxation prameter (only used with
% HAAR, RAAR, and HPR)
input.beta_switch = 10; % iteration at which beta moves from beta_0 -> beta_max

%% parameter for the data regularization
%% need to discuss how/whether the user should
%% put in information about the noise
input.data_ball = 1e-5;
% the above is the percentage of the gap
% between the measured data and the
% initial guess satisfying the
% qualitative constraints.  For a number
% very close to one, the gap is not expected
% to improve much.  For a number closer to 0
% the gap is expected to improve a lot.
% Ultimately the size of the gap depends
% on the inconsistency of the measurement model
% with the qualitative constraints.

%%==========================================
%% parameters for plotting and diagnostics
%%==========================================
input.diagnostic = true; % to turn off, just comment out.
input.verbose = 1; % options are 0 or 1
input.graphics = 1; % whether or not to display figures, options are 0 or 1.
                   % default is 1.
input.anim = 0;  % whether or not to disaply ``real time" reconstructions
                % options are 0=no, 1=yes, 2=make a movie
                % default is 1.
input.graphics_display = 'SPARS_graphics'; % unless specified, a default 
                            % plotting subroutine will generate 
                            % the graphics.  Otherwise, the user
                            % can write their own plotting subroutine

%%======================================================================
%%  Technical/software specific parameters
%%======================================================================
%% Given the parameter values above, the following technical/algorithmic
%% parameters are automatically set.  The user does not need to know 
%% about these details, and so probably these parameters should be set in 
%% a module one level below this one.  

end

