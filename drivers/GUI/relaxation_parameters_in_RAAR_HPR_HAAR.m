function varargout = relaxation_parameters_in_RAAR_HPR_HAAR(varargin)
% RELAXATION_PARAMETERS_IN_RAAR_HPR_HAAR MATLAB code for relaxation_parameters_in_RAAR_HPR_HAAR.fig
%      RELAXATION_PARAMETERS_IN_RAAR_HPR_HAAR, by itself, creates a new RELAXATION_PARAMETERS_IN_RAAR_HPR_HAAR or raises the existing
%      singleton*.
%
%      H = RELAXATION_PARAMETERS_IN_RAAR_HPR_HAAR returns the handle to a new RELAXATION_PARAMETERS_IN_RAAR_HPR_HAAR or the handle to
%      the existing singleton*.
%
%      RELAXATION_PARAMETERS_IN_RAAR_HPR_HAAR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RELAXATION_PARAMETERS_IN_RAAR_HPR_HAAR.M with the given input arguments.
%
%      RELAXATION_PARAMETERS_IN_RAAR_HPR_HAAR('Property','Value',...) creates a new RELAXATION_PARAMETERS_IN_RAAR_HPR_HAAR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before relaxation_parameters_in_RAAR_HPR_HAAR_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to relaxation_parameters_in_RAAR_HPR_HAAR_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help relaxation_parameters_in_RAAR_HPR_HAAR

% Last Modified by GUIDE v2.5 27-May-2015 14:02:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @relaxation_parameters_in_RAAR_HPR_HAAR_OpeningFcn, ...
                   'gui_OutputFcn',  @relaxation_parameters_in_RAAR_HPR_HAAR_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before relaxation_parameters_in_RAAR_HPR_HAAR is made visible.
function relaxation_parameters_in_RAAR_HPR_HAAR_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to relaxation_parameters_in_RAAR_HPR_HAAR (see VARARGIN)

% Choose default command line output for relaxation_parameters_in_RAAR_HPR_HAAR
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes relaxation_parameters_in_RAAR_HPR_HAAR wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = relaxation_parameters_in_RAAR_HPR_HAAR_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

input = getappdata(0,'input');

input.beta_0 = get(handles.beta_0,'String');
input.beta_0 = str2double(input.beta_0);
input.beta_max = get(handles.beta_max,'String');
input.beta_max = str2double(input.beta_max);
input.beta_switch = get(handles.beta_switch,'String');
input.beta_switch = str2double(input.beta_switch);

setappdata(0,'input',input); 

close(gcf);


function beta_0_Callback(hObject, eventdata, handles)
% hObject    handle to beta_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of beta_0 as text
%        str2double(get(hObject,'String')) returns contents of beta_0 as a double


% --- Executes during object creation, after setting all properties.
function beta_0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to beta_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function beta_max_Callback(hObject, eventdata, handles)
% hObject    handle to beta_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of beta_max as text
%        str2double(get(hObject,'String')) returns contents of beta_max as a double


% --- Executes during object creation, after setting all properties.
function beta_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to beta_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function beta_switch_Callback(hObject, eventdata, handles)
% hObject    handle to beta_switch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of beta_switch as text
%        str2double(get(hObject,'String')) returns contents of beta_switch as a double


% --- Executes during object creation, after setting all properties.
function beta_switch_CreateFcn(hObject, eventdata, handles)
% hObject    handle to beta_switch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
