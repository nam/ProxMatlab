function varargout = problem_family_gui(varargin)
% PROBLEM_FAMILY_GUI MATLAB code for problem_family_gui.fig
%      PROBLEM_FAMILY_GUI, by itself, creates a new PROBLEM_FAMILY_GUI or raises the existing
%      singleton*.
%
%      H = PROBLEM_FAMILY_GUI returns the handle to a new PROBLEM_FAMILY_GUI or the handle to
%      the existing singleton*.
%
%      PROBLEM_FAMILY_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROBLEM_FAMILY_GUI.M with the given input arguments.
%
%      PROBLEM_FAMILY_GUI('Property','Value',...) creates a new PROBLEM_FAMILY_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before problem_family_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to problem_family_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help problem_family_gui

% Last Modified by GUIDE v2.5 27-May-2015 14:31:25

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @problem_family_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @problem_family_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before problem_family_gui is made visible.
function problem_family_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to problem_family_gui (see VARARGIN)

% Choose default command line output for problem_family_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% delete input
list = getappdata(0);
list = fieldnames(list);
for i=1:length(list)
    if strcmp(list(i),'input')
        rmappdata(0,'input')
    end
end

% UIWAIT makes problem_family_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = problem_family_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in problem_family.
function problem_family_Callback(hObject, eventdata, handles)
% hObject    handle to problem_family (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns problem_family contents as cell array
%        contents{get(hObject,'Value')} returns selected item from problem_family


% --- Executes during object creation, after setting all properties.
function problem_family_CreateFcn(hObject, eventdata, handles)
% hObject    handle to problem_family (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

list = get(handles.problem_family,'String');
input.problem_family = list(get(handles.problem_family,'Value'));
input.problem_family = char(input.problem_family);

close(gcf);

if strcmp(input.problem_family,'Ptychography')
    setappdata(0,'input',input);
    main_ptychography
elseif strcmp(input.problem_family,'Sudoku')
    input.difficulty = inputdlg('This is a number between 1 and 64. The closer to 64, the harder the problem, and the longer it takes to generate a uniquely solvable puzzle','Difficulty',1);
    input.difficulty = str2double(input.difficulty);
    setappdata(0,'input',input); 
    main_sudoku
end
