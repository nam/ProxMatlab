%                      P_amp.m
%             written on Feb 3, 2012 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               support constraints
%
% INPUT:        input.abs_illumination = OBJECT DOMAIN CONSTRAINT:  0-1 indicator function
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_amp    = the projection IN THE PHYSICAL (time) DOMAIN
%               
% USAGE: p_amp = P_amp(S,u)
%
% Nonstandard Matlab function calls:  MagProj

function p_amp = P_amp(input,u)

if isfield(input,'supp_phase')
    p_amp = input.abs_illumination;
    p_amp(input.supp_phase) = feval('MagProj',input.abs_illumination(input.supp_phase),u(input.supp_phase));
else 
    p_amp = feval('MagProj',input.abs_illumination,u);
end

bla=zeros(size(u));
bla(input.supp_phase)=1;
sum(sum(bla))/size(u,1)/size(u,2);
input.s=floor(sum(sum(bla))*.5);
reshapedabsangle=reshape(abs(angle(p_amp)-angle(input.abs_illumination)),size(u,1)*size(u,2),1);
[tmp,I]=sort(reshapedabsangle,'descend');
tmp=reshape(input.abs_illumination,size(u,1)*size(u,2),1);
p_amp=reshape(p_amp,size(u,1)*size(u,2),1);
tmp(I(1:input.s))=p_amp(I(1:input.s));
p_amp=reshape(tmp,size(u,1),size(u,2));