%                   P_RCAAR_selector.m
%                Created by:  Matthew Tam
%                 University of Newcastle
%                    30th Oct 2013.
%
% Description: Selects the projection operator for the RCAAR algorithm.
% INPUT:  input = a data structure
%         u = the point to be projected.
% OUTPUT: u = projection of u on set indexed by input.proj_iter.
%
% USAGE: u_proj = P_RCAAR_select(input,u) 
%

function u = P_RCAAR_select(input,u)

prox_iter=input.proj_iter; %the projection to be performed.

if(prox_iter==1)
    Prox=input.Prox1;
else %i.e. (iter_proj==2)
    Prox=input.Prox2;
end
u=feval(Prox,input,u);
        
clear Prox, prox_iter;

return