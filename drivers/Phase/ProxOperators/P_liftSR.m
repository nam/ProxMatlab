%                      P_liftSP.m
%             written on Mar 15, 2015 by 
%                   Patrick Neumann
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               nonnegativity and support constraints in the lifted space
%
% INPUT:        input, a data structure with .supp_ampl a vector of indeces of the nonzero elements of the array
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_SP    = the projection IN THE PHYSICAL (time) DOMAIN in
%                         the lifted space
%               
% USAGE: p_liftSP = P_liftSP(input,u)
%
% Nonstandard Matlab function calls:

function p_liftSR = P_liftSR(input,u)

[~,n] = size(u);
supp_ampl = input.supp_ampl;
cheat = zeros(n,1);
cheat(supp_ampl) = 1;
cheat_lift = cheat*cheat';
lifted_supp_ampl = cheat_lift~=0;

p_liftSR = real(u.*lifted_supp_ampl);
