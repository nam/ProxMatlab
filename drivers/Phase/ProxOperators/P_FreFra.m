%                      P_M.m
%             written on May 23, 2002 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto Fourier
%               magnitude constraints
%
% INPUT:        M = nonegative real FOURIER DOMAIN CONSTRAINT
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_M    = the projection IN THE PHYSICAL (time) DOMAIN
%               phat_M = projection IN THE FOURIER DOMAIN
%               
% USAGE: [p_M,phat_M] = P_M(M,u)
%
% Nonstandard Matlab function calls:  MagProj

function p_M = P_FreFra(input,f)

% p_M = feval('ifft2',feval('MagProj',M,feval('fft2',u)));

if input.use_farfield_formula
    if  input.fresnel_nr>0
        fhat = -1i*input.fresnel_nr/(input.Nx*input.Ny*2*pi) * ifftshift(fft2(fftshift(f-input.illumination))) ...
         + ifftshift(input.FT_conv_kernel);
        % input.fhat = -i*input.fresnel_nr/(input.Nx*input.Ny*2*pi) * ifftshift(fft2(fftshift(f-input.illumination))) ...
        %  + ifftshift(input.FT_conv_kernel);
        p_M=feval('MagProj',input.data,fhat);
        % scale M in the data preprocessor so that you don't have to 
        % rescale your fft's each time.  
        p_M=1i/input.fresnel_nr*(input.Nx*input.Ny*2*pi) *ifftshift(ifft2(fftshift(p_M)));
    else
        fhat = (-1i/input.Nx/input.Ny)*ifftshift(fft2(fftshift(f)));
        % input.fhat = (-i/(input.Nx*input.Ny))*ifftshift(fft2(fftshift(f)));
        p_M=feval('MagProj',input.data,fhat);
        p_M=1i*(input.Nx*input.Ny) *ifftshift(ifft2(fftshift(p_M)));
    end
    
else
    fhat = feval('ifft2',input.FT_conv_kernel.*feval('fft2',f))/input.magn;
    p_M=feval('MagProj',input.data,fhat);
    p_M=ifft2(fft2(p_M*input.magn)./input.FT_conv_kernel);
end
%res = input.scaling * res;
end