%                      Approx_PM_Poisson.m
%             written on Feb. 18, 2011 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto Fourier
%               magnitude constraints. This is an approximate
%               projector onto a ball around the data determined
%               by the Kullback-Leibler divergence, as appropriate
%               for Poisson noise.  The analysis of such 
%               approximate projections was studied in 
%               D.R.Luke, ``Local Linear Convergence of Approximate 
%               Projections onto Regularized Sets '', Nonlinear Analysis, 
%               75(2012):1531--1546.
%
% INPUT:        Func_params = a data structure with .data = nonegative real FOURIER DOMAIN CONSTRAINT
%                             .data_ball is the regularization parameter described in 
%                             D. R. Luke, Nonlinear Analysis 75 (2012) 1531–1546.
%                            .TOL2 is an extra tolerance. 
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_M    = the projection IN THE PHYSICAL (time) DOMAIN
%               phat_M = projection IN THE FOURIER DOMAIN
%               
% USAGE: u_epsilon = Approx_PM_Gaussian(Func_params,u)
%
% Nonstandard Matlab function calls:  MagProj

function u_epsilon = Approx_PM_Gaussian(Func_params,u)

TOL2=Func_params.TOL2;
b=Func_params.data.*Func_params.data;
epsilon = Func_params.data_ball;
U = feval('fft2',u);
U0 = feval('MagProj',Func_params.data,U);
U0_sq = U0.*feval('conj',U0);
tmp = U0_sq - b;
h=sum(sum(tmp.*conj(tmp)));
if(h>=epsilon+TOL2)
    u_epsilon = feval('ifft2',U0);
else
    u_epsilon = u;
    % h=h-epsilon;
end


% lambda = .5;
% DUU0 = U-U0;
% DUU0_sq = DUU0.*feval('conj',DUU0);
% term2 = 2*real(DUU0.*feval('conj',U0));
% Ulambda = lambda*U+(1-lambda)*U0;
% % compute damping parameter
% Ulambda_sq = Ulambda.*feval('conj',Ulambda);
% tmp = Ulambda_sq - b;
% h=sum(sum(tmp.*conj(tmp)));
% counter=0;
% lb=0;
% ub=1;
% 
% while ((abs(h-epsilon)>=TOL)&& (counter<40))
%     counter=counter+1;
%     if h-epsilon>0
%         ub=lambda;
%     else
%         lb=lambda;
%     end
%     dUlambda_sq = 2*lambda*DUU0_sq+term2;
%     dh = 2*sum(sum(dUlambda_sq.*tmp));
%     tmp_lambda = min(max(lambda-(h-epsilon)/dh, lb),ub);
%     if(tmp_lambda==lambda)
%         if (ub-lb>=TOL)
%             lambda=.5*(ub+lb);
%         else
%             break
%         end
%     else
%         lambda=tmp_lambda;
%     end
% 
%     Ulambda = lambda*U+(1-lambda)*U0;
%     % compute damping parameter
%     Ulambda_sq = Ulambda.*feval('conj',Ulambda);
%     tmp = Ulambda_sq - b;
%     h=sum(sum(tmp.*conj(tmp)));
% end
% counter;
% u_epsilon=feval('ifft2',Ulambda);


