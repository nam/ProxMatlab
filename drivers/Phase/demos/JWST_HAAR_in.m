%                         JWST_in
%              written on October 6, 2011 by
%                        Russell Luke
%                 University of Goettingen
%
% DESCRIPTION:  parameter input file for the JWST problem described in 
%               ``Optical Wavefront Reconstruction:  Theory and Numerical  Methods'', 
%               D. R. Luke, J. V. Burke and R. G. Lyon. SIAM Review 44:169-224 (2002).
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This is the input file that the user sees/modifies.  It should be simple, 
%% avoid jargon or acronyms, and should be a model for a menu-driven GUI

function prbl = JWST_HAAR_in(prbl)

%% We start very general.
%%
%% What type of problem is being solved?  Classification 
%% is according to the geometry:  Affine, Cone, Convex, 
%% Phase, Affine-sparsity, Nonlinear-sparsity, Sudoku

prbl.problem_family = 'Phase';
prbl.expert=true;

%%==========================================
%% Problem parameters
%%==========================================
%% What is the name of the data file?
prbl.data_filename = 'JWST_data_processor';


%% What type of object are we working with?
%% Options are: 'phase', 'real', 'nonnegative', 'complex'
prbl.object = 'complex';

%% What type of constraints do we have?
%% Options are: 'support only', 'real and support', 'nonnegative and support',
%%              'amplitude only', 'sparse real', 'sparse complex', and 'hybrid'
prbl.constraint = 'amplitude only';

%% What type of measurements are we working with?
%% Options are: 'single diffraction', 'diversity diffraction', 
%%              'ptychography', and 'complex'
prbl.experiment = 'JWST';

%% Next we move to things that most of our users will know 
%% better than we will.  Some of these may be overwritten in the 
%% data processor file which the user will most likely write. 
%% Are the measurements in the far field or near field?
%% Options are: 'far field' or 'near field', 
prbl.distance = 'far field';

%% What are the dimensions of the measurements?
prbl.Nx = 128;
prbl.Ny = prbl.Nx;
prbl.Nz = 1; % do not formulate in the product space

if(strcmp(prbl.distance,'near field'))
    prbl.fresnel_nr = 1*2*pi*prbl.Nx;
    prbl.use_farfield_formula=0;
else
    prbl.fresnel_nr =  0; %1*2*pi*prbl.Nx;
    prbl.use_farfield_formula=1;
end

%% What are the noise characteristics (Poisson or Gaussian or none)?
prbl.noise= 'none'; %'Poisson'; %'none';

%%==========================================
%%  Algorithm parameters
%%==========================================
%% Now set some algorithm parameters that the user should be 
%% able to control (without too much damage)

%% Algorithm:
prbl.method = 'HAAR'; %'Wirtinger';%AP; HPR;  QNAP
prbl.numruns=1; % the only time this parameter will
% be different than 1 is when we are
% benchmarking...not something a normal user
% would be doing.
prbl.keep_log=0;


%% The following are parameters specific to RAAR, HPR, and HAAR that the 
%% user should be able to set/modify.  Surely
%% there will be other algorithm specific parameters that a user might 
%% want to play with.  Don't know how best 
%% to do this.  Thinking of a GUI interface, we could hard code all the 
%%  parameters the user might encounter and have the menu options change
%% depending on the value of the prbl.method field. 
%% do different things depending on the chosen algorithm:


    %% maximum number of iterations and tolerances
    prbl.MAXIT = 500;
    prbl.TOL = 1e-8;

    %% relaxaton parameters in RAAR, HPR and HAAR
    prbl.beta_0 = 0.5;                % starting relaxation prameter (only used with
    % HAAR, HPR and RAAR)
    prbl.beta_max =0.5;             % maximum relaxation prameter (only used with
    % HAAR, RAAR, and HPR)
    prbl.beta_switch = 20;           % iteration at which beta moves from beta_0 -> beta_max

    %% parameter for the data regularization 
    %% need to discuss how/whether the user should
    %% put in information about the noise
    prbl.data_ball = 1e-3;  
    % the above is the percentage of the gap
    % between the measured data and the
    % initial guess satisfying the
    % qualitative constraints.  For a number 
    % very close to one, the gap is not expected 
    % to improve much.  For a number closer to 0
    % the gap is expected to improve a lot.  
    % Ultimately the size of the gap depends
    % on the inconsistency of the measurement model 
    % with the qualitative constraints.

%%==========================================
%% parameters for plotting and diagnostics
%%==========================================
prbl.diagnostic = true; % to turn this off, just comment out
prbl.verbose = 1; % options are 0 or 1
prbl.graphics = 1; % whether or not to display figures, options are 0 or 1.
                   % default is 1.
prbl.anim = 1;  % whether or not to disaply ``real time" reconstructions
                % options are 0=no, 1=yes, 2=make a movie
                % default is 1.
prbl.graphics_display = 'JWST_graphics'; % unless specified, a default 
                            % plotting subroutine will generate 
                            % the graphics.  Otherwise, the user
                            % can write their own plotting subroutine

%%======================================================================
%%  Technical/software specific parameters
%%======================================================================
%% Given the parameter values above, the following technical/algorithmic
%% parameters are automatically set.  The user does not need to know 
%% about these details, and so probably these parameters should be set in 
%% a module one level below this one.  

end
