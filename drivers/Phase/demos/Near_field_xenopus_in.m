%                      Near_field_xenopus_in.m
%              written on June 1, 2017 by
%                        Russell Luke
%                 University of Goettingen
%
% DESCRIPTION:  parameter input file for main_ProxToolbox
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This is the input file that the user sees/modifies.  It should be simple, 
%% avoid jargon or acronyms, and should be a model for a menu-driven GUI

function prbl = Near_field_xenopus_in(prbl)

%% We start very general.
%%==========================================
%% Problem parameters
%%==========================================
% What type of problem are we dealing with?
prbl.problem_family = 'Phase';
prbl.expert=false;

%% What is the name of the data file?
prbl.data_filename = 'Goettingen_data_processor';


%% What type of object are we working with?
%% Options are: 'phase', 'real', 'nonnegative', 'complex'
prbl.object = 'phase';

%% What type of constraints do we have?
%% Options are: 'support only', 'real and support', 'nonnegative and support',
%%              'amplitude only', 'sparse real', 'sparse complex', and 'hybrid'
prbl.constraint = 'phase on support';


%% What type of measurements are we working with?
%% Options are: 'single diffraction', 'diversity diffraction', 
%%              'ptychography', and 'complex'
prbl.experiment = 'xenopus';

%% Next we move to things that most of our users will know 
%% better than we will.  Some of these may be overwritten in the 
%% data processor file which the user will most likely write. 
%% Are the measurements in the far field or near field?
%% Options are: 'far field' or 'near field', 
prbl.distance = 'near field';

%% What are the dimensions of the measurements?
prbl.step_up = 0;            % number of dyads to increase
                             %   resolution.  step_up = -1 or -2
                             %   REDUCES resolution

%% What are the noise characteristics (Poisson or Gaussian)?
prbl.noise='Poisson';
prbl.seed = 337;   %sum(clock);           % seed for random initial guess.
prbl.snr = 100;                    % noise level.  For Gaussian noise, the larger snr 
                             % the cleaner the data.  For Poisson noise, the
                             % larger snr the noisier the data.  
%%==========================================
%%  Algorithm parameters
%%==========================================
%% Now set some algorithm parameters that the user should be 
%% able to control (without too much damage)

%% Algorithm:
prbl.method = 'RAAR'; 
prbl.numruns=1; % the only time this parameter will
% be different than 1 is when we are
% benchmarking...not something a normal user
% would be doing.
prbl.restart=0; % if you want to keep the solution from a previous
                %   run, set 'restart=1', or to load
                %   from a file, set 'restart=2'.

%% maximum number of iterations and tolerances
prbl.MAXIT = 40;
prbl.TOL = 7e-8;
prbl.TOL2 = 1e-15;

%% relaxaton parameters in RAAR, HPR and HAAR
prbl.beta_0 = 1.0;                % starting relaxation prameter (only used with
% HAAR, HPR and RAAR)
prbl.beta_max =0.50;             % maximum relaxation prameter (only used with
% HAAR, RAAR, and HPR)
prbl.beta_switch = 4;           % iteration at which beta moves from beta_0 -> beta_max

%% parameter for the data regularization
%% need to discuss how/whether the user should
%% put in information about the noise
prbl.data_ball = .999826e-0;
% prbl.data_ball = .9998261e-0;
% the above is the percentage of the gap
% between the measured data and the
% initial guess satisfying the
% qualitative constraints.  For a number
% very close to one, the gap is not expected
% to improve much.  For a number closer to 0
% the gap is expected to improve a lot.
% Ultimately the size of the gap depends
% on the inconsistency of the measurement model
% with the qualitative constraints.
                
%%==========================================
%% parameters for plotting and diagnostics
%%==========================================
prbl.diagnostic = true; % to stop the diagnostics, just comment this field out.
prbl.verbose = 1; % options are 0 or 1
prbl.graphics = 1; % whether or not to display figures, options are 0 or 1.
                   % default is 1.
prbl.anim = 1;  % whether or not to disaply ``real time" reconstructions
                % options are 0=no, 1=yes, 2=make a movie
                % default is 1.
prbl.graphics_display = 'Phase_graphics'; % unless specified, a default 
                            % plotting subroutine will generate 
                            % the graphics.  Otherwise, the user
                            % can write their own plotting subroutine

%%======================================================================
%%  Technical/software specific parameters
%%======================================================================
%% Given the parameter values above, the following technical/algorithmic
%% parameters are automatically set.  The user does not need to know 
%% about these details, and so probably these parameters should be set in 
%% a module one level below this one.  

end


                         
