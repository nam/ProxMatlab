function B_roi = ExtractROI(A,pA,dim,B)
% extracts region of interest (roi) from matrix A with desired pixel
% dimensions
% INPUT VARIABLES
% A: huge matrix
% (pA.i,pA.j) central point of roi
% dim.A.i pixel dimension of A along row - direction
% dim.A.j pixel dimension of A along column - direction
% dim.B.i desired dimension in B - along row
% dim.B.j desired dimension in B - along column
% B.i desired size of B: rows
% B.j desired size of B: columns
% LOCAL VARIABLES
% B_roi roi of A with central point pA with dimensions dim.B

% coordinate system with desired dimensions
xi = dim.B.j*((1:B.j)-ceil(B.j/2+1));
yi = dim.B.i*((1:B.i)-ceil(B.i/2+1));
[XI,YI] = meshgrid(xi,yi);

ximn = min(XI(:));
ximx = max(XI(:));
yimn = min(YI(:));
yimx = max(YI(:));

% calculate #pixels needed from A to build B
x =[-ceil(abs(ximn)/dim.A.j):0,1:ceil(abs(ximx)/dim.A.j)];
n.j = length(x);
y =[-ceil(abs(yimn)/dim.A.i):0,1:ceil(abs(yimx)/dim.A.i)];
n.i  = length(y);

% calculate ROI in A 
Roi.A.i = pA.i+x;
Roi.A.j = pA.j+y;

% in case out of boundary of A -> surround by ones
[A_i,A_j] = size(A);

if Roi.A.j(1)<1
    tmp = ones(A_i,abs(Roi.A.j(1)-1));
    A = [tmp, A];
    clear tmp;
   Roi.A.j = Roi.A.j+abs(Roi.A.j(1))+1; 
end

if Roi.A.j(end)>A_j
    tmp = ones(A_i,abs(Roi.A.j(end)-A_j));
    A = [A,tmp];
    clear tmp;
end

[A_i,A_j] = size(A);

if Roi.A.i(1)<1
    tmp = ones(abs(Roi.A.i(1)-1),A_j);
    A = [tmp; A];
    clear tmp;
    Roi.A.i = Roi.A.i+abs(Roi.A.i(1))+1;
end

if Roi.A.i(end)>A_i
    tmp = ones(abs(Roi.A.i(end)-A_i),A_j);
    A = [A;tmp];
    clear tmp;
end

% total ROI in A
RoiA = A(Roi.A.i,Roi.A.j);
RoiA =RoiA.';

% build coordinates systems for RoiA 
[X,Y] = meshgrid(dim.A.j*x,dim.A.i*y);
%look for problems in the coordinate systems
xmn = min(X(:));
xmx = max(X(:));
ymn = min(Y(:));
ymx = max(Y(:));

if xmn>ximn || xmx<ximx || ymn>yimn || ymx<yimx 
    error('Problems in ExtractROI: setting up coordinate systems failed!')
end

% interpolation of RoiA in order to get B_roi with desired dimensions
B_roi = interp2(X,Y,RoiA,XI,YI);

B_roi = B_roi.';

end