%                  Siemens_processor.m
%                written on May 27, 2012 by
%                     Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  
%
% INPUT: input = a data structure
% OUTPUT: input = a data structure
%
% USAGE: input = Siemens_processor(input) 
%
% Data loaded: Siemens_star_200px.mat
% ProxToolbox function calls:  
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data reader/processor

function method_input = Siemens_processor(method_input)


data_ball = method_input.data_ball;

noise = method_input.noise;
snr = method_input.data_ball;

addpath('../InputData/Phase')

[fid,message]=fopen('Siemens_star_200px.mat','r','l');
if (fid<0)
   disp('******************************************************************')
   disp('* Input data missing.  Please download the phase input data from *') 
   disp('*    http://vaopt.math.uni-goettingen.de/data/Phase.tar.gz       *')
   disp('* Save and unpack the Phase.tar.gz datafile in the               *')
   disp('*    ProxMatlab/InputData subdirectory                           *')
   disp('******************************************************************')
else
    disp(['Loading data file: Siemens_star_200px.mat'])
    load -ascii Siemens_star_200px.mat
    S = Siemens_star_200px;
    clear Siemens_star_200px
    fclose(fid);
end
S=ZeroPad(S);
[m,n]=size(S);
method_input.Nx=n;
method_input.Ny=m;

if(strcmp(method_input.object,'real')||strcmp(method_input.object,'nonnegative'))
    M=abs(fft2(S));
    % method_input.data_ball=method_input.Nx*method_input.Ny*data_ball;
    method_input.rt_data=M;
    % standard for the main program is that
    % the data field is the magnitude SQUARED
    % in Luke.m this is changed to the magnitude.
    method_input.data=M.^2;
    method_input.norm_rt_data=norm(S,'fro');
    method_input.norm_data=method_input.norm_rt_data^2;
    method_input.data_zeros = find(M==0);
    % below, we make the support too small to increase the inconsistency
    % Stmp=zeros(size(S));
    % Stmp((m/2-m/4):(m/2+m/4),(n/2-n/4):(n/2+n/4))=S((m/2-m/4):(m/2+m/4),(n/2-n/4):(n/2+n/4));
    % S=Stmp;
    method_input.support_idx = find(S~=0);
    
    % use the amplitude field to represent the
    % support constraint.
    method_input.amplitude = method_input.norm_rt_data*S/(norm(S,'fro'));
    method_input.amplitude = method_input.amplitude/norm(method_input.amplitude,'fro')*method_input.norm_rt_data(1);
    method_input.supp_phase = find(ones(m,n));
    method_input.illumination_phase = find(ones(m,n));
elseif(strcmp(method_input.object,'complex'))
    % put some phase across S
    points = method_input.Nx;
    method_input.norm_rt_data=norm(S,'fro');
    method_input.norm_data=method_input.norm_rt_data^2;
    % use the amplitude field to represent the
    % support constraint.
    method_input.amplitude = method_input.norm_rt_data*S/(norm(S,'fro'));
    % input.amplitude = input.amplitude/norm(input.amplitude,'fro')*input.norm_rt_data(1);
    G=zeros(size(S));% Gaussian(points,10,[points/2+1,points/2+1]);
    W=S*exp(1i*2*pi*G);
    M=abs(fft2(W));
    % method_input.data_ball=method_input.Nx*method_input.Ny*data_ball;
    method_input.rt_data=M;
    % standard for the main program is that
    % the data field is the magnitude SQUARED
    % in Luke.m this is changed to the magnitude.
    method_input.data=M.*M;
    method_input.data_zeros = find(M==0);
    method_input.support_idx = find(S~=0);    
elseif(strcmp(method_input.object,'phase'))
    % put some phase across S
    points = method_input.Nx;
    % use the amplitude field to represent the
    % support constraint.
    method_input.amplitude = ones(size(S));  
    G=Gaussian(points,10,[points/2+1,points/2+1]);
    W=exp((1i*2*pi)*S.*G);
    method_input.norm_rt_data=norm(W,'fro');
    M=abs(fft2(W));
    % method_input.data_ball=method_input.Nx*method_input.Ny*data_ball;
    method_input.rt_data=M;
    method_input.norm_data=method_input.norm_rt_data^2;
    % standard for the main program is that
    % the data field is the magnitude SQUARED
    % in Luke.m this is changed to the magnitude.
    method_input.data=M.*M;
    method_input.data_zeros = find(M==0);
    method_input.support_idx = find(W~=0);    
    method_input.supp_phase = S;
    method_input.illumination_phase = S;    
end
% initial guess
method_input.u_0 = ifft2(M.*exp(2*pi*1i*rand(m,n)));
% method_input.u_0 = ifft2(M.*exp(2*pi*1i*Gaussian(N,N/2,[N/2+1,N/2+1]))).*S;
% method_input.u_0 = method_input.u_0/norm(method_input.u_0,'fro')*method_input.amplitude; 

method_input.product_space_dimension = 1;
method_input.truth = S;
method_input.truth_dim = size(S);
method_input.norm_truth=norm(S,'fro');

end



