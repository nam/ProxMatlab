function prbl = dictyM103_stx6_600frames(prbl)
%%  Parameters of the forward problem
prbl.OperatorName = 'FresnelPropExpAmpl';
prbl.syntheticdata_flag = false;


%load intensity
load dictyM103_stx6_600frames;
data = DP;     %diffraction pattern (image) on detector

%data = DPmed; %diffraction pattern with 3x3 median filter

prbl.data= data;
prbl.Nx =size(data,2);
prbl.Ny = size(data,1);


%supp=supp2; %use tight support
supp =supp; %use loose support

% define support

%load supp_for_data_without_outliers.mat;
N=prbl.Nx;
xn = [-N:2:N-1]/N;
N=prbl.Ny;
yn = [-N:2:N-1]/N;
[X,Y]=meshgrid(yn,xn);
prbl.supp_phase = find(supp>0);
prbl.supp_ampl = [];%find(supp_ampl>0);
%prbl.supp_ampl = prbl.supp_phase;%find(supp_ampl>0);

% The following 4 variables are included for convenience if experimental
% data are used. They are used only for the computation of the Fresnel
% number and the magnification factor. 
% If simulated data are used, the Fresnel number may be provided directly.
% In this case the magnification number may always be set to 1 since the
% case magn=M can be reduced to the case magn=1 by replacing fresnel_nr by
% M*fresnel_nr and intensity by intensity/M.
%
Gamma = P.exp.z_eff;                % distance object plane to observation plane [m]
%R = 5e-3;                 % distance source to observation plane [m]
lambda = P.exp.lambda;           % x-ray wavelength [m]
p_eff = P.det.dx/P.exp.M; %effective pixel size
prbl.x = [0:prbl.Nx-1]*p_eff;
prbl.y = [0:prbl.Ny-1]*p_eff;
prbl.fresnel_nr = 2*pi * (p_eff*p_eff*prbl.Nx*prbl.Ny)/(Gamma*lambda)
%prbl.fresnel_nr=2*pi*P.fresnelnumber;
prbl.magn = 1;
prbl.intensity = 1;   
prbl.sSobo = 0.25;
%prbl.init_guess = zeros(size(prbl.supp_phase));
prbl.phase_init_guess = [];
prbl.ampl_init_guess = [];
prbl.product_space_dimension=1; % this parameter is 
% used in Russell's algorithms to distinguish between
% problems formulated on the product space (dimension >1) 
% from ``ordinary" problems (dimension=1)

prbl.Nphase = length(prbl.supp_phase);
%% Defines the initial guess if one is not already specified, or if the 
%% specified initial guess is not in the right form (RL)?
if (length(prbl.phase_init_guess) ~= length(prbl.supp_phase) || ...
        length(prbl.ampl_init_guess) ~= length(prbl.supp_ampl))
    prbl.phase_init_guess = zeros(length(prbl.supp_phase),1);
    prbl.ampl_init_guess = zeros(length(prbl.supp_ampl),1);
end
prbl.init_guess = [prbl.phase_init_guess;prbl.ampl_init_guess];

[F,prbl] = feval(prbl.OperatorName,prbl);
    [~,prbl] =F.evaluate(F,prbl.init_guess);% Russells package needs one call of the Fresnelpropagator to work (RH)
    if(strcmp(prbl.method_class,'Hohage'))
    prbl.data=data;
else %Some scaling issues, dont know what Thorstens .scaling is for (RH/RL)
    prbl.data=data/prbl.scaling;
    prbl.rt_data = sqrt(prbl.data); 
    prbl.data_zeros = find(data==0);
    prbl.norm_rt_data = norm(prbl.rt_data,'fro');
    prbl.data_ball=prbl.Nx*prbl.Ny*prbl.data_ball;
end

% diagonal of inverse Gram matrix in image space
% this allows a quadratic approximation of the Poisson log-likelihood for
% high count rates

%% Makes a call to Thorsten's Operator file.  Probably should
%% be in the main_HohageLuke.m master file and here, 
%% but I needed to do it this way to avoid 
%% problems with the initialization.
%[prbl_par, prbl_par_ref] = feval(prbl_par.OperatorName,prbl_par); 
%%Takes place in line 72.

% Careful:  this is specific to the type of data and should
prbl.y_err = ones(size(prbl.data(:)));

end