
%% This is the input file that the user sees/modifies.  It should be simple, 
%% avoid jargon or acronyms, and should be a model for a menu-driven GUI

function prbl = dicty_hires_RAAR()

%% We start very general.
%% What type of problem is being solved?  Classification 
%% is according to the geometry:  Affine, Cone, Convex, 
%% Phase, Affine-sparsity, Nonlinear-sparsity, Sudoku

prbl.problem_family = 'Phase';

%%==========================================
%% Problem parameters
%%==========================================
%% What is the name of the data file?
prbl.data_filename = 'dicty_hires_dataprocessor';

%% What type of constraints do we have?
%% Options are: 'support only', 'real and support', 'nonnegative and support',
%%              'amplitude only', 'sparse real', 'sparse complex', and 'hybrid'
prbl.constraint = 'amplitude with support and sparsity';

%% What type of measurements are we working with?
%% Options are: 'single diffraction', 'diversity diffraction', 
%%              'ptychography', and 'complex', new: 'diffractionHohage'
prbl.experiment = 'single diffraction';

%% Next we move to things that most of our users will know 
%% better than we will.  Some of these may be overwritten in the 
%% data processor file which the user will most likely write. 
%% Are the measurements in the far field or near field?
%% Options are: 'far field' or 'near field', 
prbl.distance = 'near field';

%% What are the noise characteristics (Poisson or Gaussian)?
prbl.noise='none';
%% parameters of the regularization method

% regularization method
prbl.method = 'RAAR_RMS'; 
prbl.numruns=1; % the only time this parameter will
                  % be different than 1 is when we are
                  % benchmarking...not something a normal user
                  % would be doing.

    %% maximum number of iterations and tolerances
    global max_it;
    global beta_0;
    global beta_max;
    global beta_switch;
    prbl.MAXIT = max_it;
    prbl.TOL = 1e-12;

    %% relaxaton parameters in RAAR, HPR and HAAR
    prbl.beta_0 = beta_0;                % starting relaxation prameter (only used with
    % HAAR, HPR and RAAR)
    prbl.beta_max =beta_max;             % maximum relaxation prameter (only used with
    % HAAR, RAAR, and HPR)
    prbl.beta_switch = beta_switch;           % iteration at which beta moves from beta_0 -> beta_max

    %% parameter for the data regularization 
    %% need to discuss how/whether the user should
    %% put in information about the noise
    prbl.data_ball = 1e-5;  
    prbl.product_space_dimension = 1;
    % the above is the percentage of the gap
    % between the measured data and the
    % initial guess satisfying the
    % qualitative constraints.  For a number 
    % very close to one, the gap is not expected 
    % to improve much.  For a number closer to 0
    % the gap is expected to improve a lot.  
    % Ultimately the size of the gap depends
    % on the inconsistency of the measurement model 
    % with the qualitative constraints.


%%==========================================
%% parameters for plotting and diagnostics
%%==========================================
prbl.verbose = 1; % options are 0 or 1
prbl.graphics = 1; % whether or not to display figures, options are 0 or 1.
                   % default is 1.
prbl.anim = 0;  % whether or not to disaply ``real time" reconstructions
                % options are 0=no, 1=yes, 2=make a movie
                % default is 1.
prbl.graphics_display = 'Phase_graphics'; % unless specified, a default 
                            % plotting subroutine will generate 
                            % the graphics.  Otherwise, the user
                            % can write their own plotting subroutine

%%======================================================================
%%  Technical/software specific parameters
%%======================================================================
%% Given the parameter values above, the following technical/algorithmic
%% parameters are automatically set.  The user does not need to know 
%% about these details, and so probably these parameters should be set in 
%% a module one level below this one.  

end