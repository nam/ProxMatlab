%                      Phase_graphics.m
%                  written on May 23, 2012 by 
%                         Russell Luke
%       Inst. Fuer Numerische und Angewandte Mathematik
%                    Universitaet Gottingen
%
%
% DESCRIPTION:  Script driver for viewing results from projection
%               algorithms on various toy problems
%
% INPUT:  
%              method = character string for the algorithm used.
%         true_object = the original image
%                 u_0 = the initial guess
%                   u = the algorithm "fixed point"
%              change = the norm square of the change in the
%                              iterates
%              error  = squared set distance error at each
%                              iteration
%              noneg = the norm square of the nonnegativity/support
%                              constraint at each iteration
%              gap  = the norm square of the gap distance, that is
%                     the distance between the projections of the
%                     iterates to the sets
%
% OUTPUT:       graphics
% USAGE: Phase_graphics(method_input,method_output)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function success = Phase_graphics(method_input, method_output)
              
method=method_input.method;
beta0 = method_input.beta_0;
beta_max = method_input.beta_max;
u_0 = method_input.u_0;
u = method_output.u1(:,:,1);
u2 = method_output.u2(:,:,1);
iter = method_output.stats.iter;
change = method_output.stats.change;

if(any(strcmp('time', fieldnames(method_output.stats))))
    time = method_output.stats.time;
else
    time=999
end


  figure(904)
      subplot(2,2,1), imagesc(abs(u)), colormap gray; axis equal tight; colorbar; title('best approximation amplitude - physical constraint satisfied'); drawnow;
      label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
      title(label)
    subplot(2,2,2); imagesc(real(u)); colormap gray; axis equal tight; colorbar; title('best approximation phase - physical constraint satisfied'); drawnow; %caxis([4.85,5.35]); caxis([-0.9 -0.4]);
    subplot(2,2,3); imagesc(abs(u2)); colormap gray; axis equal tight; colorbar; title('best approximation amplitude - Fourier constraint satisfied'); drawnow; %caxis([4.85,5.35]);
    subplot(2,2,4); imagesc(real(u2)); colormap gray; axis equal tight; colorbar; title('best approximation phase - Fourier constraint satisfied'); drawnow; %caxis([4.85,5.35]); caxis([-0.9 -0.4]);

    figure(900);

    subplot(2,2,1); imagesc(abs(u)); colormap gray; axis equal tight; colorbar; title('best approximation amplitude - physical constraint satisfied'); drawnow; 
    subplot(2,2,2); imagesc(real(u)); colormap gray; axis equal tight; colorbar; title('best approximation phase - physical constraint satisfied'); drawnow; %caxis([4.85,5.35]);
    label = [ 'iteration', ', time = ',num2str(time), 's'];
    subplot(2,2,3);   semilogy(change),xlabel(label),ylabel(['log of change in iterates'])
    label = ['Algorithm: ',method];
    title(label)
    if(any(strcmp('diagnostic', fieldnames(method_input))))
        gap  = method_output.stats.gap;
        label = [ 'iteration', ', time = ',num2str(time), 's'];        
        subplot(2,2,4);   semilogy(gap),xlabel(label),ylabel(['log of the gap distance'])
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(label)
    end
    


success = 1;
