%                      Phase_animation.m
%                  written on May 23, 2012 by 
%                         Russell Luke
%       Inst. Fuer Numerische und Angewandte Mathematik
%                    Universitaet Gottingen
%
%
%
function [output] = Phase_animation(input,output)

if(input.Nx==1||input.Ny==1)
       figure(904)
      % colormap('gray');
    if (input.Nx==1)
      plot((real(output.u(:,input.product_space_dimension))));
    elseif(input.Ny==1)
      plot((real(output.u(input.product_space_dimension,:))));
    end
      title(['iteration ',num2str(input.iter)])
      axis off
      % set(h,'EraseMode','xor');
      drawnow 
else
    if(strcmp(input.distance,'near field'))
        figure(902)
        % colormap('gray');
        imagesc((angle(output.u(:,:,input.product_space_dimension))));
        title(['Phase of reconstructed object, iteration ',num2str(input.iter)])
        drawnow

        % figure(903)
        % % colormap('gray');
        % imagesc((real(feval(input.Prox1,input,output.u(:,:,input.product_space_dimension)))));
        % title(['real part of reconstructed object, iteration ',num2str(input.iter)])
        % axis off
        % drawnow

    else
        figure(904)
        % colormap('gray');
        imagesc((real(output.u(:,:,input.product_space_dimension))));
        title(['iteration ',num2str(input.iter)])
        axis off
        drawnow
    end
end
end
