close all;
addpath('scratch/ProxToolbox_v2.0/drivers/Ptychography');
addpath('scratch/ProxToolbox_v2.0/drivers/Ptychography/Ptychography_data/Robin');


%resultsDir = '140406Ptychography';i
resultsDir = 'scratch/ProxToolbox_v2.0/OutputData/restrictedPupil/140426Ptychography';

load([resultsDir '/Ptychography_in_PHeBIE_restrictedPupil_run1.input.mat']);
trueObject = input.sample_plane;
trueProbe  = input.probe;
%clear input

%methods = {'PALM', 'Rodenburg', 'Thibault', 'Nesterov_PALM', 'Thibault_AP'};
methods = {'PHeBIE','Rodenburg','Thibault','Thibault_RAAR','Thibault_AP'};
numMethods = length(methods);

rmsObj = zeros(numMethods,300);
rmsPro = zeros(numMethods,300);
rFact  = zeros(numMethods,300);
objVal = zeros(numMethods,300);
normCh = zeros(numMethods,300);

object = zeros([size(trueObject,1),size(trueObject,2),numMethods]);
probe  = zeros([size(trueProbe,1),size(trueProbe,2),numMethods]);

for m=1:numMethods
    thisFile = ['/Ptychography_in_' methods{m} '_restrictedPupil_run1.output.mat'];
    load([resultsDir thisFile]);
    
    rmsObj(m,:) = output.stats.customError(:,1);
    rmsPro(m,:) = output.stats.customError(:,2);
    rFact(m,:)  = output.stats.customError(:,4);
    objVal(m,:) = output.stats.customError(:,5);
    normCh(m,:) = output.stats.change;
    
    object(:,:,m) = output.u_final.object;
    probe(:,:,m)  = output.u_final.probe;
    
    clear output
end

%% Post-processing objects and probes (for plotting purposes).
normProbe  = norm(trueProbe,'fro');
normObject = norm(trueObject,'fro');
%trueObject = trueObject * exp(-1i*sum(sum(angle(trueObject))) / length(trueObject));
for m=1:numMethods
    object(:,:,m) = object(:,:,m) * normObject / norm(object(:,:,m),'fro');
    probe(:,:,m)  = probe(:,:,m)  * normProbe / norm(probe(:,:,m),'fro');
end

%% RMS-object
figure(1);
plot(transpose(rmsObj(1:3,:)))
title('RMS-object')
legend('PHeBIE','Rodenburg','Thibault')
ylabel('Object RMS')
xlabel('Iterations')

figure(2)
plot(transpose(rmsObj))
title('RMS-object')
legend('PHeBIE','Rodenburg','Thibault','Thibault RAAR','Thibault AP')
ylabel('Object RMS')
xlabel('Iterations')


%% RMS-probe
figure(3);
plot(transpose(rmsPro(1:3,:)))
title('RMS-Probe')
legend('PHeBIE','Rodenburg','Thibault')
ylabel('Probe RMS')
xlabel('Iterations')

figure(4)
plot(transpose(rmsPro))
title('RMS-Probe')
legend('PHeBIE','Rodenburg','Thibault','Thibault RAAR','Thibault AP')
ylabel('Probe RMS')
xlabel('Iterations')


%% R-factor
figure(5);
plot(transpose(rFact(1:3,:)))
title('R-Factor')
legend('PHeBIE','Rodenburg','Thibault')
ylabel('R-Factor')
xlabel('Iterations')

figure(6)
plot(transpose(rFact))
title('R-Factor')
legend('PHeBIE','Rodenburg','Thibault','Thibault RAAR','Thibault AP')
ylabel('R-Factor')
xlabel('Iterations')



%% Objective value
figure(7);
semilogy(transpose(objVal(1:3,:)))
title('Objective Value')
legend('PHeBIE','Rodenburg','Thibault')
ylabel('F(x^k,y^k,z^k)')
xlabel('Iterations (i.e., k)')

figure(8)
semilogy(transpose(objVal))
title('Objective Value')
legend('PHeBIE','Rodenburg','Thibault','Thibault RAAR','Thibault AP')
ylabel('F(x^k,y^k,z^k)')
xlabel('Iterations (i.e., k)')



%% change norm
figure(9);
semilogy(transpose(normCh(1:3,:)))
title('Step-size')
legend('PHeBIE','Rodenburg','Thibault')
ylabel('||u^k-u^{k-1}||^2')
xlabel('Iterations (i.e., k)')

figure(10)
semilogy(transpose(normCh))
title('Step-size')
legend('PHeBIE','Rodenburg','Thibault','Thibault RAAR','Thibault AP')
ylabel('||u^k-u^{k-1}||^2')
xlabel('Iterations (i.e., k)')



%% Reconstructions
figure(11)

ampLimit = [min([min(min(min(abs(trueObject)))),min(min(min(abs(object))))]) max([max(max(max(abs(trueObject)))),max(max(max(abs(object))))])];
phaseLimit = [-pi +pi]; %[min(min(min(angle(object(:,:,3))))) max(max(max(angle(object(:,:,3)))))];

a(1)=subplot(2,4,1);
imagesc(abs(trueObject));
axis equal tight
title('True Object Amplitude')
colormap gray;
b(1)=subplot(2,4,5);
imagesc(angle(trueObject));
axis equal tight
title('True Object Phase')
colormap gray;
caxis(phaseLimit);

a(2)=subplot(2,4,2);
imagesc(abs(object(:,:,1)));
axis equal tight
title('PHeBIE: Object Amplitude')
colormap gray;
caxis(ampLimit)
b(2)=subplot(2,4,6);
imagesc(angle(object(:,:,1)));
axis equal tight
title('PHeBIE: Object Phase')
colormap gray;
caxis(phaseLimit);

a(3)=subplot(2,4,3);
imagesc(abs(object(:,:,2)));
axis equal tight
title('Rodenburg: Object Amplitude')
colormap gray;
caxis(ampLimit)
b(3)=subplot(2,4,7);
imagesc(angle(object(:,:,2)));
axis equal tight
title('Rodenburg: Object Phase')
colormap gray;
caxis(phaseLimit);

a(4)=subplot(2,4,4);
imagesc(abs(object(:,:,3)));
axis equal tight
title('Thibault: Object Amplitude')
colormap gray;
pos_a4 = get(a(4),'Position');
colorbar
set(a(4),'Position',pos_a4);
caxis(ampLimit)
b(4)=subplot(2,4,8);
imagesc(angle(object(:,:,3)));
axis equal tight
title('Thibault: Object Phase')
colormap gray;
pos_b4 = get(b(4),'Position');
colorbar
set(b(4),'Position',pos_b4);
caxis(phaseLimit);

position = get(gcf,'Position');
set(gcf, 'Position', [position(1:2) 1.5*position(3:4)]);
paperPosition = get(gcf,'PaperPosition');
set(gcf, 'PaperPosition', [paperPosition(1:2) 1.5*paperPosition(3:4)]);


figure(12)

a(1)=subplot(2,6,1);
imagesc(abs(trueObject));
axis equal tight
title('True Object Amplitude')
colormap gray;
b(1)=subplot(2,6,7);
imagesc(angle(trueObject));
axis equal tight
title('True Object Phase')
colormap gray;
caxis(phaseLimit);

a(2)=subplot(2,6,2);
imagesc(abs(object(:,:,1)));
axis equal tight
title('PHeBIE: Object Amplitude')
colormap gray;
caxis(ampLimit)
b(2)=subplot(2,6,8);
imagesc(angle(object(:,:,1)));
axis equal tight
title('PHeBIE: Object Phase')
colormap gray;
caxis(phaseLimit);

a(3)=subplot(2,6,3);
imagesc(abs(object(:,:,2)));
axis equal tight
title('Rodenburg: Object Amplitude')
colormap gray;
caxis(ampLimit)
b(3)=subplot(2,6,9);
imagesc(angle(object(:,:,2)));
axis equal tight
title('Rodenburg: Object Phase')
colormap gray;
caxis(phaseLimit);

a(4)=subplot(2,6,4);
imagesc(abs(object(:,:,3)));
axis equal tight
title('Thibault: Object Amplitude')
colormap gray;
caxis(ampLimit)
b(4)=subplot(2,6,10);
imagesc(angle(object(:,:,3)));
axis equal tight
title('Thibault: Object Phase')
colormap gray;
caxis(phaseLimit);

a(5)=subplot(2,6,5);
imagesc(abs(object(:,:,4)));
axis equal tight
title('Thibault RAAR: Object Amplitude')
colormap gray;
caxis(ampLimit)
b(5)=subplot(2,6,11);
imagesc(angle(object(:,:,4)));
axis equal tight
title('Thibault RAAR: Object Phase')
colormap gray;
caxis(phaseLimit);

a(6)=subplot(2,6,6);
imagesc(abs(object(:,:,5)));
axis equal tight
title('Thibault AP: Object Amplitude')
colormap gray;
pos_a6 = get(a(6),'Position');
colorbar
set(a(6),'Position',pos_a6);
caxis(ampLimit)
b(6)=subplot(2,6,12);
imagesc(angle(object(:,:,5)));
axis equal tight
title('Thibault AP: Object Phase')
colormap gray;
pos_b6 = get(b(6),'Position');
colorbar
set(b(6),'Position',pos_b6);
caxis(phaseLimit);

position = get(gcf,'Position');
set(gcf, 'Position', [position(1:2) 2*position(3:4)]);
paperPosition = get(gcf,'PaperPosition');
set(gcf, 'PaperPosition', [paperPosition(1:2) 2*paperPosition(3:4)]);


figure(13)

p = trueProbe;
c(1)=subplot(1,4,1);
imagesc(hsv2rgb(im2hsv(p./max(max(abs(p))), 1)));
axis equal tight
title('True Probe')

p = probe(:,:,1);
c(2) = subplot(1,4,2);
imagesc(hsv2rgb(im2hsv(p./max(max(abs(p))), 1)));
axis equal tight
title('PHeBIE: Probe')

p = probe(:,:,2);
c(3) = subplot(1,4,3);
imagesc(hsv2rgb(im2hsv(p./max(max(abs(p))), 1)));
axis equal tight
title('Rodenburg: Probe')

p = probe(:,:,3);
c(4) = subplot(1,4,4);
imagesc(hsv2rgb(im2hsv(p./max(max(abs(p))), 1)));
axis equal tight
title('Thibault: Probe')
% pos_c4 = get(c(4),'Position');
% colorbar
% set(c(4),'Position',pos_c4);

position = get(gcf,'Position');
set(gcf, 'Position', [position(1:2) 1.5*position(3:4)]);
paperPosition = get(gcf,'PaperPosition');
set(gcf, 'PaperPosition', [paperPosition(1:2) 1.5*paperPosition(3:4)]);


figure(14)

p = trueProbe;
c(1)=subplot(1,6,1);
imagesc(hsv2rgb(im2hsv(p./max(max(abs(p))), 1)));
axis equal tight
title('True Probe')

p = probe(:,:,1);
c(2) = subplot(1,6,2);
imagesc(hsv2rgb(im2hsv(p./max(max(abs(p))), 1)));
axis equal tight
title('PHeBIE: Probe')

p = probe(:,:,2);
c(3) = subplot(1,6,3);
imagesc(hsv2rgb(im2hsv(p./max(max(abs(p))), 1)));
axis equal tight
title('Rodenburg: Probe')

p = probe(:,:,3);
c(4) = subplot(1,6,4);
imagesc(hsv2rgb(im2hsv(p./max(max(abs(p))), 1)));
axis equal tight
title('Thibault: Probe')

p = probe(:,:,4);
c(3) = subplot(1,6,5);
imagesc(hsv2rgb(im2hsv(p./max(max(abs(p))), 1)));
axis equal tight
title('Thibault RAAR: Probe')

p = probe(:,:,5);
c(4) = subplot(1,6,6);
imagesc(hsv2rgb(im2hsv(p./max(max(abs(p))), 1)));
axis equal tight
title('Thibault AP: Probe')
% pos_c4 = get(c(4),'Position');
% colorbar
% set(c(4),'Position',pos_c4);

position = get(gcf,'Position');
set(gcf, 'Position', [position(1:2) 2*position(3:4)]);
paperPosition = get(gcf,'PaperPosition');
set(gcf, 'PaperPosition', [paperPosition(1:2) 2*paperPosition(3:4)]);

%% Initializations and constraints
figure(15)
imagesc(input.Probe_mask);
colormap gray;
%colorbar
axis equal tight
title('Probe Mask')

figure(16)
imagesc(input.object_support);
colormap gray;
%colorbar
axis equal tight
title('Object Support');

figure(17)
p = input.probe_guess;
imagesc(hsv2rgb(im2hsv(p./max(max(abs(p))), 1)));
axis equal tight;
title('Initial Probe Guess');

figure(18);
subplot(1,2,1);
imagesc(abs(input.object_guess));
title('Initial Object Amplitude')
axis equal tight
colormap gray;
colorbar
subplot(1,2,2);
imagesc(angle(input.object_guess));
axis equal tight
caxis([-pi +pi])
colorbar
title('Initial Object Phase')

clear input
%% Save files
figs = get(0,'children');
directory = ['png'];
if ~exist(directory,'dir')
    mkdir(directory);
end
for i=1:length(figs)
    figure_filename = [directory '/' 'figure' num2str(i)];
    %saveas(figs(i), figure_filename, 'fig');
    saveas(figs(i), figure_filename, 'png');
end
