%                      HPR.m
%             written on May 23, 2002 by
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%               Revised Jan.17, 2005,
%                    Russell Luke
%               University of Delaware
%
% DESCRIPTION:  user-friendly version of the Heinz-Patrick-Russell algorithm.  
%               See Bauschke,Combettes&Luke, Journal of the Optical 
%   		    Society of America A, 20(6):1025-1034 (2003)
%
%
% INPUT: u, an array, and method_input, a data structure
%
% OUTPUT: unew and method_input, the appended data structure with 
%
% USAGE: [unew, method_input] = HPR(u, method_input)
%
% Nonstandard Matlab function calls:  method_input.Prox1 and .Prox2

function [unew, method_input] = HPR(u, method_input)

beta0 = method_input.beta_0;
beta_max = method_input.beta_max;
beta_switch=method_input.beta_switch;
iter = method_input.iter;

beta = exp((-iter/beta_switch).^3)*beta0+(1-exp((-iter/beta_switch).^3))*beta_max; % unrelaxes as the
    % iterations proceed.
    % next iterate
    % tmp_u = (feval('R_S',S,feval('R_M',M,u))+u)/2;  % (R_S(R_M(u))+u)/2
tmp = feval(method_input.Prox2,method_input,u);
tmp2=(2+beta-1)*tmp- u;
unew = (2*feval(method_input.Prox1,method_input,tmp2)-tmp2 + u +(1-beta)*tmp)/2;
