%                      GRAAL.m
%             written on June 23, 2017 by
%                   Russell Luke & Yura Malitskyi
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  non-user-friendly version of Malitskyi's Golden Ratio Algorithm
%
%
% INPUT:  method_input, a data structure
%
% OUTPUT: method_output, a data structure with 
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = RAAR(method_input)
%
% Nonstandard Matlab function calls:  method_input.Prox1 and .Prox2

function method_output = GRAAL_expert(method_input)

Prox1 = method_input.Prox1;
Prox2 = method_input.Prox2;
x0 = method_input.u_0;
[~,~,p,q]=size(x0);
MAXIT = method_input.MAXIT;
TOL = method_input.TOL;
TOL2 = method_input.TOL2;
if(~any(strcmp('phi', fieldnames(method_input))))
    phi = 1.4;
else
    phi=method_input.phi;
end
if(~any(strcmp('inv_Lipschitz_const', fieldnames(method_input))))
    la = 0.5;
else
    la=method_input.inv_Lipschitz_const;
end
iter=1;
method_input.iter=iter;
% preallocate the error monitors:
change=zeros(1,MAXIT);
change(1)=999;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  set up diagnostic arrays
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=change;
    shadow_change=change;
    if(any(strcmp('truth',fieldnames(method_input))))
        Relerrs = change;
    end
    if(~any(strcmp('anim',fieldnames(method_input))))
        anim = 0;           % numeric graphics toggle.  anim=1 generates animated picture
        %   of each iterate in succession.
    else
        anim=method_input.anim;
        if anim==2
            % preallocate an array to store the movie frames
            method_output.mov(floor(MAXIT/2)) = struct('cdata',[],'colormap',[]);
        end
    end
    if(anim>=1)
        method_output.u=x0;
        method_output=feval(method_input.animation, method_input,method_output);
    end
end
normM = method_input.norm_data; % normM = sqrt(sum(sum(method_input.data.*method_input.data)));

tic;
%    """
%   Golden Ratio Algorithm for the problem x = Tx
%
%    T is the operator
%    J is the energy which we want to check, like |x-x*|
%   x0 is the starting point
%
%    """

x = x0;
xbar = x0;
tau = 1. / phi + 1. / phi^2;
tmp1 = feval(Prox1, method_input, x0);
tmp2 = feval(Prox2, method_input, tmp1);
% F = lambda x: x - T(x)
th = 1;
Fx = x-tmp2;

while ((iter<MAXIT+1)&& (change(iter)>=TOL))
    iter=iter+1;
    method_input.iter=iter;
    x1 = xbar - la * Fx;
    tmp1 = feval(Prox1,method_input,x1);
    tmp2 = feval(Prox2, method_input,tmp1);
    Fx1 = x1-tmp2;
    
    n1 = feval('norm',x1 - x,'fro')^2;
    n2 = feval('norm',Fx1 - Fx, 'fro')^2;
    % la1 = min(tau * la, 0.25 * phi * th / la * safe_division(n1, n2))
    la1 = feval('min',tau * la, 0.25 * phi * th / la * (n1 / n2));
    change(iter) = sqrt(n1);
    gap(iter) = norm(tmp1-tmp2);
    xbar = ((phi - 1) * x1 + xbar) / phi;
    th = phi * la1 / la;
    x=x1; la=la1; Fx=Fx1;
    
    if(any(strcmp('diagnostic', fieldnames(method_input))))
        % iter, la
        % graphics
        if((anim>=1)&&(mod(iter,2)==0))
            method_output.u=x;
            method_output=feval(method_input.animation, method_input,method_output);
        end
    end
end
method_output.stats.time = toc;

u=x;
tmp = feval(Prox1,method_input,u);
method_output.u=tmp;

tmp2 = feval(Prox2,method_input,u);
if(method_input.Nx==1)
    method_output.u1 = tmp(:,1);
    method_output.u2 = tmp2(:,1);
elseif(method_input.Ny==1)
    method_output.u1 = tmp(1,:);
    method_output.u2 = tmp2(1,:);
elseif(method_input.Nz==1)
    method_output.u1 = tmp(:,:,1);
    method_output.u2 = tmp2(:,:,1);
else
    method_output.u1 = tmp;
    method_output.u2 = tmp2;
end
method_input.data_ball=tmp2;
change=change(2:iter);
method_output.stats.iter = iter-1;
method_output.stats.change = change;

if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=gap(2:iter);
    shadow_change=gap;
    method_output.stats.gap = gap;
    method_output.stats.shadow_change = shadow_change;
    if(any(strcmp('truth',fieldnames(method_input))))
        method_output.stats.Relerrs=Relerrs;
    end
end
