function method_output = fixed_point_mapping(method_input)
%                  fixed_point_mapping.m
%             written on Aug. 18, 2017 by
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%
% DESCRIPTION:  algorithm wrapper for easy development with ProxToolbox
%
%
% INPUT:  method_input, a data structure
%
% OUTPUT: method_output, a data structure with 
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = algorithm_wrapper(method_input)
%
% Nonstandard Matlab function calls:  method_input.Prox1 and .Prox2

%%%%%%%%%%%%%%%%%%%%%%
%  Initialize        %
%%%%%%%%%%%%%%%%%%%%%%

MAXIT = method_input.MAXIT;
TOL = method_input.TOL;
 
iter=1;
method_input.iter=iter;
u = method_input.u_0;
change=zeros(1,MAXIT);
% Even if more detailed diagnstics are not run, the mathematical 
% exit criteria will be on step-sizes, for which one needs to monitor the 
% norm of the difference between successive iterates of the blocks, in 
% the case that u is a cell of blocks. 
% To do this, we need to convert the relevant block elements into ARRAYS
% preallocate the error monitors:
% initialize the step monitoring arrays:
[change(iter),method_input]=feval(method_input.iterate_monitor, u, method_input);
tic;
% Different algorithms will have different quatities that one 
% should monitor.  Since we take a fixed point perspective, the main
% thing to monitor is the change in the iterates.
while((iter<MAXIT+1)&&(change(iter)>=TOL))
  iter=iter+1;
  method_input.iter=iter;
  % makes call to algorithm:
  [u_new, method_input]=feval(method_input.method, u, method_input);
  [change(iter),method_input]=feval(method_input.iterate_monitor, u_new, method_input);
  % other algorithm-specific things to monitor are packed into
  % the method_input data structure.
  % update
  u=u_new;
end
method_output.stats.time = toc;

method_output.u=u;

change=change(2:iter);
method_output.stats.iter = iter-1;
method_output.stats.change = change;

method_output.u_monitor=method_input.u_monitor;
if(any(strcmp('diagnostic', fieldnames(method_input)))&&(method_input.diagnostic))    
    if(~iscell(method_input.u_monitor))
        u2 = feval(method_input.Prox2,method_input,method_input.u_monitor);
        u1 = feval(method_input.Prox1,method_input,u2);
        if(method_input.Nx==1)
            method_output.u1 = u1(:,1);
            method_output.u2 = u2(:,1);
        elseif(method_input.Ny==1)
            method_output.u1 = u1(1,:);
            method_output.u2 = u2(1,:);
        elseif(method_input.Nz==1)
            method_output.u1 = u1(:,:,1);
            method_output.u2 = u2(:,:,1);
        else
            method_output.u1 = u1;
            method_output.u2 = u2;
        end        
    end
    method_output.stats.gap = method_input.gap(2:iter);
    if(any(strcmp('shadow_change', fieldnames(method_input))))
        method_output.stats.shadow_change = method_input.shadow_change(2:iter);
    end
    if(any(strcmp('Relerrs',fieldnames(method_input))))
        method_output.stats.Relerrs=method_input.Relerrs;
    end
end
