%                      GRAAL.m
%             written on Aug 18, 2017 by
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  user-friendly version of Malitskyi's Golden-Ratio algorithm 
%
%
% INPUT:  u, an array, and method_input, a data structure
%
% OUTPUT: method_input, an appended data structure with 
%               xnew = the algorithm fixed point
%
% USAGE: [xnew, method_input] = GRAAL_simple(x, method_input)
%
% Nonstandard Matlab function calls:  method_input.Prox1 and .Prox2

function [xnew, method_input] = GRAAL(x, method_input)

%    """
%   Golden Ratio Algorithm for the problem x = Tx
%
%    T is the operator
%    J is the energy which we want to check, like |x-x*|
%   x0 is the starting point
%
%    """
if(~any(strcmp('GRAAL_initialized', fieldnames(method_input))))
    if(~any(strcmp('phi', fieldnames(method_input))))
        phi = 1.4;
    else
        phi=method_input.phi;
    end
    if(~any(strcmp('inv_Lipschitz_const', fieldnames(method_input))))
        la = 0.5;
    else
        la=method_input.inv_Lipschitz_const;
    end   
    method_input.x=x;
    method_input.xbar=method_input.x;
    method_input.tau = 1. / phi + 1. / phi^2;
    tmp1 = feval(method_input.Prox1, method_input, x);
    tmp2 = feval(method_input.Prox2, method_input, tmp1);
    % F = lambda x: x - T(x)
    method_input.th = 1;
    method_input.Fx = x-tmp2;
    method_input.inv_Lipschitz_const=la;
    method_input.GRAAL_initialized=true;
    x1=x; 
    xnew=x1+999;
else
    phi=method_input.phi;
    la=method_input.inv_Lipschitz_const;
    x1 = method_input.xbar - la * method_input.Fx;
    tmp1 = feval(method_input.Prox1,method_input,x1);
    tmp2 = feval(method_input.Prox2, method_input,tmp1);
    Fx1 = x1-tmp2;
    
    n1 = feval('norm',x1 - x,'fro')^2;
    n2 = feval('norm',Fx1 - method_input.Fx, 'fro')^2;
    % la1 = min(tau * la, 0.25 * phi * th / la * safe_division(n1, n2))
    n1dn2=min(1e+4, n1/n2);
    la1 = feval('min',method_input.tau * la, 0.25 * phi * method_input.th / la * (n1dn2));
    method_input.xbar = ((phi - 1) * x1 + method_input.xbar) / phi;
    method_input.th = phi * la1 / la;
    method_input.inv_Lipschitz_const=la1; 
    method_input.Fx=Fx1;
    xnew=x1;
end
