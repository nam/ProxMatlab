%                      DRAP.m
%             written on Aug 18, 2017 by
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  user-friendly version of the Relaxed Averaged Alternating Reflection algorithm as 
%               proposed by Nguyen Thao (PhD Thesis, Uni. Göttingen, 2017)
%
%
% INPUT:  u, an array, and method_input, a data structure
%
% OUTPUT: method_input, the appended data structure with 
%               unew = the algorithm image
%
% USAGE: [method_output, method_input] = DRAP(method_input,u)
%
% Nonstandard Matlab function calls:  method_input.Prox1 and .Prox2

function [unew, method_input] = DRAP(u, method_input)

beta0 = method_input.beta_0;
beta_max = method_input.beta_max;
beta_switch = method_input.beta_switch;
iter = method_input.iter;

tmp1 = feval(method_input.Prox2,method_input,u);
beta = exp((-iter/beta_switch).^3)*beta0+(1-exp((-iter/beta_switch).^3))*beta_max; % unrelaxes as the
tmp2= beta*(tmp1 -u); % feval(Prox1,method_input,tmp1);
tmp_3 = feval(method_input.Prox1, method_input,tmp1+tmp2); % (beta*(2*tmp3-tmp1) + (1-beta)*tmp1 + u)/2;
unew = tmp_3-tmp2;
