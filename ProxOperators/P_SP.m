%                      P_SP.m
%             written on May 23, 2002 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               nonnegativity and support constraints
%
% INPUT:        input, a data structure with .supp_ampl a vector of indeces of the nonzero elements of the array
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_SP    = the projection IN THE PHYSICAL (time) DOMAIN
%               
% USAGE: p_SP = P_SP(input,u)
%
% Nonstandard Matlab function calls:  

function p_SP = P_SP(input,u)

p_SP=zeros(size(u));
p_SP(input.support_idx) = feval('max',real(u(input.support_idx)),0);


