%                      P_Amod.m
%             written on June 1, 2017 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               support constraints
%
% INPUT:        input.supp_ampl = OBJECT (TIME) DOMAIN CONSTRAINT:  indeces of the 
%               nonzero elements in the object domain.
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_A    = the projection IN THE PHYSICAL (time) DOMAIN,
%                        leaves the phase alone on the support, resets
%                        amplitude to 1 and sets amplitude to 1 with 0
%                        phase outside support.
%               
% USAGE: p_A = P_Amod(input,u)
%
% Nonstandard Matlab function calls:  

function p_A = P_Amod(input,u)

p_A=ones(size(u));
p_A(input.support_idx) = u(input.support_idx)./abs(u(input.support_idx));
end
