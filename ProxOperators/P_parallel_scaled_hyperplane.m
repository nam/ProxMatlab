%  hyperplane projection subroutine of the
%  Cimmino ART method  
%  for tomographic recostruction of a 
%  density profile.
% 
%  Russell Luke
%  Universitaet Goettingen
%  May 29, 2012
%
function v = P_parallel_scaled_hyperplane(input,u0)

[m,~]=size(input.A);
v=input.A*u0;
v=u0+ 1/m*input.A'*(input.b-v);


clear u0 b AAT
return