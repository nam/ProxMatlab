%  hyperplane projection subroutine of the
%  ART method  
%  for tomographic recostruction of a 
%  density profile.
% 
%  Russell Luke
%  Universitaet Goettingen
%  May 29, 2012
%
function v = P_sequential_hyperplane(input,u0)

[m,n]=size(input.A);
K=mod(input.iter,m)+1;
a=input.A(K,:);
aaT=(a*a')+1e-20;
b=input.b(K)/aaT;
v=a*u0/aaT;
v=u0+a'*(b-v);


clear u0 b AAT input
return