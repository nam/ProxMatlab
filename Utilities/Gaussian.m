% DESCRIPTION: Script for forming a 1-2D Gaussian of height 1 on a
%              Cartesian grid. 
% INPUT:  points       := number of points for gaussian
%                         representation (assumes square arrays)          
%         var          := a 1 or 2 dimensional array that gives the
%                         variance of the gaussian in the x and y 
%                         directions respectively.
%         center_index := a 1 or 2 dimensional array that gives the
%                         index coordinates of the center of the
%                         gaussian.
% OUTPUT: g, a (points)^n-dimensional gaussian of unit magnitude,
%         where n is the dimension and points is the discretization
%         resolution.
% USAGE:  g=Gaussian(points,var,center_index)
%
% Written by Russell Luke, July 17, 2001
% luke@math.uni-goettingen.de
%
function g=Gaussian(points,var,center_index)

dim=length(center_index);
L=2;
x=-L/2:L/points:L/2-L/points;
if(dim==1)
  centerx=x(center_index);
  g=exp(-((x-centerx).^2)*pi/var(1));
elseif(dim==2)  
  if((length(var)==1)&(dim==2))
    var(2)=var(1);
  end
  centerx=x(center_index(1));
  centery=x(center_index(2));
  for j=1:points
    g(j,:)=exp(-(((x-centerx).^2)/var(1)+((x(j)-centery)^2)/var(2))*pi);
  end
end
