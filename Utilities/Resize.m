% resizes arrays.  Takes an array A that is M by N and resizes it to Mnew by
% Nnew.
% usage: Anew = Resize(A, Mnew,Nnew)

function Anew = Resize(A, Mnew,Nnew)

[M,N] = size(A);
if Nnew <= N
  Nstep = N/Nnew;
  Mstep = M/Mnew;

  countN = 0;
  for j = 1:Nstep:N
    countN = countN+1;
    countM = 0;
    for i = 1:Mstep:M
      countM = countM+1;
      Anew(countM,countN) = A(i,j);
    end
  end
else
  Nstep = Nnew/N;
  Mstep = Mnew/M;
  temp=ones(Mstep,Nstep);

  countN = 0;
  for j = 1:N
    countN = countN+1;
    countM = 0;
    for i = 1:M
      countM = countM+1;
      Anew((i-1)*Mstep+1:i*Mstep,(j-1)*Nstep+1:j*Nstep) = A(countM,countN)*temp;
    end
  end
end

